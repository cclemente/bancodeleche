<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bebe extends Model{
    protected $table ='DATOS_BEBE';
    // NOMBRE DE LAS COLUMNOS DE LA TABLA
    /*
    $table->string('ID_DATOS_BEBE');
    $table->string('FECHA_NACIMIENTO');
    $table->string('HORA_NACIMIENTO');
    */
    protected $primaryKey = 'ID_DATOS_BEBE';
    public $timestamps = false;
    protected $fillable = array('FECHA_NACIMIENTO','HORA_NACIMIENTO');
    protected $hidden = ['created_at','updated_at'];
    // Definimos a continuación la relación de esta tabla con otras.
    // Ejemplos de relaciones:
    // 1 usuario tiene 1 teléfono   ->hasOne() Relación 1:1
    // 1 teléfono pertenece a 1 usuario   ->belongsTo() Relación 1:1 inversa a hasOne()
    // 1 post tiene muchos comentarios  -> hasMany() Relación 1:N
    // 1 comentario pertenece a 1 post ->belongsTo() Relación 1:N inversa a hasMany()
    // 1 usuario puede tener muchos roles  ->belongsToMany()
    //  etc..
    public function relacionbebemadre(){
      // $this hace referencia al objeto que tengamos en ese momento de Fabricante.
      return $this->hasOne('App\RelacionBebeMadre');
    }



}
