<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class RegistroUsuario extends Authenticatable{
    use Notifiable;
    // NOMBRE DE LA TABLA EN LA BASE DE DATOS
    protected $table='REGISTRO_USUARIO';
    // NOMBRE DE LAS COLUMNOS DE LA TABLA
    /*
    $table->string('PASSWORD');
    $table->string('USUARIO');
    $table->integer('ID_ROL_SISTEMA')->unsigned();
    $table->foreign('ID_ROL_SISTEMA')->references('ID_ROL_SISTEMA')->on('ROL_SISTEMA');
    $table->integer('ID_HOSPITAL')->unsigned();
    $table->foreign('ID_HOSPITAL')->references('ID_HOSPITAL')->on('HOSPITALES');
    */
    protected $primaryKey = 'ID_REGISTRO_USUARIO';
    public $timestamps = false;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'PASSWORD', 'remember_token',
    ];
    protected $fillable = array('USUARIO','PASSWORD','ID_ROL_SISTEMA','ID_HOSPITAL');
    // Definimos a continuación la relación de esta tabla con otras.
    // Ejemplos de relaciones:
    // 1 usuario tiene 1 teléfono   ->hasOne() Relación 1:1
    // 1 teléfono pertenece a 1 usuario   ->belongsTo() Relación 1:1 inversa a hasOne()
    // 1 post tiene muchos comentarios  -> hasMany() Relación 1:N
    // 1 comentario pertenece a 1 post ->belongsTo() Relación 1:N inversa a hasMany()
    // 1 usuario puede tener muchos roles  ->belongsToMany()
    //  etc..
    public function relacionmadreprueba(){
      return $this->hasOne('App\RolSistema','App\Hospitales');
    }


}
