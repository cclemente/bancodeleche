<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelacionMadreRegistroUsuario extends Model{
    protected $table = 'RELACION_MADRE_REGISTRO_USUARIO';
    // NOMBRE DE LAS COLUMNOS DE LA TABLA
    /*
      $table->integer('ID_MADRE')->unsigned();
      $table->foreign('ID_MADRE')->references('ID_MADRE')->on('MADRE');
      $table->integer('ID_REGISTRO_USUARIO')->unsigned();
      $table->foreign('ID_REGISTRO_USUARIO')->references('ID_REGISTRO_USUARIO')->on('REGISTRO_USUARIO');
    */
    public $timestamps = false;
    protected $primaryKey = 'ID_RELACION_MADRE_REGISTRO_USUARIO';
    protected $fillable = array('ID_MADRE','id');
    protected $hidden = ['created_at','updated_at'];

    public function relacionmadreregistrousuario(){
      // 1 fabricante tiene muchos aviones
      // $this hace referencia al objeto que tengamos en ese momento de Fabricante.
      return $this->hasOne('App\Madre','App\Usuario');
    }
}
