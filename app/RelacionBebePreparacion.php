<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelacionBebePreparacion extends Model{
  protected $table ='RELACION_BEBE_PREPARACION';
  // NOMBRE DE LAS COLUMNOS DE LA TABLA
  /*
  $table->string('ID_DATOS_BEBE');
  $table->string('FECHA_NACIMIENTO');
  $table->string('HORA_NACIMIENTO');
  */
  protected $primaryKey = 'ID_RELACION_BEBE_PREPARACION';
  public $timestamps = false;
  protected $fillable = array('ID_DATOS_BEBE','ID_PREPARACION_SUCEDANEO');
  protected $hidden = ['created_at','updated_at'];
  // Definimos a continuación la relación de esta tabla con otras.
  // Ejemplos de relaciones:
  // 1 usuario tiene 1 teléfono   ->hasOne() Relación 1:1
  // 1 teléfono pertenece a 1 usuario   ->belongsTo() Relación 1:1 inversa a hasOne()
  // 1 post tiene muchos comentarios  -> hasMany() Relación 1:N
  // 1 comentario pertenece a 1 post ->belongsTo() Relación 1:N inversa a hasMany()
  // 1 usuario puede tener muchos roles  ->belongsToMany()
  //  etc..
  public function relaciondatosbebemadre(){
    // $this hace referencia al objeto que tengamos en ese momento de Fabricante.
    return $this->hasOne('App\Bebe','App\PreparacionSucedaneo');
  }
}
