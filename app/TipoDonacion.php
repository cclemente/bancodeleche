<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDonacion extends Model{
  // NOMBRE DE LA TABLA EN LA BASE DE DATOS
  protected $table = 'TIPO_DONACION';
  // NOMBRE DE LAS COLUMNOS DE LA TABLA
  /*
  $table->string('DESCRIPCION');
  */
  public $timestamps = false;
  protected $primaryKey = 'ID_TIPO_DONACION';
  protected $fillable = array('DESCRIPCION');
  protected $hidden = ['created_at','updated_at'];
  // Definimos a continuación la relación de esta tabla con otras.
  // Ejemplos de relaciones:
  // 1 usuario tiene 1 teléfono   ->hasOne() Relación 1:1
  // 1 teléfono pertenece a 1 usuario   ->belongsTo() Relación 1:1 inversa a hasOne()
  // 1 post tiene muchos comentarios  -> hasMany() Relación 1:N
  // 1 comentario pertenece a 1 post ->belongsTo() Relación 1:N inversa a hasMany()
  // 1 usuario puede tener muchos roles  ->belongsToMany()
  //  etc..
  public function relacionmadreprueba(){
    return $this->hasOne('App\Recepcion');
  }
}
