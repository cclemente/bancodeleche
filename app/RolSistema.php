<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolSistema extends Model{
  // NOMBRE DE LA TABLA EN LA BASE DE DATOS
  protected $table = 'ROl_SISTEMA';
  // NOMBRE DE LAS COLUMNOS DE LA TABLA
  /*
  $table->increments('ID_ROL_SISTEMA');
  $table->string('DESCRIPCION');
  */
  public $timestamps = false;
  protected $primaryKey = 'ID_ROL_SISTEMA';
  protected $fillable = array('DESCRIPCION');
  // Definimos a continuación la relación de esta tabla con otras.
  // Ejemplos de relaciones:
  // 1 usuario tiene 1 teléfono   ->hasOne() Relación 1:1
  // 1 teléfono pertenece a 1 usuario   ->belongsTo() Relación 1:1 inversa a hasOne()
  // 1 post tiene muchos comentarios  -> hasMany() Relación 1:N
  // 1 comentario pertenece a 1 post ->belongsTo() Relación 1:N inversa a hasMany()
  // 1 usuario puede tener muchos roles  ->belongsToMany()
  //  etc..
  public function relacionmadreprueba(){
    return $this->hasOne('App\RegistroUsuario');
  }
}
