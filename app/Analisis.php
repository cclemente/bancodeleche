<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Analisis extends Model{
    // NOMBRE DE LA TABLA EN LA BASE DE DATOS
    protected $table = 'ANALISIS';
    // NOMBRE DE LAS COLUMNOS DE LA TABLA
    /*
    $table->increments('ID_ANALISIS');
    $table->integer('ID_CLASIFICACION')->unsigned();
    $table->foreign('ID_CLASIFICACION')->references('ID_CLASIFICACION')->on('CLASIFICACION_LECHE');
    $table->boolean('EMBALAJE');
    $table->boolean('COLOR');
    $table->boolean('SUCIEDAD');
    $table->boolean('OLOR');
    $table->decimal('CANTIDAD');
    $table->boolean('DESECHADA');
    $table->integer('ID_RECEPCION')->unsigned();
    $table->foreign('ID_RECEPCION')->references('ID_RECEPCION')->on('RECEPCION');
    */
    protected $primaryKey = 'ID_ANALISIS';
    public $timestamps = false;
    protected $fillable = array('ID_CLASIFICACION','EMBALAJE','COLOR','SUCIEDAD','OLOR','CREMATROCITO','KILO_CALORIAS','PROTEINAS','CARBOHIDRATOS','GRASAS','CANTIDAD_ANALIZADA','CANTIDAD_DESECHADA','CANTIDAD_ALMACENADA','ID_RECEPCION','ID_ETIQUETA');
    protected $hidden = ['created_at','updated_at'];
    // Definimos a continuación la relación de esta tabla con otras.
    // Ejemplos de relaciones:
    // 1 usuario tiene 1 teléfono   ->hasOne() Relación 1:1
    // 1 teléfono pertenece a 1 usuario   ->belongsTo() Relación 1:1 inversa a hasOne()
    // 1 post tiene muchos comentarios  -> hasMany() Relación 1:N
    // 1 comentario pertenece a 1 post ->belongsTo() Relación 1:N inversa a hasMany()
    // 1 usuario puede tener muchos roles  ->belongsToMany()
    //  etc..
    public function relacionconanalisisyrecepcion(){
      // $this hace referencia al objeto que tengamos en ese momento de Fabricante.
      return $this->hasOne('App\RelacionMadrePrueba','App\Recepcion');
    }
}
