<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelacionMadrePrueba extends Model{
  protected $table = 'RELACION_MADRE_PRUEBA';
  // NOMBRE DE LAS COLUMNOS DE LA TABLA
  /*
  $table->integer('ID_MADRE')->unsigned();
  $table->foreign('ID_MADRE')->references('ID_MADRE')->on('MADRE');
  $table->integer('ID_PRUEBA')->unsigned();
  $table->foreign('ID_PRUEBA')->references('ID_PRUEBA')->on('PRUEBA');
  */
  public $timestamps = false;
  protected $primaryKey = 'ID_RELACION_MADRE_PRUEBA';
  protected $fillable = array('ID_MADRE','ID_PRUEBA',);
  protected $hidden = ['created_at','updated_at'];

  public function relacionmadreprueba(){
    // 1 fabricante tiene muchos aviones
    // $this hace referencia al objeto que tengamos en ese momento de Fabricante.
    return $this->hasOne('App\Prueba','App\Madre');
  }
}
