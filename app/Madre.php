<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Madre extends Model{
    // NOMBRE DE LA TABLA EN LA BASE DE DATOS
    protected $table = 'MADRE';
    // NOMBRE DE LAS COLUMNOS DE LA TABLA
    /*
    $table->string('ID_SISTEMA');
    $table->string('NOMBRE');
    $table->string('APELLIDO_PATERNO');
    $table->string('APELLIDO_MATERNO');
    $table->date('FECHAR_REGISTRO');
    */
    protected $primaryKey = 'ID_MADRE';
    public $timestamps = false;
    protected $fillable = array('ID_SISTEMA','NOMBRE','APELLIDO_PATERNO','APELLIDO_MATERNO','FECHA_REGISTRO');
    protected $hidden = ['created_at','updated_at'];
    // Definimos a continuación la relación de esta tabla con otras.
    // Ejemplos de relaciones:
    // 1 usuario tiene 1 teléfono   ->hasOne() Relación 1:1
    // 1 teléfono pertenece a 1 usuario   ->belongsTo() Relación 1:1 inversa a hasOne()
    // 1 post tiene muchos comentarios  -> hasMany() Relación 1:N
    // 1 comentario pertenece a 1 post ->belongsTo() Relación 1:N inversa a hasMany()
    // 1 usuario puede tener muchos roles  ->belongsToMany()
    //  etc..
    public function relacionmadreprueba(){
      // $this hace referencia al objeto que tengamos en ese momento de Fabricante.
      return $this->hasOne('App\RelacionMadrePrueba');
    }
}
