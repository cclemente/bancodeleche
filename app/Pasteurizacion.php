<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasteurizacion extends Model{
  // NOMBRE DE LA TABLA EN LA BASE DE DATOS
  protected $table = 'PASTEURIZACION';
  // NOMBRE DE LAS COLUMNOS DE LA TABLA
  /*
  $table->increments('ID_DOSIFICACION');
  $table->string('ID_SISTEMA');
  $table->date('FECHA_DOSIFICACION');
  $table->time('HORA_DOSIFICACION');
  $table->string('SERVICIO');
  $table->decimal('CANTIDAD_DOSIFICACION_ML');
  $table->boolean('NUMERO_TOMA');
  $table->decimal('ADMINISTRACION_ML');
  */
//  protected $primaryKey = 'ID_DOSIFICACION';
  public $timestamps = false;
  protected $fillable = array('ID_ETIQUETA','ETIQUETA_PASTEURIZACION','ACIDEZ_DORNIC','PASTEURIZADA','CANTIDAD_PASTEURIZADA','CANTIDAD_DESECHADA');
  protected $hidden = ['created_at','updated_at'];
  // Definimos a continuación la relación de esta tabla con otras.
  // Ejemplos de relaciones:
  // 1 usuario tiene 1 teléfono   ->hasOne() Relación 1:1
  // 1 teléfono pertenece a 1 usuario   ->belongsTo() Relación 1:1 inversa a hasOne()
  // 1 post tiene muchos comentarios  -> hasMany() Relación 1:N
  // 1 comentario pertenece a 1 post ->belongsTo() Relación 1:N inversa a hasMany()
  // 1 usuario puede tener muchos roles  ->belongsToMany()
  //  etc..
}
