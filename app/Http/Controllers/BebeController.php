<?php

namespace App\Http\Controllers;

use App\Bebe;
use App\Madre;
use App\RelacionBebeMadre;
use Illuminate\Http\Request;
use DB;
class BebeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
      if (!$request->input('id_sistema')){
        // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
        // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
        return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para recuperar datos.'])],422);
      }
      $resultado=DB::table('DATOS_BEBE')
         ->join('RELACION_MADRE_DATOS_BEBE','DATOS_BEBE.ID_DATOS_BEBE','=','RELACION_MADRE_DATOS_BEBE.ID_DATOS_BEBE')
         ->join('MADRE','RELACION_MADRE_DATOS_BEBE.ID_MADRE','=','MADRE.ID_MADRE')
         ->select('*')
         ->where('MADRE.ID_SISTEMA','=',$request->id_sistema)
         ->get();
      return response()->json(['status'=>'ok','data'=>$resultado],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
      if (!$request->input('id_sistema') || !$request->input('fecha_nacimiento') || !$request->input('hora_nacimiento')) {
        // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
        // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
        return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
      }
      $resultado=Bebe::create(['FECHA_NACIMIENTO'=>$request->fecha_nacimiento,'HORA_NACIMIENTO'=>$request->hora_nacimiento]);
      $madre=Madre::where(['ID_SISTEMA'=>$request->id_sistema])->first();
      $relacion=RelacionBebeMadre::create(['ID_MADRE'=>$madre->ID_MADRE,'ID_DATOS_BEBE'=>$resultado->ID_DATOS_BEBE]);
      return response()->json(['status'=>'ok','data'=>$resultado],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bebe  $bebe
     * @return \Illuminate\Http\Response
     */
    public function show(Bebe $bebe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bebe  $bebe
     * @return \Illuminate\Http\Response
     */
    public function edit(Bebe $bebe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bebe  $bebe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bebe $bebe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bebe  $bebe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bebe $bebe)
    {
        //
    }
}
