<?php

namespace App\Http\Controllers;

use App\DosificacionSucedaneo;
use App\RelacionDosificacionSucedaneo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class DosificacionSucedaneoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        if(!$request->input('id_sistema') || !$request->input('fecha')|| !$request->input('id_bebe')){
          // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
          // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
          return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
        }
        $resultado=DB::table('DOSIFICACION_SUCEDANEO')
           ->join('RELACION_BEBE_DOSIFICACION_S','DOSIFICACION_SUCEDANEO.ID_DOSIFICACION_SUCEDANEO','=','RELACION_BEBE_DOSIFICACION_S.ID_DOSIFICACION_SUCEDANEO')
           ->join('DATOS_BEBE','RELACION_BEBE_DOSIFICACION_S.ID_DATOS_BEBE','=','DATOS_BEBE.ID_DATOS_BEBE')
           ->select('*')
           ->where('DOSIFICACION_SUCEDANEO.ID_SISTEMA','=',$request->id_sistema)
           ->where('DOSIFICACION_SUCEDANEO.FECHA_DOSIFICACION','=',$request->fecha)
           ->where('DATOS_BEBE.ID_DATOS_BEBE','=',$request->id_bebe)
           ->count();
        return response()->json(['status'=>'ok','data'=>$resultado],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
      // Primero comprobaremos si estamos recibiendo todos los campos.
      if(!$request->input('id_sistema') || !$request->input('fecha') || !$request->input('hora') || !$request->input('servicio') || !$request->input('tipoFormula') || !$request->input('dosificacion')|| !$request->input('cantidad_desechada')|| !$request->input('id_bebe')){
        // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
        // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
        return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
      }
      $result=DosificacionSucedaneo::create(['ID_SISTEMA'=>$request->id_sistema,'FECHA_DOSIFICACION'=>$request->fecha,'HORA_DOSIFICACION'=>$request->hora,'SERVICIO'=>$request->servicio,'ID_TIPO_FORMULA'=>$request->tipoFormula,'CANTIDAD_DOSIFICACION_ML'=>$request->dosificacion,'CANTIDAD_DESECHADA_ML'=>$request->cantidad_desechada]);
      $relacion=RelacionDosificacionSucedaneo::create(['ID_DATOS_BEBE'=>$request->id_bebe,'ID_DOSIFICACION_SUCEDANEO'=>$result->ID_DOSIFICACION_SUCEDANEO]);
      return response()->json(['status'=>'ok','data'=>$result,'data2'=>$relacion],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DosificacionSucedaneo  $dosificacionSucedaneo
     * @return \Illuminate\Http\Response
     */
    public function show(DosificacionSucedaneo $dosificacionSucedaneo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DosificacionSucedaneo  $dosificacionSucedaneo
     * @return \Illuminate\Http\Response
     */
    public function edit(DosificacionSucedaneo $dosificacionSucedaneo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DosificacionSucedaneo  $dosificacionSucedaneo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DosificacionSucedaneo $dosificacionSucedaneo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DosificacionSucedaneo  $dosificacionSucedaneo
     * @return \Illuminate\Http\Response
     */
    public function destroy(DosificacionSucedaneo $dosificacionSucedaneo)
    {
        //
    }
}
