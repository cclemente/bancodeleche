<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Session;
use App\Madre;
use DB;
use App\Usuario;
use App\Prueba;
use App\RelacionMadrePrueba;
use App\RelacionMadreRegistroUsuario;
class MadreController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
      /*
      $todos=DB::table('MADRE')
          ->join('RELACION_MADRE_RECEPCION','MADRE.ID_MADRE','=','RELACION_MADRE_RECEPCION.ID_MADRE')
          ->join('RECEPCION', 'RECEPCION.ID_RECEPCION', '=', 'RELACION_MADRE_RECEPCION.ID_RECEPCION')
          ->join('RELACION_RECEPCION_NINIO','RELACION_RECEPCION_NINIO.ID_RECEPCION','=','RECEPCION.ID_RECEPCION')
          ->join('NINIO', 'NINIO.ID_NINIO','=','RELACION_RECEPCION_NINIO.ID_NINIO')
          ->select('NINIO.ETIQUETA')
          ->where('RECEPCION.ID_TIPO_DONACION','=','1')
          ->get();
      if (empty($todos)){
        return response()->json(['status'=>'ok','data'=>$DatosMadre,'data1'=>$todos],200);
      }*/
      if (!$request->input('id_sesion')){
        return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para recuperar datos.'])],422);
      }
      $todos=DB::table('MADRE')
          ->join('RELACION_MADRE_REGISTRO_USUARIO','MADRE.ID_MADRE','=','RELACION_MADRE_REGISTRO_USUARIO.ID_MADRE')
          ->select('*')
          ->where('RELACION_MADRE_REGISTRO_USUARIO.ID_REGISTRO_USUARIO','=',$request->id_sesion)
          ->get();
      return response()->json(['status'=>'ok','data'=>$todos],200);
      //  return response()->json(['status'=>'ok','data'=>Madre::all()], 200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        // Primero comprobaremos si estamos recibiendo todos los campos.
        if (!$request->input('NOMBRE') || !$request->input('APELLIDO_PATERNO') || !$request->input('APELLIDO_MATERNO') || !$request->input('SIDA') || !$request->input('SIFILIS') || !$request->input('HEPATITIS') || !$request->input('DROGAS') || !$request->input('ID_SESION') ){
          // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
          // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
          return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
        }
        $id_sistema=strtoupper($request->NOMBRE[0]).strtoupper($request->NOMBRE[0]).strtoupper($request->APELLIDO_PATERNO[0]).strtoupper($request->APELLIDO_PATERNO[1]).date('d-m').(string)rand(0,10).(string)rand(0,10).rand(0,10);
        $rest = Madre::create($request->all()+['FECHA_REGISTRO'=>date('Y-m-d'),'ID_SISTEMA'=>$id_sistema]);
        $otro = Prueba::create(['SIDA'=>intval($request->SIDA)-1,'SIFILIS'=>intval($request->SIFILIS)-1,'HEPATITIS'=>intval($request->HEPATITIS)-1,'DROGAS'=>intval($request->DROGAS)-1]);
        $ultimo=RelacionMadrePrueba::create(['ID_MADRE'=>$rest->ID_MADRE,'ID_PRUEBA'=>$otro->ID_PRUEBA]);
        $importante=RelacionMadreRegistroUsuario::create(['ID_MADRE'=>$rest->ID_MADRE,'id'=>intval($request->ID_SESION)]);
        return response()->json($rest->ID_SISTEMA, 201);
    }

    /**
     * SOLO REGRESA UN ELMENTO SI ES QUE LO ENCUENTRA
     *
     * @param  \App\madre  $madre
     * @return \Illuminate\Http\Response
     */
    public function show(madre $madre){
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\madre  $madre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, madre $madre){
      $madre->update($request->all());
      return response()->json($madre, 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\madre  $madre
     * @return \Illuminate\Http\Response
     */
    public function destroy(madre $madre)
    {
        //
    }
}
