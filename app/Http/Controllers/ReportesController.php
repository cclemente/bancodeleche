<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Session;
use App\Madre;
use DB;
use App\Usuario;
use App\Prueba;
use App\Recepcion;
use App\Dosificacion;
use App\DosificacionSucedaneo;
use App\RelacionMadrePrueba;
use App\RelacionMadreRegistroUsuario;
class ReportesController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
      if (!$request->input('tipo_total')){
        return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para recuperar datos.'])],422);
      }
      if ($request->tipo_total==1) {
        if (!$request->input('fecha_inicio') || !$request->input('fecha_fin')){
          return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para recuperar datos.'])],422);
        }
        $resultmadre=DB::table('MADRE')
               ->where('MADRE.FECHA_REGISTRO','>=',date($request->fecha_inicio))
               ->where('MADRE.FECHA_REGISTRO','<=',date($request->fecha_fin))
               ->count();
        $resultdat=DB::table('DATOS_BEBE')
             ->where('DATOS_BEBE.FECHA_NACIMIENTO','>=',date($request->fecha_inicio))
             ->where('DATOS_BEBE.FECHA_NACIMIENTO','<=',date($request->fecha_fin))
             ->count();
        $resulttipo=DB::table('RECEPCION')
             ->where('RECEPCION.ID_TIPO_DONACION','=',1)
             ->where('RECEPCION.FECHA_EXTRACCION','>=',date($request->fecha_inicio))
             ->where('RECEPCION.FECHA_EXTRACCION','<=',date($request->fecha_fin))
             ->count();
        $resultdos=DB::table('RECEPCION')
             ->where('RECEPCION.ID_TIPO_DONACION','=',2)
             ->where('RECEPCION.FECHA_EXTRACCION','>=',date($request->fecha_inicio))
             ->where('RECEPCION.FECHA_EXTRACCION','<=',date($request->fecha_fin))
             ->count();
          return response()->json(['status'=>'ok','data_madre'=>$resultmadre,'data_bebe'=>$resultdat,'data_homologa'=>$resulttipo,'data_heterologa'=>$resultdos],200);
    
        }/*
      Si (fechaInicio && fechaFin) {
        respuesta[0] = obtener total leche desechada entre esas fechas;
        respuesta[1] = obtener total sucedaneo desechado entre esas fechas;
        respuesta[2] = obtener promedio de leche consumida entre esas fechas;
        respuesta[3] = obtener promedio sucedaneo consumido entre esas fechas;
      } Otro {
        respuesta[0] = obtener total leche desechada;
        respuesta[1] = obtener total sucedaneo desechado;
        respuesta[2] = obtener promedio de leche consumida;
        respuesta[3] = obtener promedio sucedaneo consumido;
      }
      */
      if ($request->tipo_total==2) {
        if ($request->fecha_inicio && $request->fecha_inicio) {
          $result=DB::table('ANALISIS')
                  ->join('RECEPCION','RECEPCION.ID_RECEPCION','=','ANALISIS.ID_RECEPCION')
                  ->where('RECEPCION.FECHA_EXTRACCION','>=',$request->fecha_inicio)
                  ->where('RECEPCION.FECHA_EXTRACCION','<=',$request->fecha_fin)
                  //->select('SUM()')
                  ->sum('ANALISIS.CANTIDAD_DESECHADA');
          $resultdos=DB::table('ANALISIS')
                  ->join('RECEPCION','RECEPCION.ID_RECEPCION','=','ANALISIS.ID_RECEPCION')
                  ->join('PASTEURIZACION','PASTEURIZACION.ID_ETIQUETA','=','ANALISIS.ID_ETIQUETA')
                  ->where('RECEPCION.FECHA_EXTRACCION','>=',$request->fecha_inicio)
                  ->where('RECEPCION.FECHA_EXTRACCION','<=',$request->fecha_fin)
                  //->select('SUM()')
                  ->sum('PASTEURIZACION.CANTIDAD_DESECHADA');
          $resultres=DB::table('DOSIFICACION_SUCEDANEO')
                  ->where('DOSIFICACION_SUCEDANEO.FECHA_DOSIFICACION','>=',$request->fecha_inicio)
                  ->where('DOSIFICACION_SUCEDANEO.FECHA_DOSIFICACION','<=',$request->fecha_fin)
                  //->select('SUM()')
                  ->sum('DOSIFICACION_SUCEDANEO.CANTIDAD_DESECHADA_ML');
          $total=$result+$resultdos+$resultres;
          $promedio=Dosificacion::avg('CANTIDAD_DOSIFICACION_ML');
          $promediodos=DosificacionSucedaneo::avg('CANTIDAD_DOSIFICACION_ML');
          return response()->json(['status'=>'ok_','total'=>$total,'sucedaneo'=>$resultres,'promedio_bebe_consumo'=>$promedio,'promedio_bebe_sucedaneo'=>$promediodos],200);
        }else {
          $result=DB::table('ANALISIS')
                  ->join('RECEPCION','RECEPCION.ID_RECEPCION','=','ANALISIS.ID_RECEPCION')
                  ->sum('ANALISIS.CANTIDAD_DESECHADA');
          $resultdos=DB::table('ANALISIS')
                  ->join('RECEPCION','RECEPCION.ID_RECEPCION','=','ANALISIS.ID_RECEPCION')
                  ->join('PASTEURIZACION','PASTEURIZACION.ID_ETIQUETA','=','ANALISIS.ID_ETIQUETA')
                  ->sum('PASTEURIZACION.CANTIDAD_DESECHADA');
          $resultres=DB::table('DOSIFICACION_SUCEDANEO')
                  ->sum('DOSIFICACION_SUCEDANEO.CANTIDAD_DESECHADA_ML');
          $total=$result+$resultdos+$resultres;
          $promedio=DB::table('DOSIFICACION')
               ->avg('CANTIDAD_DOSIFICACION_ML');
          $promediodos=DB::table('DOSIFICACION_SUCEDANEO')
                 ->avg('CANTIDAD_DOSIFICACION_ML');
          return response()->json(['status'=>'ok_','total'=>$total,'sucedaneo'=>$resultres,'promedio_bebe_consumo'=>$promedio,'promedio_bebe_sucedaneo'=>$promediodos],200);
        }

      }
      if ($request->tipo_total==6) {

          return response()->json(['status'=>'ok_','data'=>$result],200);
      }
      if ($request->tipo_total==7) {

          return response()->json(['status'=>'ok_','data'=>$result],200);
      }
      if ($request->tipo_total==8) {

          return response()->json(['status'=>'ok_','data'=>$result],200);
      }
      if ($request->tipo_total==9) {

          return response()->json(['status'=>'ok_','data'=>$result],200);
      }

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        // Primero comprobaremos si estamos recibiendo todos los campos.
        if (!$request->input('NOMBRE') || !$request->input('APELLIDO_PATERNO') || !$request->input('APELLIDO_MATERNO') || !$request->input('SIDA') || !$request->input('SIFILIS') || !$request->input('HEPATITIS') || !$request->input('DROGAS') || !$request->input('ID_SESION') ){
          // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
          // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
          return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
        }
        $id_sistema=strtoupper($request->NOMBRE[0]).strtoupper($request->NOMBRE[0]).strtoupper($request->APELLIDO_PATERNO[0]).strtoupper($request->APELLIDO_PATERNO[1]).date('d-m').(string)rand(0,10).(string)rand(0,10).rand(0,10);
        $rest = Madre::create($request->all()+['FECHA_REGISTRO'=>date('Y-m-d'),'ID_SISTEMA'=>$id_sistema]);
        $otro = Prueba::create(['SIDA'=>intval($request->SIDA)-1,'SIFILIS'=>intval($request->SIFILIS)-1,'HEPATITIS'=>intval($request->HEPATITIS)-1,'DROGAS'=>intval($request->DROGAS)-1]);
        $ultimo=RelacionMadrePrueba::create(['ID_MADRE'=>$rest->ID_MADRE,'ID_PRUEBA'=>$otro->ID_PRUEBA]);
        $importante=RelacionMadreRegistroUsuario::create(['ID_MADRE'=>$rest->ID_MADRE,'id'=>intval($request->ID_SESION)]);
        return response()->json($rest->ID_SISTEMA, 201);
    }

    /**
     * SOLO REGRESA UN ELMENTO SI ES QUE LO ENCUENTRA
     *
     * @param  \App\madre  $madre
     * @return \Illuminate\Http\Response
     */
    public function show(madre $madre){
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\madre  $madre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, madre $madre){
      $madre->update($request->all());
      return response()->json($madre, 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\madre  $madre
     * @return \Illuminate\Http\Response
     */
    public function destroy(madre $madre)
    {
        //
    }
}
