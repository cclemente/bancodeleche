<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosificacion;
use App\RelacionBebeDosificacion;
use Illuminate\Support\Facades\DB;

class DosificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){/*OBTENER NUMERO DE TOMAS*/
        // Primero comprobaremos si estamos recibiendo todos los campos.
        if(!$request->input('id_sistema') || !$request->input('fecha')|| !$request->input('id_bebe')){
          // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
          // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
          return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
        }
        $resultado=DB::table('DOSIFICACION')
           ->join('RELACION_BEBE_DOSIFICACION','DOSIFICACION.ID_DOSIFICACION','=','RELACION_BEBE_DOSIFICACION.ID_DOSIFICACION')
           ->join('DATOS_BEBE','RELACION_BEBE_DOSIFICACION.ID_DATOS_BEBE','=','DATOS_BEBE.ID_DATOS_BEBE')
           ->select('*')
           ->where('DOSIFICACION.ID_SISTEMA','=',$request->id_sistema)
           ->where('DOSIFICACION.FECHA_DOSIFICACION','=',$request->fecha)
           ->where('DATOS_BEBE.ID_DATOS_BEBE','=',$request->id_bebe)
           ->count();
        return response()->json(['status'=>'ok','data'=>$resultado],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        // Primero comprobaremos si estamos recibiendo todos los campos.
        if(!$request->input('id_sistema') || !$request->input('fecha') || !$request->input('hora') || !$request->input('servicio') || !$request->input('cantidad_ml') || !$request->input('num_tom')|| !$request->input('admin_ml')|| !$request->input('tipo_dosificacion')|| !$request->input('id_bebe')){
          // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
          // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
          return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
        }
        $resultado=Dosificacion::create(['ID_SISTEMA'=>$request->id_sistema,'FECHA_DOSIFICACION'=>$request->fecha,'HORA_DOSIFICACION'=>$request->hora,'SERVICIO'=>$request->servicio,'CANTIDAD_DOSIFICACION_ML'=>$request->cantidad_ml,'NUMERO_TOMA'=>intval($request->num_tom)-1,'ADMINISTRACION_ML'=>$request->admin_ml,'ID_TIPO_DONACION'=>$request->tipo_dosificacion]);
        $relacion=RelacionBebeDosificacion::create(['ID_DATOS_BEBE'=>$request->id_bebe,'ID_DOSIFICACION'=>$resultado->ID_DOSIFICACION]);
        return response()->json(['status'=>'ok','data'=>$resultado],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dosificacion  $dosificacion
     * @return \Illuminate\Http\Response
     */
    public function show(Dosificacion $dosificacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dosificacion  $dosificacion
     * @return \Illuminate\Http\Response
     */
    public function edit(Dosificacion $dosificacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dosificacion  $dosificacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dosificacion $dosificacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dosificacion  $dosificacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dosificacion $dosificacion)
    {
        //
    }
}
