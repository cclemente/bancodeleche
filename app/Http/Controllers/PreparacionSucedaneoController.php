<?php

namespace App\Http\Controllers;

use App\PreparacionSucedaneo;
use App\RelacionBebePreparacion;
use Illuminate\Http\Request;

class PreparacionSucedaneoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
      // Primero comprobaremos si estamos recibiendo todos los campos.
      if (!$request->input('id_bebe')|| !$request->input('diagnostico')|| !$request->input('tipoFormula')){
        // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
        // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
        return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
      }
      $registro=PreparacionSucedaneo::create(['FECHA_REGISTRO'=>date('Y-m-d'),'ID_DIAGNOSTICO'=>$request->diagnostico,'ID_TIPO_FORMULA'=>$request->tipoFormula]);
      $relacion=RelacionBebePreparacion::create(['ID_DATOS_BEBE'=>$request->id_bebe,'ID_PREPARACION_SUCEDANEO'=>$registro->ID_PREPARACION_SUCEDANEO]);
      return response()->json(['status'=>'ok','data'=>$registro,'data'=>$relacion],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PreparacionSucedaneo  $preparacionSucedaneo
     * @return \Illuminate\Http\Response
     */
    public function show(PreparacionSucedaneo $preparacionSucedaneo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreparacionSucedaneo  $preparacionSucedaneo
     * @return \Illuminate\Http\Response
     */
    public function edit(PreparacionSucedaneo $preparacionSucedaneo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreparacionSucedaneo  $preparacionSucedaneo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PreparacionSucedaneo $preparacionSucedaneo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreparacionSucedaneo  $preparacionSucedaneo
     * @return \Illuminate\Http\Response
     */
    public function destroy(PreparacionSucedaneo $preparacionSucedaneo)
    {
        //
    }
}
