<?php

namespace App\Http\Controllers;

use App\Recepcion;
use App\Madre;
use App\RelacionMadreRecepcion;
use App\RelacionMadreRegistroUsuario;
use App\RelacionDatosBebeRecepcion;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
class RecepcionLecheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){/*ID_FRASCO que regresa ok,data fecha y hora*/
        // Primero comprobaremos si estamos recibiendo todos los campos.
        if (!$request->input('nom') || !$request->input('apPat') || !$request->input('apMat')|| !$request->input('id_sesion')){
          if (!$request->input('id_frasco') || !$request->input('tipoDonacion')) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
          }
          $encontreFrasco=Recepcion::where(['ID_FRASCO'=>$request->id_frasco,'ID_TIPO_DONACION'=>$request->tipoDonacion])->get();
          if (empty($encontreFrasco[0]->ID_FRASCO)) {
            return response()->json(['status'=>'false'],200);
          }else {
            return response()->json(['status'=>'true'],200);
          }
        }
        //Realizamos la consulta a la bd mientras el id sea la misma regresa el compo

        $DatosMadre=Madre::where([['NOMBRE', $request->nom],['APELLIDO_PATERNO', $request->apPat],['APELLIDO_MATERNO', $request->apMat]])->first();
        $valido=RelacionMadreRegistroUsuario::where(['ID_MADRE'=>$DatosMadre->ID_MADRE,'id'=>$request->id_sesion])->get();
        if (!$DatosMadre){
            // codigo 1000 (código específico de error en nuestra app)
            // código http a enviar 404 de recurso solicitado no existe.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra una madre con ese código.'])],404);
        }  //$todos=RelacionMadreRecepcion::where('ID_MADRE',$DatosMadre->ID_MADRE)->get();

        if (empty($valido[0]->id)) {
          // codigo 1000 (código específico de error en nuestra app)
          // código http a enviar 404 de recurso solicitado no existe.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra una madre con ese código.'])],404);
        }
        /*
        $todos=DB::table('MADRE')
            ->join('RELACION_MADRE_RECEPCION','MADRE.ID_MADRE','=','RELACION_MADRE_RECEPCION.ID_MADRE')
            ->join('RECEPCION', 'RECEPCION.ID_RECEPCION', '=', 'RELACION_MADRE_RECEPCION.ID_RECEPCION')
            ->join('RELACION_RECEPCION_NINIO','RELACION_RECEPCION_NINIO.ID_RECEPCION','=','RECEPCION.ID_RECEPCION')
            ->join('NINIO', 'NINIO.ID_NINIO','=','RELACION_RECEPCION_NINIO.ID_NINIO')
            ->select('NINIO.ETIQUETA')
            ->where('RECEPCION.ID_TIPO_DONACION','=','1')
            ->get();
        if (empty($todos)){
          return response()->json(['status'=>'ok','data'=>$DatosMadre,'data1'=>$todos],200);
        }*/
        return response()->json(['status'=>'ok','data'=>$DatosMadre],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *Se registrara un frasco con una cantidad de leche asociado a la madre, tipo de donacion
     *
     */
    public function store(Request $request){
      if (!$request->input('id_madre') || !$request->input('areaHospital') || !$request->input('fechaExtraccion')|| !$request->input('horaExtraccion') || !$request->input('tipoDonacion') || !$request->input('cantidad')){
          return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
      }
      $DatosMadre=Madre::where('ID_SISTEMA', $request->id_madre)->first();
      $etiquetaFrasco=$DatosMadre->ID_SISTEMA.'-'.$request->fechaExtraccion[5].$request->fechaExtraccion[6].$request->fechaExtraccion[7].$request->fechaExtraccion[8].$request->fechaExtraccion[9].'-'.rand(0,9).(string)rand(0,9).(string)rand(0,9);
      $rest=Recepcion::create(['ID_FRASCO'=>$etiquetaFrasco,'AREA_HOSPITALIZACION'=>$request->areaHospital,'FECHA_EXTRACCION'=>$request->fechaExtraccion,'HORA_EXTRACCION'=>$request->horaExtraccion,'ID_TIPO_DONACION'=>intval($request->tipoDonacion),'CANTIDAD_RECIBIDA'=>$request->cantidad]);
      $relacion=RelacionMadreRecepcion::create(['ID_MADRE'=>$DatosMadre->ID_MADRE,'ID_RECEPCION'=>$rest->ID_RECEPCION]);
      if ($request->tipoDonacion==1) {
        if (!$request->input('id_bebe')) {
          // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
          // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
          return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
        }
        $relaciondos=RelacionDatosBebeRecepcion::create(['ID_DATOS_BEBE'=>intval($request->id_bebe),'ID_RECEPCION'=>$rest->ID_RECEPCION]);
      }
      return response()->json(['status'=>'ok','data'=>$etiquetaFrasco],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recepcion  $recepcion
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request){
		   return "Se muestra Fabricante con id: $id";
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recepcion  $recepcion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recepcion $recepcion){
      return "Se muestra Fabricante con put: $request";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recepcion  $recepcion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recepcion $recepcion){
      return "Se muestra Fabricante con delete: $id";
    }
}
