<?php

namespace App\Http\Controllers;

use App\Pasteurizacion;
use App\Analisis;
use Illuminate\Http\Request;
use App\Recepcion;
class PasteurizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
      // Primero comprobaremos si estamos recibiendo todos los campos.
      if (!$request->input('id_etiqueta')){
          // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
          // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
          return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
      }
      $resultAnalisis=Analisis::where(['ID_ETIQUETA'=>$request->id_etiqueta])->first();
      if (empty($resultAnalisis->ID_ETIQUETA)) {
        return response()->json(['status'=>'false'],200);
      }else {
        $result=Recepcion::where(['ID_RECEPCION'=>$resultAnalisis->ID_RECEPCION])->first();
        if ($result->ID_TIPO_DONACION==1) {
          return response()->json(['status'=>'false'],200);
        }
        return response()->json(['status'=>$resultAnalisis->CANTIDAD_ALMACENADA],200);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        if (!$request->input('id_etiqueta') || !$request->input('acidez_dornic')|| !$request->input('pasteurizada')|| !$request->input('cantidad_pasteurizada')|| !$request->input('cantidad_desechada')) {
          // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
          // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
          return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
        }
        if ($request->cantidad_desechada==-1) {
          $request->cantidad_desechada=0;
        }
        if ($request->cantidad_pasteurizada==-1) {
          $request->cantidad_pasteurizada=0;
        }
        $result=Pasteurizacion::create(['ID_ETIQUETA'=>$request->id_etiqueta,'ETIQUETA_PASTEURIZACION'=>$request->id_etiqueta.'PAST','ACIDEZ_DORNIC'=>intval($request->acidez_dornic)-1,'PASTEURIZADA'=>intval($request->pasteurizada)-1,'CANTIDAD_PASTEURIZADA'=>$request->cantidad_pasteurizada,'CANTIDAD_DESECHADA'=>$request->cantidad_desechada]);
        return response()->json(['status'=>'ok','data'=>$result],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pasteurizacion  $pasteurizacion
     * @return \Illuminate\Http\Response
     */
    public function show(Pasteurizacion $pasteurizacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pasteurizacion  $pasteurizacion
     * @return \Illuminate\Http\Response
     */
    public function edit(Pasteurizacion $pasteurizacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pasteurizacion  $pasteurizacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pasteurizacion $pasteurizacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pasteurizacion  $pasteurizacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pasteurizacion $pasteurizacion)
    {
        //
    }
}
