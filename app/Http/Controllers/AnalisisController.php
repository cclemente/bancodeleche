<?php

namespace App\Http\Controllers;

use App\Analisis;
use App\Recepcion;
use Illuminate\Http\Request;

class AnalisisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
      // Primero comprobaremos si estamos recibiendo todos los campos.
      if (!$request->input('tipoClasificacion') || !$request->input('embalaje') || !$request->input('color') || !$request->input('suciedad') || !$request->input('olor') || !$request->input('cantidad_analizada') || !$request->input('cantidad_desechada') || !$request->input('cantidad_almacenada') || !$request->input('id_frasco')){
        // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
        // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
        return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso de alta.'])],422);
      }
      $encontreFrasco=Recepcion::where(['ID_FRASCO'=>$request->id_frasco])->get();
      //$etiqueta="asdasd";
      $etiqueta=substr($encontreFrasco[0]->ID_FRASCO,0,-5).(string)rand(0,9).(string)rand(0,9).rand(0,9).(string)rand(0,9).(string)rand(0,9);

      if ($request->cantidad_desechada==-1) {
        $request->cantidad_desechada=0;
      }
      if ($request->cantidad_almacenada==-1) {
        $request->cantidad_almacenada=0;
        $etiqueta=null;
      }
      if ($request->input('crematrocito') && $request->input('kilo_calorias')&& $request->input('proteinas')&& $request->input('carbohidratos')&& $request->input('grasas') ) {
        $registro=Analisis::create(['ID_CLASIFICACION'=>$request->tipoClasificacion,'EMBALAJE'=>intval($request->embalaje)-1,'COLOR'=>intval($request->color)-1,'SUCIEDAD'=>intval($request->suciedad)-1,'OLOR'=>intval($request->olor)-1,'CREMATROCITO'=>$request->crematrocito,'KILO_CALORIAS'=>$request->kilo_calorias,'PROTEINAS'=>$request->proteinas,'CARBOHIDRATOS'=>$request->carbohidratos,'GRASAS'=>$request->grasas,'CANTIDAD_ANALIZADA'=>$request->cantidad_analizada,'CANTIDAD_DESECHADA'=>$request->cantidad_desechada,'CANTIDAD_ALMACENADA'=>$request->cantidad_almacenada,'ID_RECEPCION'=>$encontreFrasco[0]->ID_RECEPCION,'ID_ETIQUETA'=>$etiqueta]);
      }else {
        $registro=Analisis::create(['ID_CLASIFICACION'=>$request->tipoClasificacion,'EMBALAJE'=>intval($request->embalaje)-1,'COLOR'=>intval($request->color)-1,'SUCIEDAD'=>intval($request->suciedad)-1,'OLOR'=>intval($request->olor)-1,'CANTIDAD_ANALIZADA'=>$request->cantidad_analizada,'CANTIDAD_DESECHADA'=>$request->cantidad_desechada,'CANTIDAD_ALMACENADA'=>$request->cantidad_almacenada,'ID_RECEPCION'=>$encontreFrasco[0]->ID_RECEPCION,'ID_ETIQUETA'=>$etiqueta]);

      }
      return response()->json(['status'=>'ok','data'=>$registro->ID_ETIQUETA],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Analisis  $analisis
     * @return \Illuminate\Http\Response
     */
    public function show(Analisis $analisis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Analisis  $analisis
     * @return \Illuminate\Http\Response
     */
    public function edit(Analisis $analisis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Analisis  $analisis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Analisis $analisis){
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Analisis  $analisis
     * @return \Illuminate\Http\Response
     */
    public function destroy(Analisis $analisis)
    {
        //
    }
}
