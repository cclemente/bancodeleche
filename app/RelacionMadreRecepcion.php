<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelacionMadreRecepcion extends Model{
    protected $table = 'RELACION_MADRE_RECEPCION';
    // NOMBRE DE LAS COLUMNOS DE LA TABLA
    /*
      $table->integer('ID_MADRE')->unsigned();
      $table->foreign('ID_MADRE')->references('ID_MADRE')->on('MADRE');
      $table->integer('ID_RECEPCION')->unsigned();
      $table->foreign('ID_RECEPCION')->references('ID_RECEPCION')->on('RECEPCION');
    */
    public $timestamps = false;
    protected $primaryKey = 'ID_RELACION_MADRE_RECPCION';
    protected $fillable = array('ID_MADRE','ID_RECEPCION');
    protected $hidden = ['created_at','updated_at'];

    public function relacionmadrerecepcin(){
      // 1 fabricante tiene muchos aviones
      // $this hace referencia al objeto que tengamos en ese momento de Fabricante.
      return $this->hasOne('App\Madre','App\Recepcion');
    }
}
