<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prueba extends Model{
  protected $table = 'PRUEBA';
  // NOMBRE DE LAS COLUMNOS DE LA TABLA
  /*
  $table->boolean('SIDA');
  $table->boolean('SIFILIS');
  $table->boolean('HEPATITIS');
  $table->boolean('DROGAS');

  */
  public $timestamps = false;
  protected $primaryKey = 'ID_PRUEBA';
  protected $fillable = array('SIDA','SIFILIS','HEPATITIS','DROGAS');
  protected $hidden = ['created_at','updated_at'];
  public function relacionmadreprueba(){
  // 1 fabricante tiene muchos aviones
  // $this hace referencia al objeto que tengamos en ese momento de Fabricante.
  return $this->hasOne('App\RelacionMadrePrueba');
}
}
