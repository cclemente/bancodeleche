<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreparacionSucedaneo extends Model{
  // NOMBRE DE LA TABLA EN LA BASE DE DATOS
  protected $table = 'PREPARACION_SUCEDANEO';
  // NOMBRE DE LAS COLUMNOS DE LA TABLA
  /*
  $table->increments('ID_PREPARACION_SUCEDANEO');
  $table->date('FECHA_REGISTRO');
  $table->integer('ID_DIAGNOSTICO')->unsigned();
  $table->foreign('ID_DIAGNOSTICO')->references('ID_DIAGNOSTICO')->on('DIAGNOSTICO');
  $table->integer('ID_TIPO_FORMULA')->unsigned();
  $table->foreign('ID_TIPO_FORMULA')->references('ID_TIPO_FORMULA')->on('TIPO_FORMULA');
  */
  public $timestamps = false;
  protected $primaryKey = 'ID_PREPARACION_SUCEDANEO';
  protected $fillable = array('FECHA_REGISTRO','ID_DIAGNOSTICO','ID_TIPO_FORMULA');
  protected $hidden = ['created_at','updated_at'];
  // Definimos a continuación la relación de esta tabla con otras.
  // Ejemplos de relaciones:
  // 1 usuario tiene 1 teléfono   ->hasOne() Relación 1:1
  // 1 teléfono pertenece a 1 usuario   ->belongsTo() Relación 1:1 inversa a hasOne()
  // 1 post tiene muchos comentarios  -> hasMany() Relación 1:N
  // 1 comentario pertenece a 1 post ->belongsTo() Relación 1:N inversa a hasMany()
  // 1 usuario puede tener muchos roles  ->belongsToMany()
  //  etc..
  public function relaciontipoDiag(){
    return $this->hasOne('App\TipoFormula','App\Diagnostico','App\RelacionBebePreparacion');
  }
}
