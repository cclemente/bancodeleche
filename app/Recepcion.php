<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recepcion extends Model{
    // NOMBRE DE LA TABLA EN LA BASE DE DATOS
    protected $table = 'RECEPCION';
    // NOMBRE DE LAS COLUMNOS DE LA TABLA
    /*
    $table->string('ID_FRASCO');
    $table->string('AREA_HOSPITALIZACION');
    $table->date('FECHA_EXTRACCION');
    $table->time('HORA_EXTRACCION');
    $table->integer('ID_TIPO_DONACION')->unsigned();
    $table->foreign('ID_TIPO_DONACION')->references('ID_TIPO_DONACION')->on('TIPO_DONACION');
    $table->decimal('CANTIDAD_RECIBIDA');
    */
    protected $primaryKey = 'ID_RECEPCION';
    public $timestamps = false;
    protected $fillable = array('ID_FRASCO','AREA_HOSPITALIZACION','FECHA_EXTRACCION','HORA_EXTRACCION','ID_TIPO_DONACION','CANTIDAD_RECIBIDA');
    protected $hidden = ['created_at','updated_at'];
    // Definimos a continuación la relación de esta tabla con otras.
    // Ejemplos de relaciones:
    // 1 usuario tiene 1 teléfono   ->hasOne() Relación 1:1
    // 1 teléfono pertenece a 1 usuario   ->belongsTo() Relación 1:1 inversa a hasOne()
    // 1 post tiene muchos comentarios  -> hasMany() Relación 1:N
    // 1 comentario pertenece a 1 post ->belongsTo() Relación 1:N inversa a hasMany()
    // 1 usuario puede tener muchos roles  ->belongsToMany()
    //  etc..
    public function relacionmadreprueba(){
      return $this->hasOne('App\TipoDonacion');
    }
}
