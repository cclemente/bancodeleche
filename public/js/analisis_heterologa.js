// funcion principal de jquery una vez cargado del DOM
$(document).ready(function() {
  // validaciones del formulario buscador
  $("#form-buscador-analisis-heterologa").validate({
    submitHandler: function(form) {
      buscarFrascoPorId();
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") == "inputIdRecepcionLeche") {
        error.appendTo('#error-input-id-recepcion-leche');
      } else {
        error.insertAfter(element);
      }
    },
    rules: {
      inputIdRecepcionLeche: {
        required: true
      },
      selectTipoLecheAnalisis: {
        required: true
      }
    },
    messages: {
      inputIdRecepcionLeche: {
        required: "Debe indicar el ID a buscar"
      },
      selectTipoLecheAnalisis: {
        required: "Debe seleccionar un tipo"
      }
    }
  });

  // validaciones del formulario del analisis
  $("#form-analisis-heterologa").validate({
    submitHandler: function(form) {
      enviarFormAnalisis();
    },
    rules: {
      selectClasificacion: {
        required: true
      },
      selectEmbalaje: {
        required: true
      },
      selectColor: {
        required: true
      },
      selectSuciedad: {
        required: true
      },
      selectOlor: {
        required: true
      },
      inputCrematrocito: {
        required: true,
        regexdecimales: /^[0-9]*(\.[0-9]+)?$/
      },
      inputKilocalorias: {
        required: true,
        regexdecimales: /^[0-9]*(\.[0-9]+)?$/
      },
      inputProteinas: {
        required: true,
        regexdecimales: /^[0-9]*(\.[0-9]+)?$/
      },
      inputCarbohidratos: {
        required: true,
        regexdecimales: /^[0-9]*(\.[0-9]+)?$/
      },
      inputGrasas: {
        required: true,
        regexdecimales: /^[0-9]*(\.[0-9]+)?$/
      },
      inputCantidad: {
        required: true,
        regexdecimales: /^[0-9]*(\.[0-9]+)?$/
      },
      inputAlmacenar: {
        required: true,
        regexdecimales: /^[0-9]*(\.[0-9]+)?$/
      }
    },
    messages: {
      selectClasificacion: {
        required: "Seleccione una opción"
      },
      selectEmbalaje: {
        required: "Seleccione una opción"
      },
      selectColor: {
        required: "Seleccione una opción"
      },
      selectSuciedad: {
        required: "Seleccione una opción"
      },
      selectOlor: {
        required: "Seleccione una opción"
      },
      inputCrematrocito: {
        required: "Este campo es requerido",
        regexdecimales: "Ingrese un número válido"
      },
      inputKilocalorias: {
        required: "Este campo es requerido",
        regexdecimales: "Ingrese un número válido"
      },
      inputProteinas: {
        required: "Este campo es requerido",
        regexdecimales: "Ingrese un número válido"
      },
      inputCarbohidratos: {
        required: "Este campo es requerido",
        regexdecimales: "Ingrese un número válido"
      },
      inputGrasas: {
        required: "Este campo es requerido",
        regexdecimales: "Ingrese un número válido"
      },
      inputCantidad: {
        required: "Este campo es requerido",
        regexdecimales: "Ingrese un número válido"
      },
      inputAlmacenar: {
        required: "Este campo es requerido",
        regexdecimales: "Ingrese un número válido"
      }
    }
  });
  $.validator.addMethod(
    "regexdecimales",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    },
    "Ingrese un número válido"
  );
});

// funcion ejecutada cuando se inserta un id de frasco de leche a buscar
function buscarFrascoPorId() {
  var idFrasco = $("#inputIdRecepcionLeche").val();
  var tipoLeche = Number($("#selectTipoLecheAnalisis").val());

  // animacion para el boton de buscar
  $('.btn-buscar').children().removeClass("fa-save").addClass("fa-sync fa-spin");
  // se deshabilita el boton mientras se procesa la peticion
  $('.btn-buscar').prop("disabled", true);

  // peticion http para determinar si existe o no el frasco
  $.ajax({
		type: 'GET',
		url: 'http://127.0.0.1:8000/api/recepciones',
		async: true,
		data:(
      'id_frasco=' + idFrasco +
      '&tipoDonacion=' + tipoLeche
    ),
		success: function (respuesta) {
      // devolvemos las clases y estado del boton a la normalidad
      $('.btn-buscar').children().removeClass("fa-sync fa-spin").addClass("fa-save");
      $('.btn-buscar').prop("disabled", false);

      // si la respuesta es true se habilita el formulario de analisis y boton de guardar
			if (respuesta.status == "true") {
        limpiarFormularioAnalisis();
        $('.btn-guardar').prop("disabled", false);
        $("#frasco-encontrado-analisis").removeClass("text-danger").addClass("text-success");
        $("#frasco-encontrado-analisis").val(idFrasco);

        // dependiendo del tipo de leche se configura el formulario de una forma u otra
        if (tipoLeche == 1) {
          habilitarFormHomologa();
        } else if (tipoLeche == 2) {
          habilitarFormHeterologa();
        }
        sessionStorage.setItem("tipoLeche", tipoLeche);   // se guarda para cuando se envie el formulario
      } else if (respuesta.status == "false") {
        limpiarFormularioAnalisis();
        deshabilitarFormularioAnalisis();
        $('.btn-guardar').prop("disabled", true);
        $("#frasco-encontrado-analisis").val("No Encontrado");
        $("#frasco-encontrado-analisis").removeClass("text-success").addClass("text-danger");
      }
      console.log(respuesta.status);
		}
	});
}

function deshabilitarFormularioAnalisis() {
  $("#selectClasificacion").prop("disabled", true);
  $("#selectEmbalaje").prop("disabled", true);
  $("#selectColor").prop("disabled", true);
  $("#selectSuciedad").prop("disabled", true);
  $("#selectOlor").prop("disabled", true);
  $("#inputCrematrocito").prop("disabled", true);
  $("#inputKilocalorias").prop("disabled", true);
  $("#inputProteinas").prop("disabled", true);
  $("#inputCarbohidratos").prop("disabled", true);
  $("#inputGrasas").prop("disabled", true);
  $("#inputCantidad").prop("disabled", true);
  $("#inputAlmacenar").prop("disabled", true);
}

function habilitarFormHomologa() {
  $("#selectClasificacion").prop("disabled", false);
  $("#selectEmbalaje").prop("disabled", false);
  $("#selectColor").prop("disabled", false);
  $("#selectSuciedad").prop("disabled", false);
  $("#selectOlor").prop("disabled", false);
  $("#inputCrematrocito").prop("disabled", true);
  $("#inputKilocalorias").prop("disabled", true);
  $("#inputProteinas").prop("disabled", true);
  $("#inputCarbohidratos").prop("disabled", true);
  $("#inputGrasas").prop("disabled", true);
  $("#inputCantidad").prop("disabled", false);
  $("#inputAlmacenar").prop("disabled", false);
}

function habilitarFormHeterologa() {
  $("#selectClasificacion").prop("disabled", false);
  $("#selectEmbalaje").prop("disabled", false);
  $("#selectColor").prop("disabled", false);
  $("#selectSuciedad").prop("disabled", false);
  $("#selectOlor").prop("disabled", false);
  $("#inputCrematrocito").prop("disabled", false);
  $("#inputKilocalorias").prop("disabled", false);
  $("#inputProteinas").prop("disabled", false);
  $("#inputCarbohidratos").prop("disabled", false);
  $("#inputGrasas").prop("disabled", false);
  $("#inputCantidad").prop("disabled", false);
  $("#inputAlmacenar").prop("disabled", false);
}

// funcion ejecutada cuando el formulario enviado es valido
function enviarFormAnalisis() {
  // animacion para el boton de guardado
  $('.btn-guardar').children().removeClass("fa-save").addClass("fa-sync fa-spin");
  // se deshabilita el boton mientras se procesa la peticion
  $('.btn-guardar').prop("disabled", true);
  var tipoLeche = Number(sessionStorage.getItem("tipoLeche"));
  var idFrasco = $("#inputIdRecepcionLeche").val();

  // la estructura de la peticion cambia segun el tipo de donacion seleccionada (homologa=1)
  if (tipoLeche == 1) {
    var form = obtenerValoresFormAnalisisHomologa();
    if (form.cantidadAlmacenar == 0) {
      form.cantidadAlmacenar = -1;
    }
    if (form.cantidadDesechada == 0) {
      form.cantidadDesechada = -1;
    }
    $.ajax({
      type: 'POST',
      url: 'http://127.0.0.1:8000/api/analisis',
      async: true,
      data:(
        'tipoClasificacion=' + form.clasificacion +
        '&embalaje=' + form.embalaje +
        '&color=' + form.color +
        '&suciedad=' + form.suciedad +
        '&olor=' + form.olor +
        '&cantidad_analizada=' + form.cantidad +
        '&cantidad_desechada=' + form.cantidadDesechada +
        '&cantidad_almacenada=' + form.cantidadAlmacenar +
        '&id_frasco=' + idFrasco
      ),
      success: function (respuesta) {
        // devolvemos las clases y estado del boton a la normalidad
        $('.btn-guardar').children().removeClass("fa-sync fa-spin").addClass("fa-save");
        $('.btn-guardar').prop("disabled", false);
  
        if (respuesta.data) {
          Swal.fire({
            icon: 'success',
            title: 'Registro Guardado!',
            text: 'ID ANÁLISIS: ' + respuesta.data
          });
        } else {
          Swal.fire({
            icon: 'success',
            title: 'Registro Guardado!',
            text: 'Tota la leche analizada se ha registrado como desechada'
          });
        }

        limpiarBuscadorAnalisis();
        limpiarFormularioAnalisis();
        deshabilitarFormularioAnalisis();
        sessionStorage.removeItem("tipoLeche");
        $('.btn-guardar').prop("disabled", true);
      }
    });
  } else if(tipoLeche == 2) {
    var form = obtenerValoresFormAnalisisHeterologa();
    if (form.cantidadAlmacenar == 0) {
      form.cantidadAlmacenar = -1;
    }
    if (form.cantidadDesechada == 0) {
      form.cantidadDesechada = -1;
    }
    $.ajax({
      type: 'POST',
      url: 'http://127.0.0.1:8000/api/analisis',
      async: true,
      data:(
        'tipoClasificacion=' + form.clasificacion +
        '&embalaje=' + form.embalaje +
        '&color=' + form.color +
        '&suciedad=' + form.suciedad +
        '&olor=' + form.olor +
        '&crematrocito=' + form.crematrocito +
        '&kilo_calorias=' + form.kilocal +
        '&proteinas=' + form.proteinas +
        '&carbohidratos=' + form.carbohidratos +
        '&grasas=' + form.grasas +
        '&cantidad_analizada=' + form.cantidad +
        '&cantidad_desechada=' + form.cantidadDesechada +
        '&cantidad_almacenada=' + form.cantidadAlmacenar +
        '&id_frasco=' + idFrasco
      ),
      success: function (respuesta) {
        // devolvemos las clases y estado del boton a la normalidad
        $('.btn-guardar').children().removeClass("fa-sync fa-spin").addClass("fa-save");
        $('.btn-guardar').prop("disabled", false);
  
        if (respuesta.data) {
          Swal.fire({
            icon: 'success',
            title: 'Registro Guardado!',
            text: 'ID ANÁLISIS: ' + respuesta.data
          });
        } else {
          Swal.fire({
            icon: 'success',
            title: 'Registro Guardado!',
            text: 'Tota la leche analizada se ha registrado como desechada'
          });
        }

        limpiarBuscadorAnalisis();
        limpiarFormularioAnalisis();
        deshabilitarFormularioAnalisis();
        sessionStorage.removeItem("tipoLeche");
        $('.btn-guardar').prop("disabled", true);
      }
    });
  }
}

// construye un objeto con los datos de analisis homologa
function obtenerValoresFormAnalisisHomologa() {
  valoresForm = {
    clasificacion: Number($("#selectClasificacion").val()),
    embalaje: Number($("#selectEmbalaje").val()),
    color: Number($("#selectColor").val()),
    suciedad: Number($("#selectSuciedad").val()),
    olor: Number($("#selectOlor").val()),
    cantidad: Number($("#inputCantidad").val()),
    cantidadAlmacenar: Number($("#inputAlmacenar").val()),
    cantidadDesechada: Number($("#inputCantidad").val()) - Number($("#inputAlmacenar").val())
  }

  return valoresForm;
}

// construye un objeto con todos los datos del formulario
function obtenerValoresFormAnalisisHeterologa() {
  valoresForm = {
    clasificacion: Number($("#selectClasificacion").val()),
    embalaje: Number($("#selectEmbalaje").val()),
    color: Number($("#selectColor").val()),
    suciedad: Number($("#selectSuciedad").val()),
    olor: Number($("#selectOlor").val()),
    crematrocito: Number($("#inputCrematrocito").val()),
    kilocal: Number($("#inputKilocalorias").val()),
    proteinas: Number($("#inputProteinas").val()),
    carbohidratos: Number($("#inputCarbohidratos").val()),
    grasas: Number($("#inputGrasas").val()),
    cantidad: Number($("#inputCantidad").val()),
    cantidadAlmacenar: Number($("#inputAlmacenar").val()),
    cantidadDesechada: Number($("#inputCantidad").val()) - Number($("#inputAlmacenar").val())
  }

  return valoresForm;
}

function limpiarBuscadorAnalisis() {
  $("#inputIdRecepcionLeche").val("");
  $("#selectTipoLecheAnalisis").val("");
  $("#frasco-encontrado-analisis").val("");
  $("#frasco-encontrado-analisis").removeClass("text-success");
}

function limpiarFormularioAnalisis() {
  $("#selectClasificacion").val("");
  $("#selectEmbalaje").val("");
  $("#selectColor").val("");
  $("#selectSuciedad").val("");
  $("#selectOlor").val("");
  $("#inputCrematrocito").val("");
  $("#inputKilocalorias").val("");
  $("#inputProteinas").val("");
  $("#inputCarbohidratos").val("");
  $("#inputGrasas").val("");
  $("#inputCantidad").val("");
  $("#inputAlmacenar").val("");
}
