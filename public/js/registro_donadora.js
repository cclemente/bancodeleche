// jquery funcion principal
$(document).ready(function() {
  // validaciones del formulario
  $("#form-registro-donadora").validate({
    submitHandler: function(form) {
      enviarFormRegistroDonadora();
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") == "nombre") {
        error.appendTo('#error-nombre');
      } else {
        error.insertAfter(element);
      }
    },
    rules: {
      nombre: {
        required: true,
        minlength: 3,
        maxlength: 255,
        regexnombre: /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/
      },
      apellidoPaterno: {
        required: true,
        minlength: 3,
        maxlength: 255,
        regexnombre: /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/
      },
      apellidoMaterno: {
        required: true,
        minlength: 3,
        maxlength: 255,
        regexnombre: /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/
      },
      vih: {
        required: true
      },
      sifilis: {
        required: true
      },
      hepatitisC: {
        required: true
      },
      drogas: {
        required: true
      }
    },
    messages: {
      nombre: {
        required: "Debe indicar su nombre",
        minlength: "El nombre debe contener al menos 3 letras",
        maxlength: "El nombre puede contener máximo 255 letras",
        regexnombre: "Solo se permiten letras"
      },
      apellidoPaterno: {
        required: "Debe indicar su apellido paterno",
        minlength: "El apellido debe contener al menos 3 letras",
        maxlength: "El apellido puede contener máximo 255 letras",
        regexnombre: "Solo se permiten letras"
      },
      apellidoMaterno: {
        required: "Debe indicar su apellido materno",
        minlength: "El apellido debe contener al menos 3 letras",
        maxlength: "El apellido puede contener máximo 255 letras",
        regexnombre: "Solo se permiten letras"
      },
      vih: {
        required: "Debe seleccionar una opción"
      },
      sifilis: {
        required: "Debe seleccionar una opción"
      },
      hepatitisC: {
        required: "Debe seleccionar una opción"
      },
      drogas: {
        required: "Debe seleccionar una opción"
      }
    }
  });
  $.validator.addMethod(
    "regexnombre",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    },
    "Solo se permiten letras"
  );
});

// funcion ejecutada cuando el formulario enviado es valido
function enviarFormRegistroDonadora() {
  // animacion para el boton de guardado
  $('.btn-guardar').children().removeClass("fa-save").addClass("fa-sync fa-spin");
  // se deshabilita el boton mientras se procesa la peticion
  $('.btn-guardar').prop("disabled", true);

  // obtenemos los datos del formulario
  var form = this.obtenerValoresFormRegistroDonadora();
  $.ajax({
		type: 'POST',
		url: 'http://127.0.0.1:8000/api/madres',
		async: true,
		data:(
      'NOMBRE=' + form.nombre +
      '&APELLIDO_PATERNO=' + form.apPat +
      '&APELLIDO_MATERNO=' + form.apMat +
      '&SIDA=' + form.vih +
      '&SIFILIS=' + form.sifilis +
      '&HEPATITIS=' + form.hepatitis +
      '&DROGAS=' + form.drogas +
      '&ID_SESION=' + form.idSesion
    ),
		success: function (respuesta) {
      // devolvemos las clases y estado del boton a la normalidad
      $('.btn-guardar').children().removeClass("fa-sync fa-spin").addClass("fa-save");
      $('.btn-guardar').prop("disabled", false);
			Swal.fire({
        icon: 'success',
        title: 'Registro Guardado!',
        text: 'ID MADRE: ' + respuesta
      });
      limpiarFormRegistroDonadora();
    },
    error: function(resp) {
      console.log(resp);
    }
	});
}

function obtenerValoresFormRegistroDonadora() {
  valoresForm = {
    idSesion: Number($("#label-id-sesion").attr("name")),
    nombre: $("#nombre").val(),
    apPat: $("#apellidoPaterno").val(),
    apMat: $("#apellidoMaterno").val(),
    vih: Number($("#vih").val()),
    sifilis: Number($("#sifilis").val()),
    hepatitis: Number($("#hepatitisC").val()),
    drogas: Number($("#drogas").val())
  }

  return valoresForm;
}

function limpiarFormRegistroDonadora() {
  $("#nombre").val('');
  $("#apellidoPaterno").val('');
  $("#apellidoMaterno").val('');
  $("#vih").val('');
  $("#sifilis").val('');
  $("#hepatitisC").val('');
  $("#drogas").val('');
}
