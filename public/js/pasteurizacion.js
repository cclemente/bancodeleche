// funcion principal de jquery una vez cargado del DOM
$(document).ready(function() {
  // validaciones del formulario buscador
  $("#form-buscador-pasteurizacion").validate({
    submitHandler: function(form) {
      buscarFrascoAnalizadoPorId();
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") == "inputIdAnalisisLeche") {
        error.appendTo('#error-input-id-analisis-leche');
      } else {
        error.insertAfter(element);
      }
    },
    rules: {
      inputIdAnalisisLeche: {
        required: true
      }
    },
    messages: {
      inputIdAnalisisLeche: {
        required: "Debe indicar el ID a buscar"
      }
    }
  });

  // validaciones del formulario de pasteurizacion
  $("#form-resultados-pasteurizacion").validate({
    submitHandler: function(form) {
      enviarFormPasteurizacion();
    },
    rules: {
      selectAcidez: {
        required: true
      },
      selectPasteurizacion: {
        required: true
      },
      inputAlmacenarPast: {
        required: true,
        regexdecimales: /^[0-9]*(\.[0-9]+)?$/
      }
    },
    messages: {
      selectAcidez: {
        required: "Seleccione una opción"
      },
      selectPasteurizacion: {
        required: "Seleccione una opción"
      },
      inputAlmacenarPast: {
        required: "Este campo es requerido",
        regexdecimales: "Ingrese un número válido"
      }
    }
  });
  $.validator.addMethod(
    "regexdecimales",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    },
    "Ingrese un número válido"
  );
});

// funcion ejecutada cuando se inserta un id de frasco de leche analizada a buscar
function buscarFrascoAnalizadoPorId() {
  var idFrasco = $("#inputIdAnalisisLeche").val();

  // animacion para el boton de buscar
  $('.btn-buscar').children().removeClass("fa-save").addClass("fa-sync fa-spin");
  // se deshabilita el boton mientras se procesa la peticion
  $('.btn-buscar').prop("disabled", true);

  // peticion http para determinar si existe o no el frasco (debe ser forzosamente heterologa)
  $.ajax({
		type: 'GET',
		url: 'http://127.0.0.1:8000/api/pasteurizaciones',
		async: true,
		data:(
      'id_etiqueta=' + idFrasco
    ),
		success: function (respuesta) {
      // devolvemos las clases y estado del boton a la normalidad
      $('.btn-buscar').children().removeClass("fa-sync fa-spin").addClass("fa-save");
      $('.btn-buscar').prop("disabled", false);
      limpiarFormPasteurizacion();

      // si la respuesta es diferente de false se habilita el formulario de pasteurizacion
			if (respuesta.status != "false") {
        $('.btn-guardar').prop("disabled", false);
        $("#frasco-encontrado-pasteurizacion").removeClass("text-danger").addClass("text-success");
        $("#frasco-encontrado-pasteurizacion").val(idFrasco);

        habilitarFormPasteurizacion();
        sessionStorage.setItem("cantidadAnalizada", respuesta.status);
      } else if (respuesta.status == "false") {
        deshabilitarFormPasteurizacion();
        $('.btn-guardar').prop("disabled", true);
        $("#frasco-encontrado-pasteurizacion").val("No Encontrado");
        $("#frasco-encontrado-pasteurizacion").removeClass("text-success").addClass("text-danger");
      }
      console.log(respuesta.status);
		}
  });
}

function habilitarFormPasteurizacion() {
  $("#selectAcidez").prop("disabled", false);
  $("#selectPasteurizacion").prop("disabled", false);
  $("#inputAlmacenarPast").prop("disabled", false);
}

function deshabilitarFormPasteurizacion() {
  $("#selectAcidez").prop("disabled", true);
  $("#selectPasteurizacion").prop("disabled", true);
  $("#inputAlmacenarPast").prop("disabled", true);
}

// funcion ejecutada cuando el formulario pasteurizacion es valido
function enviarFormPasteurizacion() {
  // animacion para el boton de guardado
  $('.btn-guardar').children().removeClass("fa-save").addClass("fa-sync fa-spin");
  // se deshabilita el boton mientras se procesa la peticion
  $('.btn-guardar').prop("disabled", true);
  var idFrasco = $("#inputIdAnalisisLeche").val();

  // obtenemos los datos del formulario
  var form = obtenerDatosFormPasteurizacion();
  if (form.cantidadAlmacenada == 0) {
    form.cantidadAlmacenada = -1;
  }
  if (form.cantidadDesechada == 0) {
    form.cantidadDesechada = -1;
  }
  $.ajax({
    type: 'POST',
    url: 'http://127.0.0.1:8000/api/pasteurizaciones',
    async: true,
    data:(
      'id_etiqueta=' + idFrasco +
      '&acidez_dornic=' + form.acidez +
      '&pasteurizada=' + form.pasteurizacion +
      '&cantidad_pasteurizada=' + form.cantidadAlmacenada +
      '&cantidad_desechada=' + form.cantidadDesechada
    ),
    success: function (respuesta) {
      // devolvemos las clases y estado del boton a la normalidad
      $('.btn-guardar').children().removeClass("fa-sync fa-spin").addClass("fa-save");
      $('.btn-guardar').prop("disabled", false);

      if (form.cantidadAlmacenada != -1) {
        Swal.fire({
          icon: 'success',
          title: 'Registro Guardado!',
          text: 'ID PASTEURIZACIÓN: ' + respuesta.data.ETIQUETA_PASTEURIZACION
        });
      } else {
        Swal.fire({
          icon: 'success',
          title: 'Registro Guardado!',
          text: 'Toda la leche pasteurizada se ha desechado'
        });
      }

      limpiarBuscadorPasteurizacion();
      limpiarFormPasteurizacion();
      deshabilitarFormPasteurizacion();
      sessionStorage.removeItem("cantidadAnalizada");
      $('.btn-guardar').prop("disabled", true);
    }
  });
}

function obtenerDatosFormPasteurizacion() {
  var valoresForm = {
    acidez: Number($("#selectAcidez").val()),
    pasteurizacion: Number($("#selectPasteurizacion").val()),
    cantidadAlmacenada: Number($("#inputAlmacenarPast").val()),
    cantidadDesechada: Number(sessionStorage.getItem("cantidadAnalizada")) - Number($("#inputAlmacenarPast").val())
  };

  return valoresForm;
}

function limpiarBuscadorPasteurizacion() {
  $("#inputIdAnalisisLeche").val("");
  $("#frasco-encontrado-pasteurizacion").val("");
  $("#frasco-encontrado-pasteurizacion").removeClass("text-success");
}

function limpiarFormPasteurizacion() {
  $("#selectAcidez").val("");
  $("#selectPasteurizacion").val("");
  $("#inputAlmacenarPast").val("");
}
