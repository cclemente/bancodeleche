// jquery funcion principal
$(document).ready(function() {
  // validaciones del formulario
  $("#form-recepcion-leche").validate({
    submitHandler: function(form) {
      //enviarForm2();
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") == "nombre") {
        error.appendTo('#error-nombre-recepcion');
      } else {
        error.insertAfter(element);
      }
    },
    rules: {
      nombreRecepcion: {
        required: true,
        minlength: 3,
        maxlength: 255,
        regexnombre: /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/
      },
      apellidoPaternoRecepcion: {
        required: true,
        minlength: 3,
        maxlength: 255,
        regexnombre: /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/
      },
      apellidoMaternoRecepcion: {
        required: true,
        minlength: 3,
        maxlength: 255,
        regexnombre: /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/
      },
    //  idMadre: {
    //    required: true,
    //    minlength: 13,
    //    maxlength: 13,
    //  regexnombre: /^[0-9\-]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/
  //  },
    areaHospitalizacion:{
      required: true,
      minlength: 5,
      maxlength: 255,
      regexnombre: /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/
    },
     //
    //idFrasco:{
    //  required: true,
    //  minlength: 13,
    //  maxlength: 13,
    //  regexnombre: /^[0-9\-]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/
    //},
    fechaExtraccion: {
      required: true,
    },
    horaEx:{
      required: true,
    },
    cantidad:{
      required: true,
      minlength: 1,
      maxlength:10,
    //regexnum: /^[0-9]*+(\.[0-9][0-9]*)+$/
  },

    messages: {
      nombreRecepcion: {
        required: "Debe indicar su nombre",
        minlength: "El nombre debe contener al menos 3 letras",
        maxlength: "El nombre puede contener máximo 255 letras",
        regexnombre: "Solo se permiten letras"
      },
      apellidoPaternoRecepcion: {
        required: "Debe indicar su apellido paterno",
        minlength: "El apellido debe contener al menos 3 letras",
        maxlength: "El apellido puede contener máximo 255 letras",
        regexnombre: "Solo se permiten letras"
      },
      apellidoMaternoRecepcion: {
        required: "Debe indicar su apellido materno",
        minlength: "El apellido debe contener al menos 3 letras",
        maxlength: "El apellido puede contener máximo 255 letras",
        regexnombre: "Solo se permiten letras"
      },
      areaHospitalizacion: {
        required: "Debe indicar el Área de Hospitalización",
        minlength: "El Área de Hospitalización debe contener al menos 5 letras",
        maxlength: "El Área de Hospitalización debe contener máximo 255 letras",
        regexnombre: "Solo se permiten letras"
      },
      fechaExtraccion: {
        required: "Debe seleccionar una fecha"
      },
      horaEX: {
        required: "Debe seleccionar una hora"
      },
      cantidad: {
        required: "Debe indicar la cantidad en ml",
        minlength: "Debe contener al menos 1 digito",
        maxlength: "Debe contener máximo 10 dígitos ",
        regexnum: "Solo se permiten números y ."
      }
    }
  }
  });
  $.validator.addMethod(
    "regexnombre",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    },
    "Solo se permiten letras"
  );
  //para la cantidad ml
  $.validator.addMethod(
    "regexnum",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    },
    "Solo se permiten números y ."
  );
});


//------------------------------------
// funcion ejecutada cuando el formulario enviado es valido
/*function enviarForm() {
  // animacion para el boton de guardado
  $('.btn-guardar').children().removeClass("fa-save").addClass("fa-sync fa-spin");
  // se deshabilita el boton mientras se procesa la peticion
  $('.btn-guardar').prop("disabled", true);

  // obtenemos los datos del formulario
  var form = this.obtenerValoresForm();
  $.ajax({
		type: 'POST',
		url: 'http://127.0.0.1:8000/api/recepciones',
		async: false,
		data:(
    //  'NOMBRE=' + form.nombre +
    //  '&APELLIDO_PATERNO=' + form.apPat +
    //  '&APELLIDO_MATERNO=' + form.apMat +
      '&AREA_HOSPITALIZACION' + form.areaHospital +
      'FECHA_EXTRACCION' + form.fechaEx +
      'HORA_EXTRACCION' + form.horaEX +
      'CANTIDAD_RECIBIDA' + form.cantidad
    ),
		success: function (respuesta) {
      // devolvemos las clases y estado del boton a la normalidad
      $('.btn-guardar').children().removeClass("fa-sync fa-spin").addClass("fa-save");
      $('.btn-guardar').prop("disabled", false);

			Swal.fire({
        icon: 'success',
        title: 'Registro Guardado!'
      });
		}
	});
}*/

/*function obtenerValoresForm() {
  valoresForm = {
    areaHospital: $("#areaHospital").val(),
    fechaEx: Number($("#fechaExtraccion").val()),
    horaEX: Number($("#sifilis").val()),
    cantidad: Number($("#hepatitisC").val())
  }

  return valoresForm;
}*/
