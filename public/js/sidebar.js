// ciclo que va agregando control de eventos a cada dropdown que exista en la barra
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}


// ciclo que va agregando control de eventos a cada dropdown que exista en la barra
var dropdown_dosificacion = document.getElementsByClassName("dropdown-btn-sucedaneo");
var j;

for (j = 0; j < dropdown_dosificacion.length; j++) {
  dropdown_dosificacion[j].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}
