<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::view('/login', 'login');
Route::view('/inicio', 'inicio');
Route::view('/dosificacion_homologa', 'dosificacion_homologa');
Route::view('/recepcion_leche', 'recepcion_leche');
Route::view('/registro_donadora', 'registro_donadora');
Route::view('/recepcion', 'recepcion');
Route::view('/analisis_leche', 'analisis_leche');
Route::view('/pasteurizacion', 'pasteurizacion');
Route::view('/reporte_totales', 'reporte_totales');
Route::view('/reporte_datos_leche', 'reporte_datos_leche');
Route::view('/preparacion_sucedaneo', 'preparacion_sucedaneo');
Route::view('/dosificacion_sucedaneo', 'dosificacion_sucedaneo');
Route::view('/productividad_estatal', 'productividad_estatal');

Auth::routes();
Route::post('login', 'Login@login');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/imprimir', 'GeneradorController@imprimir');
