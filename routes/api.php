<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::ApiResource('madres','MadreController');
Route::ApiResource('recepciones','RecepcionLecheController');
Route::ApiResource('bebes','BebeController');
Route::ApiResource('dosificaciones','DosificacionController');
Route::ApiResource('analisis','AnalisisController');
Route::ApiResource('pasteurizaciones','PasteurizacionController');
Route::ApiResource('preparaciones','PreparacionSucedaneoController');
Route::ApiResource('dosificacionesdos','DosificacionSucedaneoController');
Route::ApiResource('reportes','ReportesController');
