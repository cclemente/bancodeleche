<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TipoDonacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('TIPO_DONACION')) {
        Schema::create('TIPO_DONACION', function (Blueprint $table) {
            $table->increments('ID_TIPO_DONACION');
            $table->string('DESCRIPCION');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TIPO_DONACION');
    }
}
