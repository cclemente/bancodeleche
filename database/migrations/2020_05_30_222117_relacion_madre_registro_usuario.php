<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RelacionMadreRegistroUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('RELACION_MADRE_REGISTRO_USUARIO')) {
        Schema::create('RELACION_MADRE_REGISTRO_USUARIO', function (Blueprint $table) {
            $table->increments('ID_RELACION_MADRE_REGISTRO_USUARIO');
            $table->integer('ID_MADRE')->unsigned();
            $table->foreign('ID_MADRE')->references('ID_MADRE')->on('MADRE');
            $table->integer('id')->unsigned();
            $table->foreign('id')->references('id')->on('users');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RELACION_MADRE_REGISTRO_USUARIO');
    }
}
