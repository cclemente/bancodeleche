<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaTipoFormula extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('TIPO_FORMULA')) {
        Schema::create('TIPO_FORMULA', function (Blueprint $table) {
            $table->increments('ID_TIPO_FORMULA');
            $table->string('DESCRIPCION');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
      Schema::dropIfExists('TIPO_FORMULA');
    }
}
