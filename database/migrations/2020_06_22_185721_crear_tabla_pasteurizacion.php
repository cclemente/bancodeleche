<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPasteurizacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('PASTEURIZACION')) {
        Schema::create('PASTEURIZACION', function (Blueprint $table) {
          //  $table->increments('ID_PASTEURIZACION');
            $table->string('ID_ETIQUETA');
            $table->string('ETIQUETA_PASTEURIZACION');
            $table->boolean('ACIDEZ_DORNIC');
            $table->boolean('PASTEURIZADA');
            $table->decimal('CANTIDAD_PASTEURIZADA');
            $table->decimal('CANTIDAD_DESECHADA');
          });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('PASTEURIZACION');
    }
}
