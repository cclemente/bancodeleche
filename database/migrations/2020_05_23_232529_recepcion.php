<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Recepcion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('RECEPCION')) {
          Schema::create('RECEPCION', function (Blueprint $table) {
              $table->increments('ID_RECEPCION');
              $table->string('ID_FRASCO');
              $table->string('AREA_HOSPITALIZACION');
              $table->date('FECHA_EXTRACCION');
              $table->time('HORA_EXTRACCION');
              $table->integer('ID_TIPO_DONACION')->unsigned();
              $table->foreign('ID_TIPO_DONACION')->references('ID_TIPO_DONACION')->on('TIPO_DONACION');
              $table->decimal('CANTIDAD_RECIBIDA');
          });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RECEPCION');
    }
}
