<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RelacionMadreRecepcion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable('RELACION_MADRE_RECEPCION')) {
        Schema::create('RELACION_MADRE_RECEPCION', function (Blueprint $table) {
            $table->increments('ID_RELACION_MADRE_RECEPCION');
            $table->integer('ID_MADRE')->unsigned();
            $table->foreign('ID_MADRE')->references('ID_MADRE')->on('MADRE');
            $table->integer('ID_RECEPCION')->unsigned();
            $table->foreign('ID_RECEPCION')->references('ID_RECEPCION')->on('RECEPCION');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RELACION_MADRE_RECEPCION');
    }
}
