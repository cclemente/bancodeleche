<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaRelacionBebeDosificacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('RELACION_BEBE_DOSIFICACION')) {
        Schema::create('RELACION_BEBE_DOSIFICACION', function (Blueprint $table) {
            $table->increments('ID_RELACION_BEBE_DOSIFICACION');
            $table->integer('ID_DATOS_BEBE')->unsigned();
            $table->foreign('ID_DATOS_BEBE')->references('ID_DATOS_BEBE')->on('DATOS_BEBE');
            $table->integer('ID_DOSIFICACION')->unsigned();
            $table->foreign('ID_DOSIFICACION')->references('ID_DOSIFICACION')->on('DOSIFICACION');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
      Schema::dropIfExists('RELACION_BEBE_DOSIFICACION');
    }
}
