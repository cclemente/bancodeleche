<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Analisis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable('ANALISIS')) {
        Schema::create('ANALISIS', function (Blueprint $table) {
            $table->increments('ID_ANALISIS');
            $table->integer('ID_CLASIFICACION')->unsigned();
            $table->foreign('ID_CLASIFICACION')->references('ID_CLASIFICACION')->on('CLASIFICACION_LECHE');
            $table->boolean('EMBALAJE');
            $table->boolean('COLOR');
            $table->boolean('SUCIEDAD');
            $table->boolean('OLOR');
            $table->decimal('CREMATROCITO')->nullable();
            $table->decimal('KILO_CALORIAS')->nullable();
            $table->decimal('PROTEINAS')->nullable();
            $table->decimal('CARBOHIDRATOS')->nullable();
            $table->decimal('GRASAS')->nullable();
            $table->decimal('CANTIDAD_ANALIZADA');
            $table->decimal('CANTIDAD_DESECHADA');
            $table->decimal('CANTIDAD_ALMACENADA');
            $table->integer('ID_RECEPCION')->unsigned();
            $table->foreign('ID_RECEPCION')->references('ID_RECEPCION')->on('RECEPCION');
            $table->string('ID_ETIQUETA')->nullable();
          });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('ANALISIS');
    }
}
