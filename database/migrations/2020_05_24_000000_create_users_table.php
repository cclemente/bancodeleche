<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('users')) {
        Schema::create('users', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->string('email');
          $table->timestamp('email_verified_at')->nullable();
          $table->string('password')->unique();
          $table->integer('ID_ROL_SISTEMA')->unsigned();
          $table->foreign('ID_ROL_SISTEMA')->references('ID_ROL_SISTEMA')->on('ROL_SISTEMA');
          $table->integer('ID_HOSPITAL')->unsigned();
          $table->foreign('ID_HOSPITAL')->references('ID_HOSPITAL')->on('HOSPITALES');
          $table->rememberToken();
          $table->timestamps();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
