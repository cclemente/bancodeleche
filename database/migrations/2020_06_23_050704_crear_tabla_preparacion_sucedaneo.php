<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPreparacionSucedaneo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('PREPARACION_SUCEDANEO')) {
        Schema::create('PREPARACION_SUCEDANEO', function (Blueprint $table) {
          $table->increments('ID_PREPARACION_SUCEDANEO');
          $table->date('FECHA_REGISTRO');
          $table->integer('ID_DIAGNOSTICO')->unsigned();
          $table->foreign('ID_DIAGNOSTICO')->references('ID_DIAGNOSTICO')->on('DIAGNOSTICO');
          $table->integer('ID_TIPO_FORMULA')->unsigned();
          $table->foreign('ID_TIPO_FORMULA')->references('ID_TIPO_FORMULA')->on('TIPO_FORMULA');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('PREPARACION_SUCEDANEO');
    }
}
