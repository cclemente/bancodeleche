<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaDiagnostico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('DIAGNOSTICO')) {
        Schema::create('DIAGNOSTICO', function (Blueprint $table) {
            $table->increments('ID_DIAGNOSTICO');
            $table->string('DESCRIPCION');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('DIAGNOSTICO');        
    }
}
