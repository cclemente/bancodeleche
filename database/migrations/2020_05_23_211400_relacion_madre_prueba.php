<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RelacionMadrePrueba extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable('RELACION_MADRE_PRUEBA')) {
          Schema::create('RELACION_MADRE_PRUEBA', function (Blueprint $table) {
              $table->increments('ID_RELACION_MADRE_PRUEBA');
              $table->integer('ID_MADRE')->unsigned();
              $table->foreign('ID_MADRE')->references('ID_MADRE')->on('MADRE');
              $table->integer('ID_PRUEBA')->unsigned();
              $table->foreign('ID_PRUEBA')->references('ID_PRUEBA')->on('PRUEBA');
          });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RELACION_MADRE_PRUEBA');
    }
}
