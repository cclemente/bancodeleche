<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaDosificacionSucedaneo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('DOSIFICACION_SUCEDANEO')) {
        Schema::create('DOSIFICACION_SUCEDANEO', function (Blueprint $table) {
            $table->increments('ID_DOSIFICACION_SUCEDANEO');
            $table->string('ID_SISTEMA');
            $table->date('FECHA_DOSIFICACION');
            $table->time('HORA_DOSIFICACION');
            $table->string('SERVICIO');
            $table->integer('ID_TIPO_FORMULA')->unsigned();
            $table->foreign('ID_TIPO_FORMULA')->references('ID_TIPO_FORMULA')->on('TIPO_FORMULA');
            $table->decimal('CANTIDAD_DOSIFICACION_ML');
            $table->decimal('CANTIDAD_DESECHADA_ML');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('DOSIFICACION_SUCEDANEO');      
    }
}
