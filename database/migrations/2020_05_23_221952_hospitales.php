<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Hospitales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable('HOSPITALES')) {
        Schema::create('HOSPITALES', function (Blueprint $table) {
            $table->increments('ID_HOSPITAL');
            $table->string('NOMBRE_HOSPITAL');
            $table->string('UBICACION');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('HOSPITALES');
    }
}
