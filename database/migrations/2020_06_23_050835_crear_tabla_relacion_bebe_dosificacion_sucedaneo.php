<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaRelacionBebeDosificacionSucedaneo extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('RELACION_BEBE_DOSIFICACION_S')) {
        Schema::create('RELACION_BEBE_DOSIFICACION_S', function (Blueprint $table) {
            $table->increments('ID_RELACION_BEBE_DOSIFICACION_S');
            $table->integer('ID_DATOS_BEBE')->unsigned();
            $table->foreign('ID_DATOS_BEBE')->references('ID_DATOS_BEBE')->on('DATOS_BEBE');
            $table->integer('ID_DOSIFICACION_SUCEDANEO')->unsigned();
            $table->foreign('ID_DOSIFICACION_SUCEDANEO')->references('ID_DOSIFICACION_SUCEDANEO')->on('DOSIFICACION_SUCEDANEO');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
       Schema::dropIfExists('RELACION_BEBE_DOSIFICACION_S');
    }
}
