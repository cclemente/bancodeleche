<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RolSistema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('ROL_SISTEMA')) {
        Schema::create('ROL_SISTEMA', function (Blueprint $table) {
            $table->increments('ID_ROL_SISTEMA');
            $table->string('DESCRIPCION');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('ROL_SISTEMA');
    }
}
