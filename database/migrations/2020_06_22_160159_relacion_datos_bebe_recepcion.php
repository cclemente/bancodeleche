<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RelacionDatosBebeRecepcion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('RELACION_DATOS_BEBE_RECEPCION')) {
        Schema::create('RELACION_DATOS_BEBE_RECEPCION', function (Blueprint $table) {
            $table->increments('ID_RELACION_DATOS_BEBE_RECEPCION');
            $table->integer('ID_DATOS_BEBE')->unsigned();
            $table->foreign('ID_DATOS_BEBE')->references('ID_DATOS_BEBE')->on('DATOS_BEBE');
            $table->integer('ID_RECEPCION')->unsigned();
            $table->foreign('ID_RECEPCION')->references('ID_RECEPCION')->on('RECEPCION');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
      Schema::dropIfExists('RELACION_DATOS_BEBE_RECEPCION');
    }
}
