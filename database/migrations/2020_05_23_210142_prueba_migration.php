<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PruebaMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable('PRUEBA')) {
          Schema::create('PRUEBA', function (Blueprint $table) {
              $table->increments('ID_PRUEBA');
              $table->boolean('SIDA');
              $table->boolean('SIFILIS');
              $table->boolean('HEPATITIS');
              $table->boolean('DROGAS');


          });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PRUEBA');
    }
}
