<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaRelacionBebePreparacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('RELACION_BEBE_PREPARACION')) {
        Schema::create('RELACION_BEBE_PREPARACION', function (Blueprint $table) {
            $table->increments('ID_RELACION_BEBE_PREPARACION');
            $table->integer('ID_DATOS_BEBE')->unsigned();
            $table->foreign('ID_DATOS_BEBE')->references('ID_DATOS_BEBE')->on('DATOS_BEBE');
            $table->integer('ID_PREPARACION_SUCEDANEO')->unsigned();
            $table->foreign('ID_PREPARACION_SUCEDANEO')->references('ID_PREPARACION_SUCEDANEO')->on('PREPARACION_SUCEDANEO');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('RELACION_BEBE_PREPARACION');      
    }
}
