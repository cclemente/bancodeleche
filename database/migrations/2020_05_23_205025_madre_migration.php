<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MadreMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable('MADRE')) {
          Schema::create('MADRE', function (Blueprint $table) {
              $table->increments('ID_MADRE');
              $table->string('ID_SISTEMA')->unique();;
              $table->string('NOMBRE');
              $table->string('APELLIDO_PATERNO');
              $table->string('APELLIDO_MATERNO');
              $table->date('FECHA_REGISTRO');

          });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MADRE');
    }
}
