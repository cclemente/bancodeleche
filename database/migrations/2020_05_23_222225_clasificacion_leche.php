<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClasificacionLeche extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable('CLASIFICACION_LECHE')) {
        Schema::create('CLASIFICACION_LECHE', function (Blueprint $table) {
            $table->increments('ID_CLASIFICACION');
            $table->string('DESCRIPCION');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CLASIFICACION_LECHE');
    }
}
