<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Dosificacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('DOSIFICACION')) {
        Schema::create('DOSIFICACION', function (Blueprint $table) {
            $table->increments('ID_DOSIFICACION');
            $table->string('ID_SISTEMA');
            $table->date('FECHA_DOSIFICACION');
            $table->time('HORA_DOSIFICACION');
            $table->string('SERVICIO');
            $table->decimal('CANTIDAD_DOSIFICACION_ML');
            $table->boolean('NUMERO_TOMA');
            $table->decimal('ADMINISTRACION_ML');
            $table->integer('ID_TIPO_DONACION')->unsigned();
            $table->foreign('ID_TIPO_DONACION')->references('ID_TIPO_DONACION')->on('TIPO_DONACION');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('DOSIFICACION');
    }
}
