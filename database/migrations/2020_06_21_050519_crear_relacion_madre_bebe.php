<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearRelacionMadreBebe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('RELACION_MADRE_DATOS_BEBE')) {
        Schema::create('RELACION_MADRE_DATOS_BEBE', function (Blueprint $table) {
            $table->increments('ID_RELACION_MADRE_DATOS_BEBE');
            $table->integer('ID_MADRE')->unsigned();
            $table->foreign('ID_MADRE')->references('ID_MADRE')->on('MADRE');
            $table->integer('ID_DATOS_BEBE')->unsigned();
            $table->foreign('ID_DATOS_BEBE')->references('ID_DATOS_BEBE')->on('DATOS_BEBE');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
      Schema::dropIfExists('RELACION_MADRE_DATOS_BEBE');
    }
}
