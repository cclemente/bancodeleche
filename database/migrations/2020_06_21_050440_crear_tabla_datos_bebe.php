<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaDatosBebe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('DATOS_BEBE')) {
          Schema::create('DATOS_BEBE', function (Blueprint $table) {
              $table->increments('ID_DATOS_BEBE');
              $table->date('FECHA_NACIMIENTO');
              $table->time('HORA_NACIMIENTO');
          });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('DATOS_BEBE');
    }
}
