<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(){
        // $this->call(UserSeeder::class);
        User::create(array(
          'name'=>'',
          'email'  => '1',
          'password' => Hash::make('Materno834_En'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '1',
          'ID_HOSPITAL'=> '1'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '2',
          'password' => Hash::make('Materno3483_Lab'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '2',
          'ID_HOSPITAL'=> '1'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '3',
          'password' => Hash::make('Pretelini594-En'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '1',
          'ID_HOSPITAL'=> '2'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '4',
          'password' => Hash::make('Pretelini843-Lab'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '2',
          'ID_HOSPITAL'=> '2'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '5',
          'password' => Hash::make('Dr348fEn'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '1',
          'ID_HOSPITAL'=> '3'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '6',
          'password' => Hash::make('Dr282Lab'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '2',
          'ID_HOSPITAL'=> '3'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '7',
          'password' => Hash::make('Atlacomulco2494>En'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '1',
          'ID_HOSPITAL'=> '4'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '8',
          'password' => Hash::make('Atlacomulco564>Lab'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '2',
          'ID_HOSPITAL'=> '4'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '9',
          'password' => Hash::make('Hidalgo0454-En'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '1',
          'ID_HOSPITAL'=> '5'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '10',
          'password' => Hash::make('Hidalgo764-Lab'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '2',
          'ID_HOSPITAL'=> '5'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '11',
          'password' => Hash::make('Maximiliano9484-En'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '1',
          'ID_HOSPITAL'=> '6'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '12',
          'password' => Hash::make('Maximiliano9644-Lab'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '2',
          'ID_HOSPITAL'=> '6'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '13',
          'password' => Hash::make('Axapusco8273-En'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '1',
          'ID_HOSPITAL'=> '7'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '14',
          'password' => Hash::make('Axapusco03884-Lab'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '2',
          'ID_HOSPITAL'=> '7'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '15',
          'password' => Hash::make('Perla4834-En'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '1',
          'ID_HOSPITAL'=> '8'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '16',
          'password' => Hash::make('Perla8573-Lab'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '2',
          'ID_HOSPITAL'=> '8'
        ));
        User::create(array(
          'name'=>'',
          'email'  => '17',
          'password' => Hash::make('Superadmin'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'ID_ROL_SISTEMA'=> '3',
          'ID_HOSPITAL'=> '8'
        ));
    }
}
