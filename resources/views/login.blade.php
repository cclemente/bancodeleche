<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('css/login.css')}}" rel="stylesheet" type="text/css"/>
  <title></title>
</head>

<body>
  <!-- Componente Cabecera -->
  @include('cabecera')
  <div class="container">
    <!-- Componente Cabecera -->
  </div>
  <!-- Cuerpo Login -->
  <div class="container text-center mt-2 mb-2">
    <form class="form-signin">
      <img class="mb-4" src="{{asset('img/user.png')}}" alt="avatar" width="72" height="72">
      <!-- Titulo del formulario -->
      <h1 class="h3 mb-3 font-weight-normal">
        Inicio de Sesi&oacute;n
      </h1>

      <!-- Seleccion del Rol -->
      <div class="form-group text-left">
        <label>Seleccione Rol</label>
        <select class="form-control">
          <option value="" disabled selected>Seleccione</option>
          <option value="1">Enfermer&iacute;a</option>
          <option value="2">Laboratorio</option>
          <option value="3">Administraci&oacute;n</option>
        </select>
      </div>

      <!-- Campo para Contrasenia -->
      <div class="form-group text-left">
        <label>Ingrese su contrase&ntilde;a</label>
        <input type="password" class="form-control" placeholder="Contrase&ntilde;a">
      </div>

      <!-- Boton para entrar -->
      <button class="btn btn-lg btn-block boton-entrar-login text-white btn-primary" type="button">
        Entrar
        {{ __('Login') }}

      </button>
    </form>
  </div>
  <!-- FIN Cuerpo Login -->

  <!-- Componente Footer -->
  @include('pie')
  <div class="container">
    <!-- Componente Footer -->
  </div>

  <!-- Iconos -->
  <script src="{{asset('js/libs/fontawesome.js')}}"></script>

  <!-- JQuery 3.5.1 -->
  <script src="{{asset('js/libs/jquery-3.5.1.min.js')}}"></script>
  <!-- JQuery Validate Plugin -->
  <script src="{{asset('js/libs/jquery.validate.min.js')}}"></script>
</body>

</html>
