@extends('layout')
@section('cuerpo')

<!-- Contenedor Principal -->
<div class="p-3 pl-2 pr-2">
  <!-- Formulario Buscador -->
  <form id="form-buscador-analisis-heterologa">
    <!-- Tarjeta Para Buscar Frasco --> 
    <div class="card mb-3">
      <!-- Titulo de la tarjeta -->
      <div class="card-header">Buscar Frasco Leche Por Id Recepci&oacute;n</div>

      <!-- Cuerpo de la tarjeta -->
      <div class="card-body">
        <!-- Primer Renglon -->
        <div class="row">
          <!-- Columna Tipo Donacion -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Tipo de Leche</label>
            <select class="form-control" id="selectTipoLecheAnalisis" name="selectTipoLecheAnalisis">
              <option value="" disabled selected>Seleccione</option>
              <option value="1">Hom&oacute;loga</option>
              <option value="2">Heter&oacute;loga</option>
            </select>
          </div>
          <!-- FIN Columna Tipo Donacion -->

          <!-- Columna Buscador -->
          <div class="col-sm-4 form-group">
            <label>Identificador Frasco</label>
            <div class="input-group">
              <input
                id="inputIdRecepcionLeche"
                type="text"
                class="form-control"
                placeholder="ID Frasco"
                autocomplete="off"
                name="inputIdRecepcionLeche"
              />
              <div class="input-group-append">
                <button class="btn boton-entrar-login text-white btn-buscar" type="submit">
                  <i class="fa fa-search"></i>
                  Buscar
                </button>
              </div>
            </div>
            <!-- Solo para este caso el error tiene una ubicacion personalizada -->
            <div id="error-input-id-recepcion-leche"></div>
          </div>
          <!-- FIN Columna Buscador -->

          <!-- Columna Identidicador Frasco -->
          <div class="col-sm-4 form-group">
            <label>Identificador Encontrado</label>
            <input disabled class="form-control" id="frasco-encontrado-analisis">
          </div>
          <!-- FIN Columna Identidicador Frasco -->
        </div>
        <!-- FIN Primer Renglon -->
      </div>
      <!-- FIN Cuerpo de la tarjeta -->
    </div>
    <!-- FIN Tarjeta Para Buscar Frasco -->
  </form>
  <!-- FIN Formulario Buscador -->

  <!-- Formulario Analisis Heterologa -->
  <form id="form-analisis-heterologa">
    <!-- Tarjeta para los datos de la madre donadora --> 
    <div class="card mb-3">
      <!-- Titulo de la tarjeta -->
      <div class="card-header">Resultados del An&aacute;lisis</div>

      <!-- Cuerpo de la tarjeta -->
      <div class="card-body">
        <!-- Primer Renglon -->
        <div class="row">
          <!-- Seleccion clasificacion -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Clasificaci&oacute;n</label>
            <select disabled class="form-control" id="selectClasificacion" name="selectClasificacion">
              <option value="" disabled selected>Seleccione</option>
              <option value="1">Prematuro</option>
              <option value="2">Calostro</option>
              <option value="3">Transici&oacute;n</option>
              <option value="4">Madura</option>
            </select>
          </div>

          <!-- Seleccion embalaje -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Embalaje</label>
            <select disabled class="form-control" id="selectEmbalaje" name="selectEmbalaje">
              <option value="" disabled selected>Seleccione</option>
              <option value="1">Correcto</option>
              <option value="2">Incorrecto</option>
            </select>
          </div>

          <!-- Seleccion color -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Color</label>
            <select disabled class="form-control" id="selectColor" name="selectColor">
              <option value="" disabled selected>Seleccione</option>
              <option value="1">Correcto</option>
              <option value="2">Incorrecto</option>
            </select>
          </div>
        </div>
        <!-- FIN Primer Renglon -->

        <!-- Segundo Renglon -->
        <div class="row">
          <!-- Seleccion de suciedad -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Suciedad</label>
            <select disabled class="form-control" id="selectSuciedad" name="selectSuciedad">
              <option value="" disabled selected>Seleccione</option>
              <option value="1">Correcto</option>
              <option value="2">Incorrecto</option>
            </select>
          </div>

          <!-- Seleccion de olor -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Olor</label>
            <select disabled class="form-control" id="selectOlor" name="selectOlor">
              <option value="" disabled selected>Seleccione</option>
              <option value="1">Correcto</option>
              <option value="2">Incorrecto</option>
            </select>
          </div>

          <!-- Campo crematrocito -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Crematrocito (%)</label>
            <input
              disabled
              id="inputCrematrocito"
              name="inputCrematrocito"
              type="text"
              class="form-control"
              placeholder="Porcentaje Crematrocito"
              autocomplete="off"
            />
          </div>
        </div>
        <!-- FIN Segundo Renglon -->

        <!-- Tercer Renglon -->
        <div class="row">
          <!-- Campo kilocalorias -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Kilocalor&iacute;as (Kcal)</label>
            <input
              disabled
              id="inputKilocalorias"
              name="inputKilocalorias"
              type="text"
              class="form-control"
              placeholder="Kilocalor&iacute;s"
              autocomplete="off"
            />
          </div>

          <!-- Campo Proteinas -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Prote&iacute;nas (g/100ml)</label>
            <input
              disabled
              id="inputProteinas"
              name="inputProteinas"
              type="text"
              class="form-control"
              placeholder="Prote&iacute;nas"
              autocomplete="off"
            />
          </div>

          <!-- Campo Carbohidratos -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Carbohidratos (g/100ml)</label>
            <input
              disabled
              id="inputCarbohidratos"
              name="inputCarbohidratos"
              type="text"
              class="form-control"
              placeholder="Carbohidratos"
              autocomplete="off"
            />
          </div>
        </div>
        <!-- FIN Tercer Renglon -->

        <!-- Cuarto Renglon -->
        <div class="row">
          <!-- Campo Grasas -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Grasas (g/100ml)</label>
            <input
              disabled
              id="inputGrasas"
              name="inputGrasas"
              type="text"
              class="form-control"
              placeholder="Grasas"
              autocomplete="off"
            />
          </div>

          <!-- Campo Cantidad -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Cantidad Analizada (ml)</label>
            <input
              disabled
              id="inputCantidad"
              name="inputCantidad"
              type="text"
              class="form-control"
              placeholder="Cantidad"
              autocomplete="off"
            />
          </div>

          <!-- Campo Cantidad A Almacenar -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Cantidad a Almacenar (ml)</label>
            <input
              disabled
              id="inputAlmacenar"
              name="inputAlmacenar"
              type="text"
              class="form-control"
              placeholder="Cantidad"
              autocomplete="off"
            />
          </div>
        </div>
        <!-- FIN Cuarto Renglon -->
      </div>
      <!-- FIN Cuerpo de la tarjeta -->
    </div>
    <!-- FIN Tarjeta -->

    <!-- Opcion para guardar los datos -->
    <div style="display: flex; justify-content: center;" class="mt-3">
      <button disabled class="btn boton-entrar-login text-white btn-guardar" type="submit">
        <i class="fa fa-save"></i>
        Guardar Datos
      </button>
    </div>
    <!-- FIN Opcion para guardar los datos -->
  </form>
  <!-- FIN Formulario Analisis Heterologa -->
</div>
<!-- FIN Contenedor Principal -->

@endsection