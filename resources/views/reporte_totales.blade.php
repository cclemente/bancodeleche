@extends('layout')
@section('cuerpo')

<section id="reporte-totales">
  <!-- Preloader mostrado hasta que se cargue vue -->
  <div class="dflex" :hidden="mostrarPreloader ? false : true">
    <div class="preloader mt-5 mb-5"></div>
  </div>

  <!-- Contenedor Principal -->
  <div v-cloak class="pl-2 pr-2">
    <!-- Contenedor responsivo para una tabla -->
    <div class="table-responsive mt-3 mb-3">
      <!-- Tabla con los calendarios -->
      <table class="table text-center table-bordered table-sm">
        <!-- Encabezado Tabla -->
        <thead>
          <tr>
            <th style="width: 33%; min-width: 150px;">Fecha Inicio</th>
            <th style="width: 33%; min-width: 150px;">Fecha Fin</th>
            <th style="width: 33%; min-width: 150px;">Acciones</th>
          </tr>
        </thead>
        <!-- FIN Encabezado Tabla -->

        <!-- Cuerpo de la tabla -->
        <tbody>
          <tr>
            <!-- Columna Fecha Inicial -->
            <td>
              <input type="date" class="form-control" v-model="fechaInicio">
            </td>
            <!-- FIN Columna Fecha Inicial -->

            <!-- Columna Fecha Final -->
            <td>
            <input type="date" class="form-control" v-model="fechaFin">
            </td>
            <!-- FIN Columna Fecha Final -->

            <!-- Columna Acciones -->
            <td>
              <button
                class="btn btn-primary"
                v-on:click="consultarDatos()"
                :disabled="cargando ? true: false"
              >
                <i class="fa" v-bind:class="[cargando ? 'fa-sync fa-spin': 'fa-search']"></i>
                Consultar
              </button>
            </td>
            <!-- FIN Columna Acciones -->
          </tr>
        </tbody>
        <!-- FIN Cuerpo de la tabla -->
      </table>
      <!-- FIN Tabla con los calendarios -->
    </div>
    <!-- FIN Contenedor responsivo para una tabla -->

    <!-- Tarjeta con los resultados del reporte -->
    <div class="card mb-3" :hidden="!reporteGenerado || cargando">
      <!-- Titulo de la tarjeta -->
      <div class="card-header">Totales Encontrados</div>

      <!-- Cuerpo de la tarjeta -->
      <div class="card-body">
        <!-- Contenedor responsivo para tabla -->
        <div class="table-responsive">
          <!-- Tabla con los totales obtenidos -->
          <table class="table text-center table-bordered">
            <!-- Encabezado Tabla -->
            <thead>
              <tr>
                <th>Total Beb&eacute;s</th>
                <th>Total Madres</th>
                <th>Total Donaciones Hom&oacute;logas</th>
                <th>Total Donaciones Heter&oacute;logas</th>
              </tr>
            </thead>
            <!-- FIN Encabezado Tabla -->

            <!-- Cuerpo Tabla -->
            <tbody>
              <tr>
                <td v-for="total of totales">@{{ total }}</td>
              </tr>
            </tbody>
            <!-- Cuerpo Tabla -->
          </table>
          <!-- FIN Tabla con los totales obtenidos -->
        </div>
        <!-- FIN Contenedor responsivo para tabla -->

        <div style="display: flex; justify-content: center; align-items: center;">
          <div class="w-75 h-75">
            <canvas id="graficaTotales" width="400" height="400"></canvas>
          </div>
        </div>
      </div>
      <!-- FIN Cuerpo de la tarjeta -->
    </div>
    <!-- FIN Tarjeta con los resultados del reporte -->
  </div>
  <!-- FIN Contenedor Principal -->
</section>


<script>
var reporteTotales = new Vue({
  el: '#reporte-totales',
  data: {
    reporteGenerado: false,
    cargando: false,
    fechaInicio: null,
    fechaFin: null,
    mostrarPreloader: true,
    totales: [] // datos de prueba, se tienen que sustituir por la respuesta http
  },
  mounted: function() {
    this.mostrarPreloader = false;
  },
  methods: {
    consultarDatos: function() {
      console.log(this.fechaFin);
      // solo se envian los datos si se han indicado las dos fechas requeridas
      if (this.fechaInicio && this.fechaFin) {
        this.cargando = true;

        axios.get('http://127.0.0.1:8000/api/reportes', {
          params: {
            tipo_total: 1,
            fecha_inicio: this.fechaInicio,
            fecha_fin: this.fechaFin
          }
        }).then( response => {
          this.totales.push(response.data.data_bebe);
          this.totales.push(response.data.data_madre);
          this.totales.push(response.data.data_homologa);
          this.totales.push(response.data.data_heterologa);
          graficarReporteTotales(this.totales);

          this.cargando = false;
          this.reporteGenerado = true;
        }).catch(error => {
          console.log(error);
          this.cargando = false;
          Swal.fire({
            icon: 'error',
            title: 'Algo salió muy mal!'
          });
        });
      } else {
        Swal.fire({
          icon: 'warning',
          title: 'Atención',
          text: 'Seleccione ambas fechas antes de consultar el reporte'
        })
      }
    }
  }
});

function graficarReporteTotales(totales) {
  var graficaTotales = document.getElementById('graficaTotales').getContext('2d');
  var myChart = new Chart(graficaTotales, {
    type: 'bar',
    data: {
      labels: ['Total Bebés', 'Total Madres', 'Total Homólogas', 'Total Heterólogas'],
      datasets: [{
        label: 'Totales',
        data: totales,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)'
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
              beginAtZero: true
            }
        }]
      }
    }
  });
}
</script>

@endsection
