
@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('css/login.css')}}" rel="stylesheet" type="text/css"/>

<div class="container">
  <!-- Componente Cabecera -->
  @include('cabecera')
</div>
<div class="container text-center mt-2 mb-2">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form class="form-signin" method="POST" action="{{ route('login') }}">
                        @csrf

                        <img class="mb-4" src="{{asset('img/user.png')}}" alt="avatar" width="72" height="72">
                        <h1 class="h3 mb-3 font-weight-normal">
                          Inicio de Sesi&oacute;n
                        </h1>
                                <div class="form-group text-left">
                                  <label>Seleccione Rol</label>
                                  <select id="email" name="email" class="form-control">
                                    <option value="" disabled selected>Seleccione</option>
                                    <option value="1">Enfermer&iacute;a</option>
                                    <option value="2">Laboratorio</option>
                                    <option value="3">Administraci&oacute;n</option>
                                  </select>
                                </div>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        <div class="form-group text-left">
                          <label>Ingrese su contrase&ntilde;a</label>
                          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                          @error('password')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror

                        </div>
                        <button class="btn btn-lg btn-block boton-entrar-login text-white btn-primary" type="submit">
                          Entrar
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
  <!-- Componente Footer -->
  @include('pie')
</div>

<!-- Iconos -->
<script src="{{asset('js/libs/fontawesome.js')}}"></script>
<!-- JQuery 3.5.1 -->
<script src="{{asset('js/libs/jquery-3.5.1.min.js')}}"></script>
<!-- JQuery Validate Plugin -->
<script src="{{asset('js/libs/jquery.validate.min.js')}}"></script>

@endsection
