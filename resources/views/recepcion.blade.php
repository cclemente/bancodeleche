@extends('layout')
@section('cuerpo')


<div class="container text-center mt-3 mb-2 bg-light">
    <!-- Titulo del formulario -->
    <h1 class="text-secondary"></h1>
    <h1 class="text-secondary">Recepción de Leche Humana</h1>
  <!--  <h1 class="h2 mb-3 font-weight-">
      Recepción de Leche Humana
    </h1>  -->


    <div class="row">
        <!-- Columna Lateral Izquierda -->
        <div class="col-lg-4 mb-2">
        </div>


        <!-- Columna Central -->
        <div class="col-lg-4 mb-4">

            <!-- Campo ID de la Madre -->
            <form class="form-group text-left">
                <div class="form-group">
                    <h1 class="text"></h1>
                    <label>Número de identificación de la madre</label>
                    <input type="number" class="form-control forml-control-user" placeholder="ID de la Madre">
                </div>
            </form>


             <!-- Campo Id del frasco -->
            <div class="form-group text-left ">
                <div class="form-group">
                    <label>Número de identificación del frasco</label>
                    <input type="number" class="form-control form-control-user" placeholder="ID del Frasco">
                </div>
            </div>


            <!-- Campo nombre de la madre -->
            <form class="form-group text-left">
                <div class="form-group">
                    <label>Nombre de la madre</label>
                    <input type="text" class="form-control forml-control-user" placeholder="Nombre de la madre">
                </div>
            </form>


            <!-- Campo Servicio (área del hospitalizado) -->
            <form class="form-group text-left">
                <div class="form-group">
                    <label>Servicio (Área del hospitalizado)</label>
                    <input type="text" min="0" max="1000" class="form-control forml-control-user" placeholder="Servicio (Área del hospitalizado)">
                </div>
            </form>


            <!-- Campo Hora y Fecha de extración  -->
            <form class="form-group text-left">
                <div class="form-group">
                    <label>Hora de extracción</label>
                    <input type="time" min="0" max="50" class="form-control forml-control-user" placeholder="Hora de extración">
                </div>
                <div class="form-group">
                    <label>Fecha de extracción</label>
                    <input type="date" min="0" max="50" class="form-control forml-control-user" placeholder="Fecha de extracción">
                </div>
            </form>

            <!-- Campo Cantidad de leche recibida (ml) -->
            <form class="form-group text-left">
                <div class="form-group">
                    <label>Cantidad de leche recibida (ml)</label>
                    <input type="number" min="0" max="1000" class="form-control forml-control-user" placeholder="Cantidad de leche recibida (ml)">
                </div>
            </form>


            <!-- Boton Para Registrar Leche -->
            <button type="button" class="btn btn-warning btn-block boton-entrar-login text-white">Registrar Recepción de Leche</button>

            <!--<button type="button" class="btn btn-warning">Registrar Leche</button> -->



        </div>

        <!-- Columna Lateral Derecha -->
        <div class="col-lg-4 mb-2">
        </div>
    </div>

</div>




@endsection
