@extends('layout')
@section('cuerpo')

<section id="reporte-datos-leche">
  <!-- Preloader mostrado hasta que se cargue vue -->
  <div class="dflex" :hidden="mostrarPreloader ? false : true">
    <div class="preloader mt-5 mb-5"></div>
  </div>

  <!-- Contenedor Principal -->
  <div v-cloak class="pl-2 pr-2">
    <!-- Contenedor responsivo para una tabla -->
    <div class="table-responsive mt-3 mb-3">
      <!-- Tabla con los calendarios -->
      <table class="table text-center table-bordered table-sm">
        <!-- Encabezado Tabla -->
        <thead>
          <tr>
            <th style="width: 28%; min-width: 150px;">Fecha Inicio</th>
            <th style="width: 28%; min-width: 150px;">Fecha Fin</th>
            <th style="width: 16%; min-width: 100px;">Todas las fechas</th>
            <th style="width: 28%; min-width: 150px;">Acciones</th>
          </tr>
        </thead>
        <!-- FIN Encabezado Tabla -->

        <!-- Cuerpo de la tabla -->
        <tbody>
          <tr>
            <!-- Columna Fecha Inicial -->
            <td>
              <input type="date" class="form-control" v-model="fechaInicio" :disabled="todasLasFechas">
            </td>
            <!-- FIN Columna Fecha Inicial -->

            <!-- Columna Fecha Final -->
            <td>
              <input type="date" class="form-control" v-model="fechaFin" :disabled="todasLasFechas">
            </td>
            <!-- FIN Columna Fecha Final -->

            <!-- Columna check todas las fechas -->
            <td>
              <div class="custom-control custom-checkbox mt-2">
                <input
                  type="checkbox"
                  class="custom-control-input"
                  id="customCheck1"
                  v-model="todasLasFechas"
                  v-on:click="fechaInicio = null; fechaFin = null"
                >
                <label class="custom-control-label" for="customCheck1"></label>
              </div>
            </td>
            <!-- Columna check todas las fechas -->

            <!-- Columna Acciones -->
            <td>
              <button
                class="btn btn-primary"
                v-on:click="consultarDatos()"
                :disabled="cargando ? true: false"
              >
                <i class="fa" v-bind:class="[cargando ? 'fa-sync fa-spin': 'fa-search']"></i>
                Consultar
              </button>
            </td>
            <!-- FIN Columna Acciones -->
          </tr>
        </tbody>
        <!-- FIN Cuerpo de la tabla -->
      </table>
      <!-- FIN Tabla con los calendarios -->
    </div>
    <!-- FIN Contenedor responsivo para una tabla -->

    <!-- Tarjeta con los resultados del reporte -->
    <div class="card mb-3" :hidden="!reporteGenerado || cargando">
      <!-- Titulo de la tarjeta -->
      <div class="card-header">Datos Encontrados</div>

      <!-- Cuerpo de la tarjeta -->
      <div class="card-body">
        <!-- Contenedor responsivo para tabla -->
        <div class="table-responsive">
          <!-- Tabla con los totales obtenidos -->
          <table class="table text-center table-bordered">
            <!-- Encabezado Tabla -->
            <thead>
              <tr>
                <th>Promedio Leche Humana Consumida Por Niño (ml)</th>
                <th>Promedio Suced&aacute;neo Humana Consumida Por Niño (ml)</th>
                <th>Total Leche Humana Desechada (ml)</th>
                <th>Total Suced&aacute;neo Desechado (ml)</th>
              </tr>
            </thead>
            <!-- FIN Encabezado Tabla -->

            <!-- Cuerpo Tabla -->
            <tbody>
              <tr>
                <td v-for="total of totales">@{{ total }}</td>
              </tr>
            </tbody>
            <!-- Cuerpo Tabla -->
          </table>
          <!-- FIN Tabla con los totales obtenidos -->
        </div>
        <!-- FIN Contenedor responsivo para tabla -->
      </div>
      <!-- FIN Cuerpo de la tarjeta -->
    </div>
    <!-- FIN Tarjeta con los resultados del reporte -->

    <!-- Tarjeta grafica totales -->
    <div class="card mb-3" :hidden="!reporteGenerado || cargando">
      <!-- Titulo de la tarjeta -->
      <div class="card-header">Gr&aacute;fica Totales</div>

      <!-- Cuerpo Tarjeta -->
      <div class="card-body">
        <!-- Contenedor Grafica Totales -->
        <div style="display: flex; justify-content: center; align-items: center;">
          <div class="w-50 h-50">
            <canvas id="graficaTotalesLeche" width="400" height="400"></canvas>
          </div>
        </div>
        <!-- FIN Contenedor Grafica Totales -->
      </div>
      <!-- Fin Cuerpo Tarjeta -->
    </div>
    <!-- FIN Tarjeta grafica totales -->

    <!-- Tarjeta grafica promedios -->
    <div class="card mb-3" :hidden="!reporteGenerado || cargando">
      <!-- Titulo de la tarjeta -->
      <div class="card-header">Gr&aacute;fica Promedios</div>

      <!-- Cuerpo Tarjeta -->
      <div class="card-body">
        <!-- Contenedor Grafica Promedios -->
        <div style="display: flex; justify-content: center; align-items: center;">
          <div class="w-50 h-50">
            <canvas id="graficaPromediosLeche" width="400" height="400"></canvas>
          </div>
        </div>
        <!-- FIN Contenedor Grafica Promedios -->
      </div>
      <!-- Fin Cuerpo Tarjeta -->
    </div>
    <!-- FIN Tarjeta grafica promedios -->
  </div>
  <!-- FIN Contenedor Principal -->
</section>

<script>
var reporteDatosLeche = new Vue({
  el: '#reporte-datos-leche',
  data: {
    reporteGenerado: false,
    cargando: false,
    fechaInicio: null,
    fechaFin: null,
    mostrarPreloader: true,
    todasLasFechas: false,
    totales: [] // datos de prueba, se tienen que sustituir por la respuesta http
  },
  mounted: function() {
    this.mostrarPreloader = false;
  },
  methods: {
    consultarDatos: function() {
      // solo se envian los datos si se han indicado las dos fechas requeridas o si se marcó el check
      if ((this.fechaInicio && this.fechaFin) || this.todasLasFechas) {
        this.cargando = true;
        if (this.todasLasFechas) {
          // peticion sin mandar fechas
          axios.get('http://127.0.0.1:8000/api/reportes', {
            params: {
              tipo_total: 2
            }
          })
          .then( response => {
            if (!response.data.promedio_bebe_consumo) {
              this.totales.push(0);
            } else {
              this.totales.push(response.data.promedio_bebe_consumo);
            }
            if (!response.data.promedio_bebe_sucedaneo) {
              this.totales.push(0);
            } else {
              this.totales.push(response.data.promedio_bebe_sucedaneo);
            }
            this.totales.push(response.data.total);
            this.totales.push(response.data.sucedaneo);
            
            graficarTotalesLeche(this.totales.slice(0, 2));
            graficarPromediosLeche(this.totales.slice(2, 4));

            this.cargando = false;
            this.reporteGenerado = true;
          }).catch(error => {
            this.cargando = false;
            Swal.fire({
              icon: 'error',
              title: 'Algo salió muy mal!'
            });
          });
        } else {
          // peticion enviando las fechas
          axios.get('http://127.0.0.1:8000/api/reportes', {
            params: {
              tipo_total: 2,
              fecha_inicio: this.fechaInicio,
              fecha_fin: this.fechaFin
            }
          })
          .then( response => {
            if (!response.data.promedio_bebe_consumo) {
              this.totales.push(0);
            } else {
              this.totales.push(response.data.promedio_bebe_consumo);
            }
            if (!response.data.promedio_bebe_sucedaneo) {
              this.totales.push(0);
            } else {
              this.totales.push(response.data.promedio_bebe_sucedaneo);
            }
            this.totales.push(response.data.total);
            this.totales.push(response.data.sucedaneo);
            
            graficarTotalesLeche(this.totales.slice(0, 2));
            graficarPromediosLeche(this.totales.slice(2, 4));

            this.cargando = false;
            this.reporteGenerado = true;
          }).catch(error => {
            this.cargando = false;
            Swal.fire({
              icon: 'error',
              title: 'Algo salió muy mal!'
            });
          });
        }
      } else {
        Swal.fire({
          icon: 'warning',
          title: 'Atención',
          text: 'Seleccione ambas fechas antes de consultar el reporte'
        })
      }
    }
  }
});

function graficarTotalesLeche(totales) {
  var graficaTotales = document.getElementById('graficaTotalesLeche').getContext('2d');
  var myChart = new Chart(graficaTotales, {
    type: 'bar',
    data: {
      labels: ['Leche Humana', 'Sucedáneo'],
      datasets: [{
        label: 'Totales Desechados',
        data: totales,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)'
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
              beginAtZero: true
            }
        }]
      }
    }
  });
}

function graficarPromediosLeche(promedios) {
  var graficaPromedios = document.getElementById('graficaPromediosLeche').getContext('2d');
  var myChart = new Chart(graficaPromedios, {
    type: 'bar',
    data: {
      labels: ['Leche Humana', 'Sucedáneo'],
      datasets: [{
        label: 'Promedios Consumidos Por Niño',
        data: promedios,
        backgroundColor: [
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)'
        ],
        borderColor: [
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
              beginAtZero: true
            }
        }]
      }
    }
  });
}
</script>

@endsection
