@extends('layout')
@section('cuerpo')

<div id="recepcion">
<form id="form-recepcion-leche" onsubmit="return false">
  <!-- Contenedor Principal -->
  <div class="p-3 pl-2 pr-2">

    <div class="card mb-3">
      <!-- Titulo de la tarjeta ------------------------->
    <!--  <div class="card-header">RECEPCIÓN DE LECHE HUMANA</div> -->
    <div class="row justify-content-center">
      <h2 class="text-secondary">Recepción de Leche Humana</h2>

    </div>

      </div>

    <!-- Tarjeta para los datos de la madre donadora -->
    <div class="card mb-3">
      <!-- Titulo de la tarjeta -->
      <div class="card-header">Buscar Donadora</div>

      <!-- Cuerpo -->
      <div class="card-body">
        <!-- Primer Renglon -->
        <div class="row">
          <!-- campo nombre de la madre -->
          <div class="col-sm-4 form-group">
            <label>Nombre Donadora</label>
            <div class="input-group">
              <input
                id="nombreRecepcion"
                type="text"
                class="form-control"
                placeholder="Nombre"
                autocomplete="off"
                v-model="nom"
                name="nombreRecepcion"
              />

            </div>
            <!-- Solo para este caso el error tiene una ubicacion personalizada -->
            <div id="error-nombre-recepcion"></div>
          </div>

          <!-- Columna apellido paterno -->
          <div class="col-sm-4 form-group">
            <label>Apellido Paterno</label>
            <input
              id="apellidoPaternoRecepcion"
              type="text"
              class="form-control"
              placeholder="Apellido Paterno"
              autocomplete="off"
              v-model="apPat"
              name="apellidoPaternoRecepcion"
            />
          </div>

          <!-- Columna apellido materno -->
          <div class="col-sm-4 form-group">
            <label>Apellido Materno</label>
            <input
              id="apellidoMaternoRecepcion"
              type="text"
              class="form-control"
              v-model="apMat"
              placeholder="Apellido Materno"
              autocomplete="off"
              name="apellidoMaternoRecepcion"
            />
          </div>
          <!-- Columna ID de la madre -->
          <div class="col-sm-8 form-group">
            <label>ID de la madre</label>
            <input
              id="idMadre"
              type="text"
              class="form-control text-success"
              placeholder="ID Madre"
              autocomplete="off"
              v-model="datos_madre.ID_SISTEMA"
              name="idMadre"
              disabled
            />
          </div>

          <div class="col-sm-4 form-group">
            <center>
              <br>
              <button class="btn boton-entrar-login text-white" @click="getDatosMadre">
                <i class="fa fa-search"></i>
                  Buscar
              </button>
            </center>
          </div>
        </div> <!-- FIN Primer Renglon -->
      </div>
    </div>
    <!-- FIN Tarjeta para los datos de la madre donadora -->

    <div v-show="tipoDonacion == '1'">
      <!-- Tarjeta para los datos del bebé -->
      <div class="card mb-3">
        <!-- Titulo de la tarjeta -->
        <div class="card-header">Datos del bebé</div>

        <!-- Si la cantidad de bebés == 0 Se debe de registrar un bebé -->
        <!--<div v-if="cantidadBebes == '0'"> -->
          <div class="card-body">
            <div class="row">
              <div class="col-sm-6 form-group">
                <label>Fecha de Nacimiento del Bebé</label>
                <input
                  id="fechaNacimiento_bebe"
                  type="date"
                  class="form-control"
                  placeholder="Fecha de Nacimiento"
                  autocomplete="off"
                  v-model="fechaNacimiento_bebe"
                  name="fechaNacimiento_bebe"
                  value="">
              </div>

              <!-- Columna Hora de Nacimiento del Bebé -->
              <div class="col-sm-6 form-group">
                <label>Hora de Nacimiento del Bebé</label>
                <input
                  id="horaNacimiento_bebe"
                  type="time"
                  class="form-control"
                  placeholder="Hora de Nacimiento"
                  autocomplete="off"
                  v-model="horaNacimiento_bebe"
                  name="horaNacimiento_bebe"
                  />
              </div>

              <div class="col-sm-12 form-group">
                <center>
                  <br>
                  <button class="btn boton-entrar-login text-white" @click="registrarRelacion_BebeMadre">
                    <i class="fa fa-save"></i>
                      Guardar Bebé
                  </button>
                </center>
              </div>


              <div v-if = "cantidadBebes != '0'">
                <div class="col-sm-8 form-group">
                  <label>Bebé a alimentar</label>
                  <select class="form-control" id="combobox" v-model="bebeSeleccionado">
                    <option value="0" disabled># Fecha de Nacimiento - Hora de Nacimiento</option>
                    <option v-for="(bebe,index) in bebesMadre" :value="bebe">@{{index+1}}.  @{{bebe.FECHA_NACIMIENTO}} ------ @{{bebe.HORA_NACIMIENTO}}</option>
                  </select>

                </div>
              </div>


            </div>


          </div>

      </div> <!-- Tarjeta para los datos del bebé -->
    </div>



<!-- FIN Tarjeta para los datos del servicio ....................................... -->

<div v-if = "existe_madre == 'FALSE'">
    <!-- Tarjeta para los datos del frasco -->
    <div class="card">
      <!-- Titulo de la tarjeta ------------------------->
      <div class="card-header">Datos del Frasco</div>

      <!-- Cuerpo -->
      <div class="card-body">
        <!-- Primer Renglon -->
        <div class="row">
          <!-- Columna Tipo Domación -->
          <div class="col-sm-4 form-group">
            <label>Tipo de donación</label>
            <select v-model="tipoDonacion" class="form-control" name="tipoDonacion" id="tipoDonacion" disabled>
              <option value="0" disabled selected>Seleccione</option>
              <option value="1">Leche Homóloga</option>
              <option value="2">Leche Heterológa</option>
            </select>
          </div>
          <!-- Columna ID Frasco .................... -->
        <!--  <div class="col-sm-4 form-group">
            <label>ID del Frasco</label>
            <input
              id="idFrasco"
              type="text"
              class="form-control"
              placeholder="ID Frasco"
              autocomplete="off"
              name="idFrasco"
            />
          </div>
-->
          <!-- Columna Fecha de extracción -->
          <div class="col-sm-4 form-group">
            <label>Fecha de Extracción</label>
              <input
                id="fechaEx"
                type="date"
                class="form-control"
                placeholder="Fecha de Extracción"
                autocomplete="off"
                name="fechaEx"
                v-model="fechaEx"
                disabled
              />
          </div>

          <!-- Columna Hora Extracción -->
          <div class="col-sm-4 form-group">
            <label>Hora de Extracción</label>
              <input
                id="horaEX"
                type="time"
                class="form-control"
                placeholder="Hora de Extracción"
                autocomplete="off"
                name="horaEx"
                v-model="horaEx"
                disabled
              />
          </div>
          <!-- Columna Servicio (Área del hospitalizado) -->
          <div class="col-sm-8 form-group">
            <label>Servicio (Área del hospitalizado)</label>
            <input
              id="servicio"
              type="text"
              class="form-control"
              placeholder="Servicio (Área del hospitalizado)"
              autocomplete="off"
              name="servicio"
              v-model="servicio"
              disabled
            />
          </div>
          <!-- Cantidad de leche recibida (ml) -->
          <div class="col-sm-4 form-group">
            <label>Cantidad de leche recibida (ml)</label>
            <input
              id="cantidad"
              type="number"
              class="form-control"
              placeholder="Cantidad de leche recibida (ml)"
              autocomplete="off"
              name="cantidad"
              v-model="cantidad"
              disabled
            />
          </div>



        </div>
      </div>
    </div>
    <!-- FIN Tarjeta para los datos de resultados de analisis -->

    <div style="display: flex; justify-content: center;" class="mt-3">
      <button class="btn boton-entrar-login text-white" @click="verificarAccion()" disabled>
        <i class="fa fa-save"></i>
        Guardar Datos
      </button>
    </div>
    </div>


    <div v-else-if="existe_madre=='TRUE'">
      <!-- Tarjeta para los datos del frasco -->
      <div class="card">
        <!-- Titulo de la tarjeta ------------------------->
        <div class="card-header">Datos del Frasco</div>

        <!-- Cuerpo -->
        <div class="card-body">
          <!-- Primer Renglon -->
          <div class="row">
            <!-- Columna Tipo Domación -->
            <div class="col-sm-4 form-group">
              <label>Tipo de donación</label>
              <select v-model="tipoDonacion" class="form-control" name="tipoDonacion" id="tipoDonacion">
                <option value="0" disabled selected>Seleccione</option>
                <option value="1">Leche Homóloga</option>
                <option value="2">Leche Heterológa</option>
              </select>
            </div>
            <!-- Columna ID Frasco .................... -->
          <!--  <div class="col-sm-4 form-group">
              <label>ID del Frasco</label>
              <input
                id="idFrasco"
                type="text"
                class="form-control"
                placeholder="ID Frasco"
                autocomplete="off"
                name="idFrasco"
              />
            </div>
  -->
            <!-- Columna Fecha de extracción -->
            <div class="col-sm-4 form-group">
              <label>Fecha de Extracción</label>
                <input
                  id="fechaEx"
                  type="date"
                  class="form-control"
                  placeholder="Fecha de Extracción"
                  autocomplete="off"
                  name="fechaEx"
                  v-model="fechaEx"
                />
            </div>

            <!-- Columna Hora Extracción -->
            <div class="col-sm-4 form-group">
              <label>Hora de Extracción</label>
                <input
                  id="horaEX"
                  type="time"
                  class="form-control"
                  placeholder="Hora de Extracción"
                  autocomplete="off"
                  name="horaEx"
                  v-model="horaEx"
                />
            </div>
            <!-- Columna Servicio (Área del hospitalizado) -->
            <div class="col-sm-8 form-group">
              <label>Servicio (Área del hospitalizado)</label>
              <input
                id="servicio"
                type="text"
                class="form-control"
                placeholder="Servicio (Área del hospitalizado)"
                autocomplete="off"
                name="servicio"
                v-model="servicio"
              />
            </div>
            <!-- Cantidad de leche recibida (ml) -->
            <div class="col-sm-4 form-group">
              <label>Cantidad de leche recibida (ml)</label>
              <input
                id="cantidad"
                type="number"
                class="form-control"
                placeholder="Cantidad de leche recibida (ml)"
                autocomplete="off"
                name="cantidad"
                v-model="cantidad"
              />
            </div>



          </div>
        </div>
      </div>
      <!-- FIN Tarjeta para los datos de resultados de analisis -->

      <div style="display: flex; justify-content: center;" class="mt-3">
        <button class="btn boton-entrar-login text-white" @click="registrarRecepcion">
          <i class="fa fa-save"></i>
          Guardar Datos
        </button>
      </div>
    </div>
    <!-- FIN Contenedor div de vue -->
  </div>
</div>
  <!-- FIN Contenedor Principal -->
</form>

</div>
<script type="text/javascript">
  const app = new Vue({
    el: '#recepcion',
    data: {
      ID_SESION: '<?php echo  auth()->user()->id; ?>',
      mensaje: 'Hola',
      existe_madre: 'FALSE',
      id_madre: '',
      nom: '',
      apPat: '',
      apMat: '',
      idMadre: '',
      servicio: '',
      idFrasco: '',
      fechaEx: '',
      horaEx: '',
      tipoDonacion: '0',
      cantidad: '',

      cantidadBebes: '',
      fechaNacimiento_bebe: '',
      horaNacimiento_bebe: '',


      datos_madre: [],
      bebesMadre: [],
      bebeSeleccionado: {}
    },
    methods: {
      getDatosMadre(){
        //Usando AXIOS
        //http://127.0.0.1:8000/api/recepciones?id=HHOT26-05434
        axios.get('http://127.0.0.1:8000/api/recepciones', {
          params: {
            id_sesion: this.ID_SESION,
            nom: this.nom,
            apPat:this.apPat,
            apMat:this.apMat
          }
        }).then(
          response => {
            this.datos_madre = response.data.data;
            this.existe_madre = 'TRUE';
            this.id_madre = this.datos_madre.ID_SISTEMA;
            //Procedemos a verificar si la madre cuenta con un bebé registrado
            console.log("ID MADRE RECUPERADO: "+this.id_madre);
            this.getBebesMadre();

          }
        ).catch(
          error => {
            Swal.fire({
              icon: 'warning',
              title: 'Error, la madre no está registrada!'
            });
            //alert(error);
          }
        );
      },
      getBebesMadre(){
        axios.get('http://127.0.0.1:8000/api/bebes', {
          params: {
            id_sistema: this.id_madre
          }
        }).then(
          response => {
            this.bebesMadre = response.data.data;
            //this.existe_madre = 'TRUE';
            //Obtenemos la cantidad de bebés que tiene la madre
            this.cantidadBebes = this.bebesMadre.length;
            console.log(this.bebesMadre);
            console.log("LONGITUD ARREGLO: "+this.bebesMadre.length);

          }
        ).catch(
          error => {
            Swal.fire({
              icon: 'warning',
              title: 'Error, la madre no Tiene bebés!'
            });
            //alert(error);
          }
        );
      },
      registrarRecepcion(){ //Cuando Ya tiene bebés la madre, recibe y manda el ID del bebe
        //Primero validamos que se haya seleccionado un bebé
        if ( this.tipoDonacion == '' || this.fechaEx == '' || this.horaEX == '' || this.servicio == '' || this.cantidad == '' ){
          Swal.fire({
            position: 'center',
            icon: 'warning',
            title: 'Campos Vacíos, verifica por favor!',
            showConfirmButton: false,
            timer: 1500
          })
        }else{
          if ( this.tipoDonacion == '1' && this.bebeSeleccionado.ID_DATOS_BEBE ){
            if (this.fechaEx == '' || this.horaEX == '' || this.servicio == '' || this.cantidad == '' ) {
              Swal.fire({
                position: 'center',
                icon: 'warning',
                title: 'Campos Vacíos, verifica por favor!',
                showConfirmButton: false,
                timer: 1500
              })
            }else {
              console.log("Se está enviando el bebé: "+ this.bebeSeleccionado.ID_DATOS_BEBE);
              axios.post('http://127.0.0.1:8000/api/recepciones', {
                id_madre:this.datos_madre.ID_SISTEMA,
                areaHospital: this.servicio,
                fechaExtraccion: this.fechaEx,
                horaExtraccion: this.horaEx,
                tipoDonacion: this.tipoDonacion,
                cantidad: this.cantidad,
                id_bebe: this.bebeSeleccionado.ID_DATOS_BEBE
              }).then(
                  response => {
                    console.log("Todo bien registro recepcion");
                    Swal.fire({
                      title: 'Recepción Registrada Con Éxito',
                      text: "ID_FRASCO: "+response.data.data,
                      icon: 'success',
                      //showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      //cancelButtonColor: '#d33',
                      confirmButtonText: 'OK'
                    }).then((result) => {
                      if (result.value) {
                        console.log("Ok");
                      }
                    })
                  }
              ).catch(error => {
                alert("Error al Registrar la Recepción de la Leche "+ error);
              });
            }



          }else{
            if ( this.tipoDonacion == '1' && this.cantidadBebes == '0' ) {
              Swal.fire({
                position: 'center',
                icon: 'warning',
                title: 'Primero Debes registrar un bebé!',
                showConfirmButton: false,
                timer: 1500
              })

            }else {
              if (this.tipoDonacion == '1') {
                Swal.fire({
                  position: 'center',
                  icon: 'warning',
                  title: 'Debes seleccionar un bebé!',
                  showConfirmButton: false,
                  timer: 1500
                })
              }else {
                if ( this.tipoDonacion == '2' ) {
                  if (this.fechaEx == '' || this.horaEX == '' || this.servicio == '' || this.cantidad == '' ) {
                    Swal.fire({
                      position: 'center',
                      icon: 'warning',
                      title: 'Campos Vacíos, verifica por favor!',
                      showConfirmButton: false,
                      timer: 1500
                    })
                  }else {
                    axios.post('http://127.0.0.1:8000/api/recepciones', {
                      id_madre:this.datos_madre.ID_SISTEMA,
                      areaHospital: this.servicio,
                      fechaExtraccion: this.fechaEx,
                      horaExtraccion: this.horaEx,
                      tipoDonacion: this.tipoDonacion,
                      cantidad: this.cantidad,
                      id_bebe: this.bebeSeleccionado.ID_DATOS_BEBE
                    }).then(
                        response => {
                          console.log("Todo bien registro recepcion");
                          Swal.fire({
                            title: 'Recepción Registrada Con Éxito',
                            text: "ID_FRASCO: "+response.data.data,
                            icon: 'success',
                            //showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            //cancelButtonColor: '#d33',
                            confirmButtonText: 'OK'
                          }).then((result) => {
                            if (result.value) {
                              console.log("Ok");
                            }
                          })
                        }
                    ).catch(error => {
                      alert("Error al Registrar la Recepción de la Leche "+ error);
                    });
                  }
                }
              }
            }

          }
        }




      },
      registrarRelacion_BebeMadre(){
        if (this.fechaNacimiento_bebe == '' || this.horaNacimiento_bebe == '') {
          Swal.fire({
            position: 'center',
            icon: 'warning',
            title: 'Campos Vacíos, verifica por favor!',
            showConfirmButton: false,
            timer: 1500
          })
        }else{
          axios.post('http://127.0.0.1:8000/api/bebes', {
            id_sistema: this.id_madre,
            fecha_nacimiento: this.fechaNacimiento_bebe,
            hora_nacimiento: this.horaNacimiento_bebe
          }).then(
              response => {
                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Bebé Registrado',
                  showConfirmButton: false,
                  timer: 1500
                })
                this.fechaNacimiento_bebe = '';
                this.horaNacimiento_bebe = '';
                this.getBebesMadre();  //Recuperamos la lista de los bebés
                //swal("Registro Guardado!", "ID FRASCO: "+response.data.data, "success")
                //console.log("Todo bien");
                //alert("Recepción Registrada Con Éxito ID_FRASCO"+response.data.data);
              }
          ).catch(error => {
            alert("Error al Registrar la Relación Madre-Bebé "+ error);
          });
        }

      }

    },
    created: function () {
      console.log("Hola desde VUE");
      //this.getDatosMadre();
    }

  });
</script>
@endsection
