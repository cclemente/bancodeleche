@extends('layout')
@section('cuerpo')

<div id="dosificacion_sucedaneo">
  <form id="form-dosificacion-sucedaneo" onsubmit="return false">

    <!-- Contenedor Principal -->
    <div class="container p-3">

      <!-- Titulo de la tarjeta -->
      <div class="card mb-3">
        <div class="row justify-content-center">
          <h2 class="text-secondary">Dosificación Sucedáneo</h2>
        </div>
      </div>


      <!-- Tarjeta para los datos de la madre donadora -->
      <div class="card mb-3">
        <!-- Titulo de la tarjeta -->
        <div class="card-header">Buscar Donadora</div>

        <!-- Cuerpo -->
        <div class="card-body">
          <!-- Primer Renglon -->
          <div class="row">
            <!-- campo nombre de la madre -->
            <div class="col-sm-4 form-group">
              <label>Nombre Donadora</label>
              <div class="input-group">
                <input
                  id="nombre_donadora"
                  type="text"
                  class="form-control"
                  placeholder="Nombre"
                  autocomplete="off"
                  v-model="nom"
                  name="nombre_donadora"
                />

              </div>
              <!-- Solo para este caso el error tiene una ubicacion personalizada -->
              <div id="error-nombre"></div>
            </div>

            <!-- Columna apellido paterno -->
            <div class="col-sm-4 form-group">
              <label>Apellido Paterno</label>
              <input
                id="apellidoPaterno_donadora"
                type="text"
                class="form-control"
                placeholder="Apellido Paterno"
                autocomplete="off"
                v-model="apPat"
                name="apellidoPaterno_donadora"
              />
            </div>

            <!-- Columna apellido materno -->
            <div class="col-sm-4 form-group">
              <label>Apellido Materno</label>
              <input
                id="apellidoMaterno_donadora"
                type="text"
                class="form-control"
                v-model="apMat"
                placeholder="Apellido Materno"
                autocomplete="off"
                name="apellidoMaterno_donadora"
              />
            </div>

          </div>

          <!-- Segundo Renglon -->
          <div class="row">
            <!-- Columna ID de la madre -->
            <div class="col-sm-8 form-group">
              <label>ID de la madre</label>
              <input
                id="idMadre_donadora"
                type="text"
                class="form-control"
                placeholder="ID Madre"
                autocomplete="off"
                v-model="datos_madre.ID_SISTEMA"
                name="idMadre_donadora"
                disabled
              />
            </div>

            <div class="col-sm-4 form-group">
              <center>
                <br>
                <button class="btn boton-entrar-login text-white" @click="getDatosMadre">
                  <i class="fa fa-search"></i>
                    Buscar
                </button>
              </center>
            </div>
          </div> <!--Termina Segundo Renglon -->
        </div>
      </div>
      <!-- FIN Tarjeta para los datos de la madre donadora -->



      <div v-show="tipoDonacion == '1' || tipoDonacion == '2' ">
        <!-- Tarjeta para los datos del bebé -->
        <div class="card mb-3">
          <!-- Titulo de la tarjeta -->
          <div class="card-header">Datos del bebé</div>

          <!-- Si la cantidad de bebés == 0 Se debe de registrar un bebé -->
          <!--<div v-if="cantidadBebes == '0'"> -->
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6 form-group">
                  <label>Fecha de Nacimiento del Bebé</label>
                  <input
                    id="fechaNacimiento_bebe"
                    type="date"
                    class="form-control"
                    placeholder="Fecha de Nacimiento"
                    autocomplete="off"
                    v-model="fechaNacimiento_bebe"
                    name="fechaNacimiento_bebe"
                    value="">
                </div>

                <!-- Columna Hora de Nacimiento del Bebé -->
                <div class="col-sm-6 form-group">
                  <label>Hora de Nacimiento del Bebé</label>
                  <input
                    id="horaNacimiento_bebe"
                    type="time"
                    class="form-control"
                    placeholder="Hora de Nacimiento"
                    autocomplete="off"
                    v-model="horaNacimiento_bebe"
                    name="horaNacimiento_bebe"
                    />
                </div>

                <div class="col-sm-12 form-group">
                  <center>
                    <br>
                    <button class="btn boton-entrar-login text-white" @click="registrarRelacion_BebeMadre">
                      <i class="fa fa-save"></i>
                        Guardar Bebé
                    </button>
                  </center>
                </div>


                <div v-if = "cantidadBebes != '0'">
                  <div class="col-sm-8 form-group">
                    <label>Bebé a alimentar</label>
                    <select @change="getNumeroToma" class="form-control" id="combobox" v-model="bebeSeleccionado">
                      <option value="0" disabled># Fecha de Nacimiento - Hora de Nacimiento</option>
                      <option v-for="(bebe,index) in bebesMadre" :value="bebe">@{{index+1}}.  @{{bebe.FECHA_NACIMIENTO}} ------ @{{bebe.HORA_NACIMIENTO}}</option>
                    </select>

                  </div>
                </div>


              </div>


            </div>

        </div> <!-- Tarjeta para los datos del bebé -->
      </div>


















      <div v-if = "existe_madre == 'FALSE'">
        <!-- Tarjeta para el registro del sucedaneo preparado -->
        <div class="card mb-3">
          <!-- Titulo de la tarjeta -->
          <div class="card-header">Datos Sucedáneo</div>

          <!-- Cuerpo -->
          <div class="card-body">

            <!-- Primer Renglon -->
            <div class="row">
              <div class="col-sm-4 form-group">
                <label>Tipo de donación</label>
                <select v-model="tipoDonacion" class="form-control" name="tipoDonacion" id="tipoDonacion" disabled>
                  <option value="0" disabled selected>Seleccione</option>
                  <option value="1">Leche Homóloga</option>
                  <option value="2">Leche Heterológa</option>
                </select>
              </div>
            </div>



            <!-- Segundo Renglon -->
            <div class="row">
              <!-- Campo Fecha de Dosificación -->
              <div class="col-sm-3 form-group">
                <label>Fecha de Dosificación</label>
                  <input
                    id="fechaDosificacion"
                    type="date"
                    class="form-control"
                    placeholder="Fecha de Dosificación"
                    autocomplete="off"
                    name="fechaDosificacion"
                    v-model="fechaDosificacion"
                    disabled
                  />
              </div>

              <!-- Campo Hora Dosificación -->
              <div class="col-sm-3 form-group">
                <label>Hora de Dosificación</label>
                  <input
                    id="horaDosificacion"
                    type="time"
                    class="form-control"
                    placeholder="Hora de Dosificación"
                    autocomplete="off"
                    name="horaDosificacion"
                    v-model="horaDosificacion"
                    disabled
                  />
              </div>

              <!-- Campo Tipo de fórmula -->
              <div class="col-sm-3 form-group">
                <label>Tipo de fórmula</label>
                <select v-model="tipoFormula" class="form-control" name="tipoFormula" id="tipoFormula" disabled>
                  <option value="0" disabled>Seleccione</option>
                  <option value="1">Prematuro</option>
                  <option value="2">Inicio</option>
                  <option value="3">Continuación o seguimiento</option>
                  <option value="4">Especial</option>
                </select>
              </div>

              <!-- Cantidad de sucedaneo dosificado (ml) -->
              <div class="col-sm-3 form-group">
                <label>Cantidad dosificada (ml)</label>
                <input
                  id="cantidadDosificacion"
                  type="number"
                  min="0"
                  max="999999"
                  class="form-control"
                  placeholder="Cantidad dosificada (ml)"
                  autocomplete="off"
                  name="cantidadDosificacion"
                  v-model="cantidadDosificacion"
                  disabled
                />
              </div>
            </div> <!-- Fin Primer Renglon -->

            <!-- Segundo Renglon -->
            <div class="row">
              <!-- Columna Servicio (Área del hospitalizado) -->
              <div class="col-sm-4 form-group">
                <label>Servicio (Área del hospitalizado)</label>
                <input
                  id="servicioDosificacion"
                  type="text"
                  class="form-control"
                  placeholder="Servicio (Área del hospitalizado)"
                  autocomplete="off"
                  name="servicioDosificacion"
                  v-model="servicioDosificacion"
                  disabled
                />
              </div>

              <!-- Campo Número de Toma -->
              <div class="col-sm-4 form-group">
                <label>Número de toma</label>
                <select v-model="numeroToma" class="form-control" name="numeroToma" id="numeroToma" disabled>
                  <option value="0" disabled>Seleccione</option>
                  <option value="1">Primera</option>
                  <option value="2">Segunda</option>
                  <option value="3">Tercera</option>
                  <option value="4">Cuarta</option>
                  <option value="5">Quinta</option>
                  <option value="6">Sexta</option>
                  <option value="7">Séptima</option>
                  <option value="8">Octava</option>
                </select>
              </div>

              <!-- Cantidad de sucedaneo desechado (ml) -->
              <div class="col-sm-3 form-group">
                <label>Cantidad desechada (ml)</label>
                <input
                  id="cantidadDosificacion"
                  type="number"
                  min="0"
                  max="999999"
                  class="form-control"
                  placeholder="Cantidad desechada (ml)"
                  autocomplete="off"
                  name="cantidadDosificacion"
                  v-model="cantidadDosificacion"
                  disabled
                />
              </div>
            </div> <!-- Fin Segundo Renglon -->
          </div>
        </div> <!-- FIN Tarjeta para los datos del sucedaneo -->
        <div style="display: flex; justify-content: center;" class="mt-3">
          <button class="btn boton-entrar-login text-white" @click="" disabled>
            <i class="fa fa-save"></i>
            Guardar Datos
          </button>
        </div>
      </div> <!-- Fin Div IF -->



      <div v-else-if="existe_madre == 'TRUE'">
        <!-- Tarjeta para el registro del sucedaneo preparado -->
        <div class="card mb-3">
          <!-- Titulo de la tarjeta -->
          <div class="card-header">Datos Sucedáneo</div>

          <!-- Cuerpo -->
          <div class="card-body">
            <!-- Primer Renglon -->
            <div class="row">
              <div class="col-sm-4 form-group">
                <label>Tipo de donación</label>
                <select v-model="tipoDonacion" class="form-control" name="tipoDonacion" id="tipoDonacion">
                  <option value="0" disabled selected>Seleccione</option>
                  <option value="1">Leche Homóloga</option>
                  <option value="2">Leche Heterológa</option>
                </select>
              </div>
            </div>


            <!-- Primer Renglon -->
            <div class="row">
              <!-- Campo Fecha de Dosificación -->
              <div class="col-sm-3 form-group">
                <label>Fecha de Dosificación</label>
                  <input
                    @change="getNumeroToma"
                    id="fechaDosificacion"
                    type="date"
                    class="form-control"
                    placeholder="Fecha de Dosificación"
                    autocomplete="off"
                    name="fechaDosificacion"
                    v-model="fechaDosificacion"
                  />
              </div>

              <!-- Campo Hora Dosificación -->
              <div class="col-sm-3 form-group">
                <label>Hora de Dosificación</label>
                  <input
                    id="horaDosificacion"
                    type="time"
                    class="form-control"
                    placeholder="Hora de Dosificación"
                    autocomplete="off"
                    name="horaDosificacion"
                    v-model="horaDosificacion"
                  />
              </div>

              <!-- Campo Tipo de fórmula -->
              <div class="col-sm-3 form-group">
                <label>Tipo de fórmula</label>
                <select v-model="tipoFormula" class="form-control" name="tipoFormula" id="tipoFormula">
                  <option value="0" disabled>Seleccione</option>
                  <option value="1">Prematuro</option>
                  <option value="2">Inicio</option>
                  <option value="3">Continuación o seguimiento</option>
                  <option value="4">Especial</option>
                </select>
              </div>

              <!-- Cantidad de sucedaneo dosificado (ml) -->
              <div class="col-sm-3 form-group">
                <label>Cantidad dosificada (ml)</label>
                <input
                  id="cantidadDosificacion"
                  type="number"
                  min="0"
                  max="999999"
                  class="form-control"
                  placeholder="Cantidad dosificada (ml)"
                  autocomplete="off"
                  name="cantidadDosificacion"
                  v-model="cantidadDosificacion"
                />
              </div>
            </div> <!-- Fin Primer Renglon -->

            <!-- Segundo Renglon -->
            <div class="row">
              <!-- Columna Servicio (Área del hospitalizado) -->
              <div class="col-sm-4 form-group">
                <label>Servicio (Área del hospitalizado)</label>
                <input
                  id="servicioDosificacion"
                  type="text"
                  class="form-control"
                  placeholder="Servicio (Área del hospitalizado)"
                  autocomplete="off"
                  name="servicioDosificacion"
                  v-model="servicioDosificacion"
                />
              </div>

              <div class="col-sm-4 form-group" v-show="tipoDonacion=='1'">
                <label>Número de toma</label>
                <select v-model="numeroToma" class="form-control" name="numeroToma" id="numeroToma">
                  <option value="0" disabled selected>Seleccione</option>
                  <option value="1">Correcto</option>
                  <option value="2">Incorrecto</option>
                </select>
              </div>

              <div class="col-sm-4 form-group" v-show="tipoDonacion=='2'">
                <label>Número de toma</label>
                <input
                  id="numeroToma"
                  type="number"
                  class="form-control"
                  placeholder="Número de toma"
                  autocomplete="off"
                  name="numeroToma"
                  v-model="numeroTomaHeterologa"
                  disabled
                />
              </div>

              <!-- Cantidad de sucedaneo desechado (ml) -->
              <div class="col-sm-3 form-group">
                <label>Cantidad desechada (ml)</label>
                <input
                  id="cantidadDesechada"
                  type="number"
                  min="0"
                  max="999999"
                  class="form-control"
                  placeholder="Cantidad desechada (ml)"
                  autocomplete="off"
                  name="cantidadDesechada"
                  v-model="cantidadDesechada"
                />
              </div>
            </div> <!-- Fin Segundo Renglon -->
          </div>
        </div> <!-- FIN Tarjeta para los datos del sucedaneo -->
        <div style="display: flex; justify-content: center;" class="mt-3">
          <button class="btn boton-entrar-login text-white" @click="registrarDosificacion_Sucedaneo">
            <i class="fa fa-save"></i>
            Guardar Datos
          </button>
        </div>
      </div> <!-- Fin Div ELSE-IF -->

    </div> <!--Fin Container Principal -->
  </form>
</div>



<script type="text/javascript">
  const app = new Vue({
    el: '#dosificacion_sucedaneo',
    data: {
      ID_SESION: '<?php echo  auth()->user()->id; ?>',
      existe_madre: 'FALSE',
      nom: '',
      apPat: '',
      apMat: '',
      idMadre: '',
      //Variables del formulario
      fechaDosificacion: '',
      horaDosificacion: '',
      tipoFormula: '0',
      cantidadDosificacion: '',
      servicioDosificacion: '',
      numeroToma: '0',
      cantidadDesechada: '',

      datos_madre: [],
      bebesMadre: [],
      id_madre: '',
      tipoDonacion: '0',
      fechaNacimiento_bebe: '',
      horaNacimiento_bebe: '',
      cantidadBebes: '',
      bebeSeleccionado: {},
      aa:'',
      mes:'',
      dia:'',
      numeroTomaHeterologa: 0,
      numeroToma: '0'

    },
    methods: {
      getDatosMadre(){
        //Usando AXIOS
        //http://127.0.0.1:8000/api/recepciones?id=HHOT26-05434
        axios.get('http://127.0.0.1:8000/api/recepciones', {
          params: {
            id_sesion: this.ID_SESION-1,
            nom: this.nom,
            apPat:this.apPat,
            apMat:this.apMat
          }
        }).then(
          response => {
            this.datos_madre = response.data.data;
            this.existe_madre = 'TRUE';
            this.id_madre = this.datos_madre.ID_SISTEMA;
            //Procedemos a verificar si la madre cuenta con un bebé registrado
            console.log("ID MADRE RECUPERADO: "+this.id_madre);
            this.getBebesMadre();

          }
        ).catch(
          error => {
            Swal.fire({
              icon: 'warning',
              title: 'Error, la madre no está registrada!'
            });
            //alert(error);
          }
        );
      },
      getBebesMadre(){
        axios.get('http://127.0.0.1:8000/api/bebes', {
          params: {
            id_sistema: this.id_madre
          }
        }).then(
          response => {
            this.bebesMadre = response.data.data;
            //this.existe_madre = 'TRUE';
            //Obtenemos la cantidad de bebés que tiene la madre
            this.cantidadBebes = this.bebesMadre.length;
            console.log(this.bebesMadre);
            console.log("LONGITUD ARREGLO: "+this.bebesMadre.length);

          }
        ).catch(
          error => {
            Swal.fire({
              icon: 'warning',
              title: 'Error, la madre no Tiene bebés!'
            });
            //alert(error);
          }
        );
      },
      registrarRelacion_BebeMadre(){
        if (this.fechaNacimiento_bebe == '' || this.horaNacimiento_bebe == '') {
          Swal.fire({
            position: 'center',
            icon: 'warning',
            title: 'Campos Vacíos, verifica por favor!',
            showConfirmButton: false,
            timer: 1500
          })
        }else{
          axios.post('http://127.0.0.1:8000/api/bebes', {
            id_sistema: this.id_madre,
            fecha_nacimiento: this.fechaNacimiento_bebe,
            hora_nacimiento: this.horaNacimiento_bebe
          }).then(
              response => {
                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Bebé Registrado',
                  showConfirmButton: false,
                  timer: 1500
                })
                this.fechaNacimiento_bebe = '';
                this.horaNacimiento_bebe = '';
                this.getBebesMadre();  //Recuperamos la lista de los bebés
                //swal("Registro Guardado!", "ID FRASCO: "+response.data.data, "success")
                //console.log("Todo bien");
                //alert("Recepción Registrada Con Éxito ID_FRASCO"+response.data.data);
              }
          ).catch(error => {
            alert("Error al Registrar la Relación Madre-Bebé "+ error);
          });
        }

      },
      getNumeroToma(){
        fecha_completa = new Date();
        this.dia = fecha_completa.getDate();
        this.mes = fecha_completa.getMonth()+1;
        this.aa = fecha_completa.getFullYear();
        fecha_actual = this.aa + "/" + this.mes + "/" + this.dia;
        console.log(this.bebeSeleccionado.ID_DATOS_BEBE+" fecha "+fecha_actual);
        axios.get('http://127.0.0.1:8000/api/dosificaciones', {
          params: {
            id_sistema:this.datos_madre.ID_SISTEMA,
            fecha: this.fechaDosificacion,
            id_bebe: this.bebeSeleccionado.ID_DATOS_BEBE
          }
        }).then(
          response => {
            chochito = response.data.data;
            //Procedemos a verificar si la madre cuenta con un bebé registrado
            console.log("Número de toma RECUPERADO: "+chochito);
            if (this.tipoDonacion=="1") {
              console.log("Toma de Homologa: "+this.numeroToma);
            }else {
              if (this.tipoDonacion == "2") {
                console.log("MOSTRANDO TOMA");
                this.numeroTomaHeterologa = chochito;
                this.numeroTomaHeterologa = chochito+1;
                console.log(this.numeroTomaHeterologa);
                //this.numero_toma = chochito+1;
              }
            }
          }
        ).catch(
          error => {
            /*Swal.fire({
              icon: 'warning',
              title: 'Error, toma no resgistrada!'
            });*/
            //alert(error);
          }
        );
      },
      registrarDosificacion_Sucedaneo(){
          //this.getNumeroToma();
          //PRIMERO VALIDAMOS GENERAL
          if ( this.tipoDonacion == '0' ) {
            Swal.fire({
              position: 'center',
              icon: 'warning',
              title: 'Selecciona Tipo de Donación!',
              showConfirmButton: false,
              timer: 1500
            })
          }else {
            //VALIDAMOS HOMOLOGA
            if (this.tipoDonacion == '1' || this.tipoDonacion == '2' ) {
              //Verificamos que haya bebés para esa madre
              if ( this.cantidadBebes == '0' ) {
                Swal.fire({
                  position: 'center',
                  icon: 'warning',
                  title: 'Primero Debes registrar un bebé!',
                  showConfirmButton: false,
                  timer: 1500
                })
              }else {
                //Verificamos que haya seleccionado
                if (this.bebeSeleccionado.ID_DATOS_BEBE) {
                  //Verificar los campos de Tipo Donacion
                  if( this.tipoDonacion == '1' ){ //HOMOLOGA
                    if ( this.fechaDosificacion == '' || this.horaDosificacion == '' || this.tipoFormula=='0' || this.cantidadDosificacion == '' || this.servicioDosificacion == '' || this.numeroToma == '0' || this.cantidadDesechada == ''  ) {
                      Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        title: 'Campos Vacíos, verifica por favor!',
                        showConfirmButton: false,
                        timer: 1500
                      })
                    }else {
                      //Usuar AXIOS
                      axios.post('http://127.0.0.1:8000/api/dosificacionesdos', {
                        id_sistema: this.id_madre,
                        fecha: this.fechaDosificacion,
                        hora: this.horaDosificacion,
                        servicio: this.servicioDosificacion,
                        tipoFormula: this.tipoFormula,
                        dosificacion: this.cantidadDosificacion,
                        cantidad_desechada: this.cantidadDesechada,
                        id_bebe: this.bebeSeleccionado.ID_DATOS_BEBE,
                      }).then(
                        response => {
                          Swal.fire({
                            icon: 'success',
                            title: 'Registro Guardado!'
                          });
                          //alert("Recepción Registrada Con Éxito ID_FRASCO"+response.data.data);
                        }
                      ).catch(error => {
                        alert(error);
                      });
                    }
                  }else {
                    if (this.tipoDonacion == '2') { //HETEROLOGA
                      if ( this.fechaDosificacion == '' || this.horaDosificacion == '' || this.tipoFormula=='0' || this.cantidadDosificacion == '' || this.servicioDosificacion == '' || this.cantidadDesechada == ''  ) {
                        Swal.fire({
                          position: 'center',
                          icon: 'warning',
                          title: 'Campos Vacíos, verifica por favor!',
                          showConfirmButton: false,
                          timer: 1500
                        })
                      }else {
                        axios.post('http://127.0.0.1:8000/api/dosificacionesdos', {
                          id_sistema: this.id_madre,
                          fecha: this.fechaDosificacion,
                          hora: this.horaDosificacion,
                          servicio: this.servicioDosificacion,
                          tipoFormula: this.tipoFormula,
                          dosificacion: this.cantidadDosificacion,
                          cantidad_desechada: this.cantidadDesechada,
                          id_bebe: this.bebeSeleccionado.ID_DATOS_BEBE,
                        }).then(
                          response => {
                            Swal.fire({
                              icon: 'success',
                              title: 'Registro Guardado!'
                            });
                            //alert("Recepción Registrada Con Éxito ID_FRASCO"+response.data.data);
                          }
                        ).catch(error => {
                          alert(error);
                        });
                      }
                    }
                  }


                }else {
                  Swal.fire({
                    position: 'center',
                    icon: 'warning',
                    title: 'Debes seleccionar un bebé!',
                    showConfirmButton: false,
                    timer: 1500
                  })
                }
              }
            }
          }





      }
    },
    created: function () {
    }

  });
</script>

@endsection
