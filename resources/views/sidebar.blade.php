<section class="contenedor-sidebar">
  <div class="sidebar">
    <!-- Enlace Inicio -->
    <a
      href="{{ url('inicio') }}"
      class="{{
        (request()->is('inicio'))
        ? 'active'
        : ''
      }}"
    >
      Inicio
    </a>

    @if(auth()->user()->ID_ROL_SISTEMA == 1)
      <!-- Enlace Registro Donadora -->
      <a
        href="{{ url('registro_donadora') }}"
        class="{{
          (request()->is('registro_donadora'))
          ? 'active'
          : ''
        }}"
      >
        Registro Donadora
      </a>
    
      <!-- Enlace Recepcion Leche -->
      <a
        href="{{ url('recepcion_leche') }}"
        class="{{
          (request()->is('recepcion_leche'))
          ? 'active'
          : ''
        }}"
      >
        Recepci&oacute;n Leche
      </a>
    @endif

    @if(auth()->user()->ID_ROL_SISTEMA == 2)
      <!-- Enlace Dosificacion Homologa -->
      <a
        href="{{ url('dosificacion_homologa') }}"
        class="{{
          (request()->is('dosificacion_homologa'))
          ? 'active'
          : ''
        }}"
      >
        Dosificaci&oacute;n Leche 
      </a>

      <!-- Enlace Analisis de Leche Heterologa -->
      <a
        href="{{ url('analisis_leche') }}"
        class="{{
          (request()->is('analisis_leche'))
          ? 'active'
          : ''
        }}"
      >
        An&aacute;lisis Leche
      </a>

      <!-- Enlace Pasteurizacion -->
      <a
        href="{{ url('pasteurizacion') }}"
        class="{{
          (request()->is('pasteurizacion'))
          ? 'active'
          : ''
        }}"
      >
        Pasteurizaci&oacute;n Leche
      </a>

      <!-- Boton para accionar el dropdown de Sucedaneo -->
      <a class="dropdown-btn-sucedaneo">Suced&aacute;neo
        <i class="fa fa-caret-down"></i>
      </a>
      <!-- Elementos contenidos en el dropdown menu -->
      <div class="dropdown-container">
        <!-- Enlace a reporte Consultar Totales -->
        <a
          href="{{ url('preparacion_sucedaneo') }}"
          class="{{
            (request()->is('preparacion_sucedaneo'))
            ? 'active'
            : ''
          }}"
        >
          Preparaci&oacute;n
        </a>
        <a
          href="{{ url('dosificacion_sucedaneo') }}"
          class="{{
            (request()->is('dosificacion_sucedaneo'))
            ? 'active'
            : ''
          }}"
        >
          Dosificaci&oacute;n</a>
      </div>
    @endif

    @if(auth()->user()->ID_ROL_SISTEMA == 3)
      <!-- Boton para accionar el dropdown de reportes -->
      <a class="dropdown-btn">Reportes
        <i class="fa fa-caret-down"></i>
      </a>
      <!-- Elementos contenidos en el dropdown menu -->
      <div class="dropdown-container">
        <!-- Enlace a reporte Consultar Totales -->
        <a
          href="{{ url('reporte_totales') }}"
          class="{{
            (request()->is('reporte_totales'))
            ? 'active'
            : ''
          }}"
        >
          Consultar Totales
        </a>
        <a
          href="{{ url('reporte_datos_leche') }}"
          class="{{
            (request()->is('reporte_datos_leche'))
            ? 'active'
            : ''
          }}"
        >
          Datos Leche
        </a>
      </div>

      <!-- Enlace Productivdad Estatal -->
      <a
        href="{{ url('productividad_estatal') }}"
        class="{{
          (request()->is('productividad_estatal'))
          ? 'active'
          : ''
        }}"
      >
        Productivdad Estatal
      </a>
    @endif

    <!-- Enlace Cerrar Sesion -->
    <a  href="{{ route('logout')}}"  onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
      Salir
      <i class="fa fa-sign-out-alt"></i>
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
  </div>
</section>
