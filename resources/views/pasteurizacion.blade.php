@extends('layout')
@section('cuerpo')

<!-- Contenedor Principal -->
<div class="p-3 pl-2 pr-2">
  <!-- Formulario Buscador -->
  <form id="form-buscador-pasteurizacion">
    <!-- Tarjeta Para Buscar Frasco --> 
    <div class="card mb-3">
      <!-- Titulo de la tarjeta -->
      <div class="card-header">Buscar Frasco Leche Por Id de An&aacute;lisis</div>

      <!-- Cuerpo de la tarjeta -->
      <div class="card-body">
        <!-- Primer Renglon -->
        <div class="row">
          <!-- Columna Buscador -->
          <div class="col-sm-6 form-group">
            <label>Identificador Frasco</label>
            <div class="input-group">
              <input
                id="inputIdAnalisisLeche"
                type="text"
                class="form-control"
                placeholder="ID Frasco"
                autocomplete="off"
                name="inputIdAnalisisLeche"
              />
              <div class="input-group-append">
                <button class="btn boton-entrar-login text-white btn-buscar" type="submit">
                  <i class="fa fa-search"></i>
                  Buscar
                </button>
              </div>
            </div>
            <!-- Solo para este caso el error tiene una ubicacion personalizada -->
            <div id="error-input-id-analisis-leche"></div>
          </div>
          <!-- FIN Columna Buscador -->

          <!-- Columna Identidicador Frasco -->
          <div class="col-sm-6 form-group">
            <label>Identificador Encontrado</label>
            <input disabled class="form-control" id="frasco-encontrado-pasteurizacion">
          </div>
          <!-- FIN Columna Identidicador Frasco -->
        </div>
        <!-- FIN Primer Renglon -->
      </div>
      <!-- FIN Cuerpo de la tarjeta -->
    </div>
    <!-- FIN Tarjeta Para Buscar Frasco -->
  </form>
  <!-- FIN Formulario Buscador -->

  <!-- Formulario Resultados Pasteurizacion -->
  <form id="form-resultados-pasteurizacion">
    <!-- Inicio Tarjeta -->
    <div class="card mb-3">
      <!-- Titulo de la tarjeta -->
      <div class="card-header">Resultados de Pasteurizaci&oacute;n</div>

      <!-- Cuerpo de la tarjeta -->
      <div class="card-body">
        <!-- Primer renglon -->
        <div class="row">
          <!-- Seleccion de acidez dornic -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Acidez Dornic</label>
            <select disabled class="form-control" id="selectAcidez" name="selectAcidez">
              <option value="" disabled selected>Seleccione</option>
              <option value="1">Normal</option>
              <option value="2">Anormal</option>
            </select>
          </div>

          <!-- Seleccion de pasteurizacion -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Pasteurizaci&oacute;n</label>
            <select disabled class="form-control" id="selectPasteurizacion" name="selectPasteurizacion">
              <option value="" disabled selected>Seleccione</option>
              <option value="1">Normal</option>
              <option value="2">Anormal</option>
            </select>
          </div>

          <!-- Campo Cantidad A Almacenar -->
          <div class="col-sm-4 form-group">
            <label class="form-label">Cantidad a Almacenar (ml)</label>
            <input
              disabled
              id="inputAlmacenarPast"
              name="inputAlmacenarPast"
              type="text"
              class="form-control"
              placeholder="Cantidad"
              autocomplete="off"
            />
          </div>
        </div>
        <!-- FIN Primer renglon -->
      </div>
      <!-- FIN Cuerpo de la tarjeta -->
    </div>
    <!-- FIN Tarjeta -->

    <!-- Opcion para guardar los datos -->
    <div style="display: flex; justify-content: center;" class="mt-3">
      <button disabled class="btn boton-entrar-login text-white btn-guardar" type="submit">
        <i class="fa fa-save"></i>
        Guardar Datos
      </button>
    </div>
    <!-- FIN Opcion para guardar los datos -->
  </form>
  <!-- FIN Formulario Resultados Pasteurizacion -->
</div>
<!-- FIN Contenedor Principal -->

@endsection
