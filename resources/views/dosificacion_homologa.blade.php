@extends('layout')
@section('cuerpo')


<div id="dosificacionHomologa">
<form id="form-dosificacion-homologa" onsubmit="return false">
  <!-- Contenedor Principal -->
  <div class="p-3 pl-2 pr-2">

    <div class="card mb-3">
      <!-- Titulo de la tarjeta ------------------------->
    <!--  <div class="card-header">DOSIFICACIÓN HOMOLOGA</div> -->
    <div class="row justify-content-center">
      <h2 class="text-secondary">Dosificación de Leche Humana</h2>

    </div>

      </div>

    <!-- Tarjeta para los datos de la madre donadora -->
    <div class="card mb-3">
      <!-- Titulo de la tarjeta -->
      <div class="card-header">Buscar Donadora</div>

      <!-- Cuerpo -->
      <div class="card-body">
        <!-- Primer Renglon -->
        <div class="row">
          <!-- campo nombre de la madre -->
          <div class="col-sm-4 form-group">
            <label>Nombre Donadora</label>
            <div class="input-group">
              <input
                id="nombreDosificacionHomo"
                type="text"
                class="form-control"
                placeholder="Nombre"
                autocomplete="off"
                v-model="nombreDosificacion"
                name="nombreDosificacionHomo"
              />

            </div>
            <!-- Solo para este caso el error tiene una ubicacion personalizada -->
            <div id="error-nombre-dosificacion"></div>
          </div>

          <!-- Columna apellido paterno -->
          <div class="col-sm-4 form-group">
            <label>Apellido Paterno</label>
            <input
              id="apellidoPaternoDosificacion"
              type="text"
              class="form-control"
              placeholder="Apellido Paterno"
              autocomplete="off"
              v-model="apellidoPaternoDosificacion"
              name="apellidoPaternoDosificacion"
            />
          </div>

          <!-- Columna apellido materno -->
          <div class="col-sm-4 form-group">
            <label>Apellido Materno</label>
            <input
              id="apellidoMaternoDosificacion"
              type="text"
              class="form-control"
              v-model="apellidoMaternoDosificacion"
              placeholder="Apellido Materno"
              autocomplete="off"
              name="apellidoMaternoDosificacion"
            />
          </div>
          <!-- Columna ID de la madre -->
          <div class="col-sm-8 form-group">
            <label>ID de la madre</label>
            <input
              id="idMadre2"
              type="text"
              class="form-control text-success"
              placeholder="ID Madre"
              autocomplete="off"
              v-model="datos_madre2.ID_SISTEMA"
              name="idMadre2"
              disabled
            />
          </div>

          <div class="col-sm-4 form-group">
            <center>
              <br>
              <button class="btn boton-entrar-login text-white" @click="getDatosMadre">
                <i class="fa fa-search"></i>
                  Buscar
              </button>
            </center>
          </div>
        </div> <!-- FIN Primer Renglon -->
      </div>
    </div>
    <!-- FIN Tarjeta para los datos de la madre donadora -->

    <div v-show="tipoDonacion2 == '1' || tipoDonacion2 == '2' ">
      <!-- Tarjeta para los datos del bebé -->
      <div class="card mb-3">
        <!-- Titulo de la tarjeta -->
        <div class="card-header">Datos del bebé</div>

        <!-- Si la cantidad de bebés == 0 Se debe de registrar un bebé -->
        <!--<div v-if="cantidadBebes == '0'"> -->
          <div class="card-body">
            <div class="row">
              <div class="col-sm-6 form-group">
                <label>Fecha de Nacimiento del Bebé</label>
                <input
                  id="fechaNacimiento_bebe"
                  type="date"
                  class="form-control"
                  placeholder="Fecha de Nacimiento"
                  autocomplete="off"
                  v-model="fechaNacimiento_bebe"
                  name="fechaNacimiento_bebe"
                  value="">
              </div>

              <!-- Columna Hora de Nacimiento del Bebé -->
              <div class="col-sm-6 form-group">
                <label>Hora de Nacimiento del Bebé</label>
                <input
                  id="horaNacimiento_bebe"
                  type="time"
                  class="form-control"
                  placeholder="Hora de Nacimiento"
                  autocomplete="off"
                  v-model="horaNacimiento_bebe"
                  name="horaNacimiento_bebe"
                  />
              </div>

              <div class="col-sm-12 form-group">
                <center>
                  <br>
                  <button class="btn boton-entrar-login text-white" @click="registrarRelacion_BebeMadre">
                    <i class="fa fa-save"></i>
                      Guardar Bebé
                  </button>
                </center>
              </div>


              <div v-if = "cantidadBebes != '0'">
                <div class="col-sm-8 form-group">
                  <label>Bebé a alimentar</label> <!-- @change="getNumeroToma"   -->
                  <select @change="getNumeroToma" class="form-control" id="combobox" v-model="bebeSeleccionado">
                    <option value="0" disabled># Fecha de Nacimiento - Hora de Nacimiento</option>
                    <option v-for="(bebe,index) in bebesMadre" :value="bebe">@{{index+1}}.  @{{bebe.FECHA_NACIMIENTO}} ------ @{{bebe.HORA_NACIMIENTO}}</option>
                  </select>

                </div>
              </div>


            </div>


          </div>

      </div> <!-- Tarjeta para los datos del bebé -->
    </div>





<div v-if = "existe_madre2 == 'FALSE'">
    <!-- Tarjeta para los datos de dosificación -->
    <div class="card">
      <!-- Titulo de la tarjeta ------------------------->
      <div class="card-header">Datos de Dosificación</div>

      <!-- Cuerpo -->
      <div class="card-body">
        <!-- Primer Renglon -->
        <div class="row">
          <!-- Columna Tipo Donacion -->
          <div class="col-sm-4 form-group">
            <label>Tipo de donación</label>
            <select v-model="tipoDonacion2" class="form-control" name="tipoDonacion2" id="tipoDonacion2" disabled>
              <option value="0" disabled selected>Seleccione</option>
              <option value="1">Leche Homóloga</option>
              <option value="2">Leche Heterológa</option>
            </select>
          </div>

          <!-- Columna Fecha de dosificacion -->
          <div class="col-sm-4 form-group">
            <label>Fecha de Dosificación</label>
              <input
                @change="getNumeroToma"
                id="fechaDosificacion"
                type="date"
                class="form-control"
                placeholder="Fecha de Dosificación"
                autocomplete="off"
                name="fechaDosificacion"
                v-model="fechaDosificacion"
                disabled
              />
          </div>

          <!-- Columna Hora dosificacion -->
          <div class="col-sm-4 form-group">
            <label>Hora de Dosificación</label>
              <input
                id="horaDosificacion"
                type="time"
                class="form-control"
                placeholder="Hora de Dosificación"
                autocomplete="off"
                name="horaDosificacion"
                v-model="horaDosificacion"
                disabled
              />
          </div>
          <!-- Columna Servicio (Área del hospitalizado) -->
          <div class="col-sm-8 form-group">
            <label>Servicio (Área del hospitalizado)</label>
            <input
              id="servicio2"
              type="text"
              class="form-control"
              placeholder="Servicio (Área del hospitalizado)"
              autocomplete="off"
              name="servicio2"
              v-model="servicio2"
              disabled
            />
          </div>
          <!-- Dosificación(ml) -->
          <div class="col-sm-4 form-group">
            <label>Dosificación (ml)</label>
            <input
              id="dosificacion"
              type="number"
              class="form-control"
              placeholder="Dosificación (ml)"
              autocomplete="off"
              name="dosificacion"
              v-model="dosificacion"
              disabled
            />
          </div>
          <div class="col-sm-4 form-group">
            <label>Administración (ml)</label>
            <input
              id="administracion"
              type="number"
              class="form-control"
              placeholder="Administración (ml)"
              autocomplete="off"
              name="administracion"
              v-model="administracion"
              disabled
            />
          </div>
          <div class="col-sm-4 form-group">
            <label>Número de toma</label>
            <input
              id="numeroToma"
              type="number"
              class="form-control"
              placeholder="Número de toma"
              autocomplete="off"
              name="numeroToma"
              v-model="numeroToma"
              disabled
            />
          </div>


        </div>
      </div>
    </div>
    <!-- FIN Tarjeta para los datos de dosificacion -->

    <div style="display: flex; justify-content: center;" class="mt-3">
      <button class="btn boton-entrar-login text-white"  disabled>
        <i class="fa fa-save"></i>
        Guardar Datos
      </button>
    </div>
    </div>


    <div v-else-if="existe_madre2=='TRUE'">
      <!-- Tarjeta para los datos de dosificación -->
      <div class="card">
        <!-- Titulo de la tarjeta ------------------------->
        <div class="card-header">Datos de Dosificación</div>

        <!-- Cuerpo -->
        <div class="card-body">
          <!-- Primer Renglon -->
          <div class="row">
            <!-- Columna Tipo Donacion -->
            <div class="col-sm-4 form-group">
              <label>Tipo de donación</label>
              <select v-model="tipoDonacion2" class="form-control" name="tipoDonacion2" id="tipoDonacion2">
                <option value="0" disabled selected>Seleccione</option>
                <option value="1">Leche Homóloga</option>
                <option value="2">Leche Heterológa</option>
              </select>
            </div>

            <!-- Columna Fecha de dosificacion -->
            <div class="col-sm-4 form-group">
              <label>Fecha de Dosificación</label>
                <input
                  @change="getNumeroToma"
                  id="fechaDosificacion"
                  type="date"
                  class="form-control"
                  placeholder="Fecha de Dosificación"
                  autocomplete="off"
                  name="fechaDosificacion"
                  v-model="fechaDosificacion"

                />
            </div>

            <!-- Columna Hora dosificacion -->
            <div class="col-sm-4 form-group">
              <label>Hora de Dosificación</label>
                <input
                  id="horaDosificacion"
                  type="time"
                  class="form-control"
                  placeholder="Hora de Dosificación"
                  autocomplete="off"
                  name="horaDosificacion"
                  v-model="horaDosificacion"

                />
            </div>
            <!-- Columna Servicio (Área del hospitalizado) -->
            <div class="col-sm-8 form-group">
              <label>Servicio (Área del hospitalizado)</label>
              <input
                id="servicio2"
                type="text"
                class="form-control"
                placeholder="Servicio (Área del hospitalizado)"
                autocomplete="off"
                name="servicio2"
                v-model="servicio2"

              />
            </div>
            <!-- Dosificación(ml) -->
            <div class="col-sm-4 form-group">
              <label>Dosificación (ml)</label>
              <input
                id="dosificacion"
                type="number"
                class="form-control"
                placeholder="Dosificación (ml)"
                autocomplete="off"
                name="dosificacion"
                v-model="dosificacion"

              />
            </div>
            <div class="col-sm-4 form-group">
              <label>Administración (ml)</label>
              <input
                id="administracion"
                type="number"
                class="form-control"
                placeholder="Administración (ml)"
                autocomplete="off"
                name="administracion"
                v-model="administracion"

              />
            </div>

              <div class="col-sm-4 form-group" v-show="tipoDonacion2=='1'">
                <label>Número de toma</label>
                <select v-model="numeroToma" class="form-control" name="numeroToma" id="numeroToma">
                  <option value="0" disabled selected>Seleccione</option>
                  <option value="1">Correcto</option>
                  <option value="2">Incorrecto</option>
                </select>
              </div>

              <div class="col-sm-4 form-group" v-show="tipoDonacion2=='2'">
                <label>Número de toma</label>
                <input
                  id="numeroToma"
                  type="number"
                  class="form-control"
                  placeholder="Número de toma"
                  autocomplete="off"
                  name="numeroToma"
                  v-model="numeroTomaHeterologa"
                  disabled
                />
              </div>



          </div>
        </div>
      </div>
      <!-- FIN Tarjeta para los datos de dosificacion -->
      <div style="display: flex; justify-content: center;" class="mt-3">
        <button class="btn boton-entrar-login text-white" @click="registrarDosificacion">
          <i class="fa fa-save"></i>
          Guardar Datos
        </button>
      </div>
    </div>
    <!-- FIN Contenedor div de vue -->
  </div>
</div>
  <!-- FIN Contenedor Principal -->
</form>

</div>
<script type="text/javascript">
  const app = new Vue({
    el: '#dosificacionHomologa',
    data: {
      ID_SESION: '<?php echo  auth()->user()->id; ?>',
      mensaje: 'Hola',
      existe_madre2: 'FALSE',
      id_madre2: '',
      nombreDosificacion: '',
      apellidoPaternoDosificacion: '',
      apellidoMaternoDosificacion: '',
      idMadre2: '',
      servicio2: '',
      fechaDosificacion: '',
      horaDosificacion: '',
      servicio2: '',
      dosificacion: '',
      administracion: '',
      numeroToma: '',
      numeroTomaHeterologa: 0,
      tipoDonacion2: '0',

      cantidadBebes: '',
      fechaNacimiento_bebe: '',
      horaNacimiento_bebe: '',
      aa:'',
      mes:'',
      dia:'',
      datos_madre2: [],
      bebesMadre: [],
      bebeSeleccionado: {}
    },

    methods: {
      onchange: function() {
        console.log(this.bebeSeleccionado)
        alert(this.bebeSeleccionado);
      },
      getNumeroToma(){
        fecha_completa = new Date();
        this.dia = fecha_completa.getDate();
        this.mes = fecha_completa.getMonth()+1;
        this.aa = fecha_completa.getFullYear();
        fecha_actual = this.aa + "/" + this.mes + "/" + this.dia;
        console.log(this.bebeSeleccionado.ID_DATOS_BEBE+" fecha "+fecha_actual);
        axios.get('http://127.0.0.1:8000/api/dosificaciones', {
          params: {
            id_sistema:this.datos_madre2.ID_SISTEMA,
            fecha: this.fechaDosificacion,
            id_bebe: this.bebeSeleccionado.ID_DATOS_BEBE
          }
        }).then(
          response => {
            chochito = response.data.data;
            //Procedemos a verificar si la madre cuenta con un bebé registrado
            console.log("Número de toma RECUPERADO: "+chochito);
            if (this.tipoDonacion2=="1") {
              console.log("Toma de Homologa: "+this.numeroToma);
            }else {
              if (this.tipoDonacion2 == "2") {
                console.log("MOSTRANDO TOMA");
                this.numeroTomaHeterologa = chochito;
                this.numeroTomaHeterologa = chochito+1;
                console.log(this.numeroTomaHeterologa);
                //this.numero_toma = chochito+1;
              }
            }
          }
        ).catch(
          error => {
            /*Swal.fire({
              icon: 'warning',
              title: 'Error, toma no resgistrada!'
            });*/
            //alert(error);
          }
        );
      },
      getDatosMadre(){
        //Usando AXIOS
        //http://127.0.0.1:8000/api/recepciones?id=HHOT26-05434
        axios.get('http://127.0.0.1:8000/api/recepciones', {
          params: {
            id_sesion: this.ID_SESION - 1,
            nom: this.nombreDosificacion, //el mismo de vmodel
            apPat:this.apellidoPaternoDosificacion,
            apMat:this.apellidoMaternoDosificacion
          }
        }).then(
          response => {
            this.datos_madre2 = response.data.data;
            this.existe_madre2 = 'TRUE';
            this.id_madre2 = this.datos_madre2.ID_SISTEMA;
            //Procedemos a verificar si la madre cuenta con un bebé registrado
            console.log("ID MADRE RECUPERADO: "+this.id_madre2);
            this.getBebesMadre();


          }
        ).catch(
          error => {
            Swal.fire({
              icon: 'warning',
              title: 'Error, la madre no está registrada!'
            });
            //alert(error);
          }
        );
      },
      getBebesMadre(){
        axios.get('http://127.0.0.1:8000/api/bebes', {
          params: {
            id_sistema: this.id_madre2
          }
        }).then(
          response => {
            this.bebesMadre = response.data.data;
            //this.existe_madre2 = 'TRUE';
            //Obtenemos la cantidad de bebés que tiene la madre
            this.cantidadBebes = this.bebesMadre.length;
            console.log(this.bebesMadre);
            console.log("LONGITUD ARREGLO: "+this.bebesMadre.length);

          }
        ).catch(
          error => {
            Swal.fire({
              icon: 'warning',
              title: 'Error, la madre no Tiene bebés!'
            });
            //alert(error);
          }
        );
      },
      registrarDosificacion(){ //Cuando Ya tiene bebés la madre, recibe y manda el ID del bebe
        this.getNumeroToma();
        //PRIMERO VALIDAMOS GENERAL
        if ( this.tipoDonacion2 == '' ) {
          Swal.fire({
            position: 'center',
            icon: 'warning',
            title: 'Selecciona Tipo de Donación!',
            showConfirmButton: false,
            timer: 1500
          })
        }else {
          //VALIDAMOS HOMOLOGA
          if (this.tipoDonacion2 == '1' || this.tipoDonacion2 == '2' ) {
            //Verificamos que haya bebés para esa madre
            if ( this.cantidadBebes == '0' ) {
              Swal.fire({
                position: 'center',
                icon: 'warning',
                title: 'Primero Debes registrar un bebé!',
                showConfirmButton: false,
                timer: 1500
              })
            }else {
              //Verificamos que haya seleccionado
              if (this.bebeSeleccionado.ID_DATOS_BEBE) {
                //Verificar los campos de Tipo Donacion
                if( this.tipoDonacion2 == '1' ){ //HOMOLOGA
                  if ( this.fechaDosificacion == '' || this.horaDosificacion == '' || this.servicio2 == '' || this.dosificacion == ''|| this.administracion == '' || this.numeroToma == '' ) {
                    Swal.fire({
                      position: 'center',
                      icon: 'warning',
                      title: 'Campos Vacíos, verifica por favor!',
                      showConfirmButton: false,
                      timer: 1500
                    })
                  }else {
                    //Usuar AXIOS
                    axios.post('http://127.0.0.1:8000/api/dosificaciones', {
                      id_sistema:this.datos_madre2.ID_SISTEMA,
                      servicio: this.servicio2,
                      fecha: this.fechaDosificacion,
                      hora: this.horaDosificacion,
                      tipo_dosificacion: this.tipoDonacion2,
                      cantidad_ml: this.dosificacion,
                      admin_ml: this.administracion,
                      num_tom: this.numeroToma, //Homologa: Correcto |Incorrecto
                      id_bebe: this.bebeSeleccionado.ID_DATOS_BEBE
                    }).then(
                        response => {
                          console.log("Todo bien registro recepcion");
                          Swal.fire({
                            title: 'Dosificación Registrada Con Éxito',
                        //    text: "ID_FRASCO: "+response.data.data,
                            icon: 'success',
                            //showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            //cancelButtonColor: '#d33',
                            confirmButtonText: 'OK'
                          }).then((result) => {
                            if (result.value) {
                              console.log("Ok");
                            }
                          })
                        }
                    ).catch(error => {
                      alert("Error al Registrar la Dosificación de la Leche "+ error);
                    });

                  }
                }else {
                  if (this.tipoDonacion2 == '2') { //HETEROLOGA
                    if ( this.fechaDosificacion == '' || this.horaDosificacion == '' || this.servicio2 == '' || this.dosificacion == ''|| this.administracion == ''  ) {
                      Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        title: 'Campos Vacíos, verifica por favor!',
                        showConfirmButton: false,
                        timer: 1500
                      })
                    }else {
                      axios.post('http://127.0.0.1:8000/api/dosificaciones', {
                        id_sistema:this.datos_madre2.ID_SISTEMA,
                        servicio: this.servicio2,
                        fecha: this.fechaDosificacion,
                        hora: this.horaDosificacion,
                        tipo_dosificacion: this.tipoDonacion2,
                        cantidad_ml: this.dosificacion,
                        admin_ml: this.administracion,
                        num_tom: 2,
                        id_bebe: this.bebeSeleccionado.ID_DATOS_BEBE
                      }).then(
                          response => {
                            console.log("Todo bien registro dosificación");
                            Swal.fire({
                              title: 'Dosificación Registrada Con Éxito',
                          //    text: "ID_FRASCO: "+response.data.data,
                              icon: 'success',
                              //showCancelButton: true,
                              confirmButtonColor: '#3085d6',
                              //cancelButtonColor: '#d33',
                              confirmButtonText: 'OK'
                            }).then((result) => {
                              if (result.value) {
                                console.log("Ok");
                              }
                            })
                          }
                      ).catch(error => {
                        alert("Error al Registrar la Dosificación de la Leche "+ error);
                      });
                    }
                  }
                }


              }else {
                Swal.fire({
                  position: 'center',
                  icon: 'warning',
                  title: 'Debes seleccionar un bebé!',
                  showConfirmButton: false,
                  timer: 1500
                })
              }
            }
          }
        }
      },
      registrarRelacion_BebeMadre(){
        if (this.fechaNacimiento_bebe == '' || this.horaNacimiento_bebe == '') {
          Swal.fire({
            position: 'center',
            icon: 'warning',
            title: 'Campos Vacíos, verifica por favor!',
            showConfirmButton: false,
            timer: 1500
          })
        }else{
          axios.post('http://127.0.0.1:8000/api/bebes', {
            id_sistema: this.datos_madre2.ID_SISTEMA,
            fecha_nacimiento: this.fechaNacimiento_bebe,
            hora_nacimiento: this.horaNacimiento_bebe
          }).then(
              response => {
                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Bebé Registrado',
                  showConfirmButton: false,
                  timer: 1500
                })
                this.fechaNacimiento_bebe = '';
                this.horaNacimiento_bebe = '';
                this.getBebesMadre();  //Recuperamos la lista de los bebés
                //swal("Registro Guardado!", "ID FRASCO: "+response.data.data, "success")
                //console.log("Todo bien");
                //alert("Recepción Registrada Con Éxito ID_FRASCO"+response.data.data);
              }
          ).catch(error => {
            alert("Error al Registrar la Relación Madre-Bebé "+ error);
          });
        }

      }

    },
    created: function () {
      console.log("Hola desde VUE");
      //this.getDatosMadre();
    }

  });
</script>
@endsection
