<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">

  <!-- Estilos de bootstrap 4 -->
  <link href="{{asset('css/libs/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>

  <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('css/login.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('css/sidebar.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('css/registro_donadora.css')}}" rel="stylesheet" type="text/css"/>
    <!--Agregando Vue y Axios -->
  <script src="{{asset('js/vue.js')}}"></script>
  <script src="{{asset('js/axios.min.js')}}"></script>

  <title></title>
</head>

<body>
  <!-- Componente Barra Lateral Izquierda -->
  @include('sidebar')

  <div class="content">
    <!-- Componente Cabecera -->
    @include('cabecera')

    <!-- Componente Cuerpo (Rutas) -->
    <div>
      @yield('cuerpo')
    </div>

    <!-- Componente Footer -->
    @include('pie')
  </div>

  <!-- Iconos -->
  <script src="{{asset('js/libs/fontawesome.js')}}"></script>
  <!-- SweetAlert2 -->
  <script src="{{asset('js/libs/sweetalert29.js')}}"></script>

  <!-- JQuery 3.5.1 -->
  <script src="{{asset('js/libs/jquery-3.5.1.min.js')}}"></script>
  <!-- JQuery Validate Plugin -->
  <script src="{{asset('js/libs/jquery.validate.min.js')}}"></script>
  <!-- popper -->
  <script src="{{asset('js/libs/popper.min.js')}}"></script>
  <!-- Bootstrap js -->
  <script src="{{asset('js/libs/bootstrap.min.js')}}"></script>
  <!-- Chart js -->
  <script src="{{asset('js/libs/Chart.min.js')}}"></script>

  <!-- Archivos personalizados -->
  <script src="{{asset('js/registro_donadora.js')}}"></script>
  <script src="{{asset('js/registro_leche.js')}}"></script>
  <script src="{{asset('js/analisis_heterologa.js')}}"></script>
  <script src="{{asset('js/pasteurizacion.js')}}"></script>
  <script src="{{asset('js/sidebar.js')}}"></script>
</body>

</html>
