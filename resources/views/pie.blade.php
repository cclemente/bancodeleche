<div class="container-custom pl-2 pr-2">
	<footer>
		<div id="detallefooter" class="col-lg-9">
			<div class="col-lg-3">
				<h4>Conoce el Estado</h4>
				<ul class="lista">
					<li><a href="https://web.archive.org/web/20190927005345/http://edomex.gob.mx/" target="_blank">Portal del Gobierno del Estado de México</a></li>
					<li><a href="https://web.archive.org/web/20190927005345/http://www.secogem.gob.mx/SAM/sit_atn_mex.asp" target="_blank">Quejas y Denuncias</a></li>
					<li><a href="https://web.archive.org/web/20190927005345/http://cemer.edomex.gob.mx/" target="_blank">Comisión Estatal de Mejora Regulatoria</a></li>
					<li><a href="https://web.archive.org/web/20190927005345/http://www.ipomex.org.mx/ipo/lgt/indice/salud.web" target="_blank">Información Pública de oficio Mexiquense</a></li>
					<li><a href="https://web.archive.org/web/20190927005345/http://www.saimex.org.mx/saimex/ciudadano/login.page" target="_blank">Solicitud de Información</a></li>
				</ul>
				<!--<div class="footermas">
					<a href="#"></a>
				</div>-->
			</div>
			<div class="col-lg-3">
				<h4>Acerca del Sitio</h4>
				<ul class="lista">
					<li><a href="https://web.archive.org/web/20190927005345/mailto:webmastersalud_edomex@edomex.gob.mx" target="_blank">Contáctanos</a></li>
					<li><a href="https://web.archive.org/web/20191017165647/http://salud.edomex.gob.mx/salud/paginageneral.html?pag_id=mapasitio" target="_self">Mapa del sitio</a></li>
					<li><a href="https://web.archive.org/web/20191017174309/http://salud.edomex.gob.mx/salud/paginageneral.html?pag_id=avisos_legales" target="_self">Avisos legales</a></li>
					<li><a href="https://web.archive.org/web/20191017172818/http://salud.edomex.gob.mx/salud/paginageneral.html?pag_id=acerca_sitio" target="_self">Acerca del sitio</a></li>
					<li><a href="https://web.archive.org/web/20190927005345/http://salud.edomex.gob.mx/intranet" target="_blank">Intranet</a></li>
				</ul>
				<!--<div class="footermas">
					<a href="#"></a>
				</div>	-->
			</div>
			<div class="col-lg-3">
				<h4>Contacto</h4>
				<ul class="lista">
					<li>Gobierno del Estado de México<br>
											Secretaría de Salud<br></li>
					<li>Av. Independencia Oriente # 1009, Col. Reforma y F.F.C.C. C.P. 50070, Toluca, Estado de México.</li>
					<li><a href="callto:2262500">(722) 2 26 25 00</a><br>
					<a href="https://web.archive.org/web/20190927005345/mailto:webmastersalud@edomex.gob.mx">webmastersalud@edomex.gob.mx
					</a></li>
				</ul>
				<!--<div class="footermas">
					<a href="#"></a>
				</div>	-->
			</div>
			<div class="col-lg-3">
				<h4>Enlaces de Interés</h4>
				<ul class="lista">
					<li><a href="https://web.archive.org/web/20190927005345/http://salud.edomex.gob.mx/isem" target="_blank">Instituto de Salud del Estado de México</a></li>
					<li><a href="https://web.archive.org/web/20190927005345/http://salud.edomex.gob.mx/hmpmonica_pretelini/home.html" target="_blank">Hospital Mateno Perinatal Mónica Pretelini Sáenz</a></li>
					<li><a href="https://web.archive.org/web/20190927005345/http://salud.edomex.gob.mx/cmalmateos" target="_blank">Centro Médico Lic. Adolfo López Mateos</a></li>
					<li><a href="https://web.archive.org/web/20190927005345/http://www.gob.mx/salud" target="_blank">Secretaria de Salud</a></li>
					<li><a href="https://web.archive.org/web/20190927005345/http://www.edomex.gob.mx/edictos" target="_blank">Edictos</a></li>
					<li><a href="https://web.archive.org/web/20190927005345/http://salud.edomex.gob.mx/isem/paginageneral.html?pag_id=enlaces_interes" target="_blank">Ver más</a></li>
				</ul>
				<!--<div class="footermas">
				<a href="#"></a>
				</div>	-->
			</div>
		</div>

		<div id="identidad" class="col-lg-3">
			<div id="logofooter"><img src="http://salud.edomex.gob.mx/salud/imagenes/h1.png" alt=""><hr></div>
		</div>
		<div style="clear:both"></div>
		<div id="footerbottom" class="col-lg-12" style="display: flex; justify-content: center;">
			<div class="row">Algunos derechos reservados 2018. Gobierno del Estado de México<br/>Esta página está diseñada para verse mejor en resolución de 1280 x 768 o superior, Firefox &amp; Chrome v30, Safari v5, IE 10
			</div>
		</div>
	</footer>
</div>
