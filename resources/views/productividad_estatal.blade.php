@extends('layout')
@section('cuerpo')

<div id="productividad_estatal">
  <form id="form-dosificacion-sucedaneo" onsubmit="return false">
    <!-- Contenedor Principal -->
    <div class="container p-3">
      <!-- Titulo de la tarjeta -->
      <div class="card mb-3">
        <div class="row justify-content-center">
          <h1 class="text-secondary">Coordinación Estatal de Lactancia Materna y Bancos de Leche </h1>
          <h2 class="text-secondary">Informe de productividad Estatal de Lactancia Materna y Banco de Leche Humana</h2>
        </div>
      </div>


      <!-- Tarjeta Información de la Unidad Hospitalaria -->
      <div class="card mb-3">
        <!-- Titulo de la tarjeta -->
        <div class="card-header">Información de la Unidad Hospitalaria </div>

        <!-- Cuerpo -->
        <div class="card-body">

          <table style="width:80%">
            <colgroup>
              <col style="width: 30%">
              <col style="width: 50%">
            </colgroup>

            <!-- Fila Reporte correspondiente al mes de -->
            <tr>
              <td>Reporte correspondiente al mes de:</td>
              <td>
                <select v-model="mes" class="form-control" name="mes" id="mes">
                  <option value="0" disabled>Seleccione</option>
                  <option value="1">Enero</option>
                  <option value="2">Febrero</option>
                  <option value="3">Marzo</option>
                  <option value="4">Abril</option>
                  <option value="5">Mayo</option>
                  <option value="6">Junio</option>
                  <option value="7">Julio</option>
                  <option value="8">Agosto</option>
                  <option value="9">Septiembre</option>
                  <option value="10">Octubre</option>
                  <option value="11">Noviembre</option>
                  <option value="12">Diciembre</option>
                </select>
              </td>
            </tr>

            <!-- Fila Institución -->
            <tr>
              <td>Institución: </td>
              <td>
                <input
                  id="institucionUH"
                  type="text"
                  class="form-control"
                  placeholder="Institución"
                  autocomplete="off"
                  v-model="institucionUH"
                  name="institucionUH"
                />
              </td>
            </tr>

            <!-- Fila Unidad Hospitalaria -->
            <tr>
              <td>Unidad Hospitalaria:</td>
              <td>
                <input
                  id="unidad_hospitalaria"
                  type="text"
                  class="form-control"
                  placeholder="Nombre de la Unidad Hospitalaria"
                  autocomplete="off"
                  v-model="unidad_hospitalaria"
                  name="unidad_hospitalaria"
                />
              </td>
            </tr>

            <!-- Fila Responsable del BLH -->
            <tr>
              <td>Responsable del Banco de Leche Humana:</td>
              <td>
                <input
                  id="nombre_responsable"
                  type="text"
                  class="form-control"
                  placeholder="Nombre del Responsable del Banco de Leche Humana"
                  autocomplete="off"
                  v-model="nombre_responsable"
                  name="nombre_responsable"
                />
              </td>
            </tr>
          </table>
        </div>
      </div> <!-- FIN Tarjeta Información de la Unidad Hospitalaria  -->

      <!-- Tarjeta Niños hospitalizados en áreas críticas y Donación -->
      <div class="card mb-3">
        <!-- Titulo de la tarjeta -->
        <div class="card-header">Niños hospitalizados en áreas críticas y Donación</div>

        <!-- Cuerpo -->
        <div class="card-body">

          <table class="table" style="width:100%">
            <colgroup>
              <!--Establece el tamaño de cada Columna-->
                <col style="width: 30%">
                <col style="width: 25%">
                <col style="width: 15%">
                <col style="width: 15%">
                <col style="width: 15%">
            </colgroup>

            <!--Encabezados Tabla-->
            <thead class="thead-light">
              <tr style="">
                <th colspan="2" style="background-color:#47944a; color: white;">
                  <center>Niños hospitalizados en áreas críticas</center>
                </th>
                <th colspan="3" style="background-color:#ad594c; color: white;">
                  <center>Donación</center>
                </th>
              </tr>
              <tr>
                <th><center>Clasificación</center></th>
                <th><center>Niños Hospitalizados</center></th>
                <th colspan="2"><center>Donadoras Internas</center></th>
                <th><center>Donadoras Externas</center></th>
              </tr>
            </thead>

            <!--Cuerpo Tabla-->
            <tbody>
              <!--Renglón 1-->
              <tr>
                <td><b>Prematuro</b></td>
                <td>
                  <input
                    id="total_prematuros"
                    type="text"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="total_prematuros"
                    name="total_prematuros"
                    disabled
                  >
                </td>
                <td rowspan="2">
                  <center><br><br><b>Homólogas</b></center>
                </td>
                <td rowspan="2">
                  <center><br><br><b>Heterólogas</b></center>
                </td>
                <td rowspan="2">
                  <center><br><br><b>Heterólogas</b></center>
                </td>
              </tr>
              <!--Renglón 2-->
              <tr>
                <td><b>Neonato<b></td>
                <td>
                  <input
                    id="total_neonatos"
                    type="text"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="total_neonatos"
                    name="total_neonatos"
                    disabled
                  >
                </td>

              </tr>

              <tr>
                <td><b>Lactante</b></td>
                <td>
                  <input
                    id="total_lactantes"
                    type="text"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="total_lactantes"
                    name="total_lactantes"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="total_homologasInternas"
                    type="text"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="total_homologasInternas"
                    name="total_homologasInternas"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="total_heterologasInternas"
                    type="text"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="total_heterologasInternas"
                    name="total_heterologasInternas"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="total_heterologasExternas"
                    type="text"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="total_heterologasExternas"
                    name="total_heterologasExternas"
                    disabled
                  >
                </td>
              </tr>
            </tbody>
          </table>

        </div> <!-- FIN Cuerpo -->
      </div>


      <!-- Tarjeta Recolección de Leche Humana -->
      <div class="card mb-3">
        <!-- Titulo de la tarjeta -->
        <div class="card-header">Recolección de Leche Humana</div>

        <!-- Cuerpo -->
        <div class="card-body">

          <table class="table" style="width:100%">
            <colgroup>
              <!--Establece el tamaño de cada Columna-->
                <col style="width: 10%">
                <col style="width: 9%">
                <col style="width: 9%">
                <col style="width: 9%">
                <col style="width: 9%">
                <col style="width: 9%">
                <col style="width: 9%">
                <col style="width: 9%">
                <col style="width: 9%">
                <col style="width: 9%">
                <col style="width: 9%">
            </colgroup>
            <!--Encabezados Tabla-->
            <thead class="thead-light">
              <tr>
                <th colspan="11" style="background-color:#2a3391; color: white;">
                  <center>Recolección de Leche Humana</center>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="3"><center>ml de leche humana recolectada</center></td>
                <td colspan="5"><center>ml de leche descartada</center></td>
                <td colspan="2"><center>ml de leche pasteurizada</center></td>
                <td rowspan="2"><center>ml de leche pasteurizada almacenada </center></td>
              </tr>
              <tr>
                <td> <center><b>Sobrante de homóloga</center></b> </td>
                <td> <center><b>Homóloga</center></b> </td>
                <td> <center><b>Heteróloga</center></b> </td>
                <td> <center><b>Embalaje</center></b> </td>
                <td> <center><b>Suciedad</center></b> </td>
                <td> <center><b>Color</center></b> </td>
                <td> <center><b>Olor</center></b> </td>
                <td> <center><b>Acidez</center></b> </td>
                <td> <center><b><=4*D</center></b> </td>
                <td> <center><b>4-8</center></b> </td>
              </tr>
              <tr>
                <td>
                  <input
                    id="sobranteHomologa"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="sobranteHomologa"
                    name="sobranteHomologa"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="homologaRLH"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="homologaRLH"
                    name="homologaRLH"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="heterologaRLH"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="heterologaRLH"
                    name="heterologaRLH"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="embalaje"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="embalaje"
                    name="embalaje"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="suciedad"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="suciedad"
                    name="suciedad"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="color"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="color"
                    name="color"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="olor"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="olor"
                    name="olor"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="acidez"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="acidez"
                    name="acidez"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="mayorCuatroRLH"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="mayorCuatroRLH"
                    name="mayorCuatroRLH"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="CuatroOchoRLH"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="CuatroOchoRLH"
                    name="CuatroOchoRLH"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="mlPasteurizada"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="mlPasteurizada"
                    name="mlPasteurizada"
                    disabled
                  >
                </td>
              </tr>
            </tbody>
          </table>

        </div>
      </div>






      <!-- Tarjeta Unidades con convenio/acuerdo para pasteurización -->
      <div class="card mb-3">
        <!-- Titulo de la tarjeta -->
        <div class="card-header">Unidades con convenio/acuerdo para pasteurización</div>

        <!-- Cuerpo -->
        <div class="card-body">

          <table class="table" style="width:100%">
            <colgroup>
              <!--Establece el tamaño de cada Columna-->
                <col style="width: 20%">
                <col style="width: 60%">
                <col style="width: 20%">

            </colgroup>
            <!--Encabezados Tabla-->
            <thead class="thead-light">
              <tr>
                <th colspan="3" style="background-color:#a1a2b3; color: white;">
                  <center>Unidades Con Convenio/Acuerdo Para Pasteurización</center>
                </th>
              </tr>
              <tr>
                <th><center>Institución</center></th>
                <th><center>Nombre de Unidad de Salud</center></th>
                <th><center> ml De Leche Recibida</center></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <input
                    id="institucionConvenio"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="institucionConvenio"
                    name="institucionConvenio"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="nombreUnidad_Salud"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="nombreUnidad_Salud"
                    name="nombreUnidad_Salud"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="mlLeche_recibida"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="mlLeche_recibida"
                    name="mlLeche_recibida"
                    disabled
                  >
                </td>
              </tr>

              <tr>
                <td>
                  <input
                    id="institucionConvenio"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="institucionConvenio"
                    name="institucionConvenio"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="nombreUnidad_Salud"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="nombreUnidad_Salud"
                    name="nombreUnidad_Salud"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="mlLeche_recibida"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="mlLeche_recibida"
                    name="mlLeche_recibida"
                    disabled
                  >
                </td>
              </tr>
            </tbody>
          </table>

        </div>
      </div>


      <!-- Tarjeta Asesoria Y Niños Beneficiados -->
      <div class="card mb-3">
        <!-- Titulo de la tarjeta -->
        <div class="card-header">Asesoría y Niños Beneficiados</div>

        <!-- Cuerpo -->
        <div class="card-body">

          <table class="table" style="width:100%">
            <colgroup>
              <!--Establece el tamaño de cada Columna-->
                <col style="width: 33%">
                <col style="width: 17%">
                <col style="width: 16%">
                <col style="width: 33%">

            </colgroup>
            <!--Encabezados Tabla-->
            <thead class="thead-light">
              <tr>
                <th style="background-color:#deb35d; color: white;">
                  <center>Asesoría</center>
                </th>
                <th colspan="3" style="background-color:#2cc4c7; color: white;">
                  <center>Niños Beneficiados</center>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td rowspan="2">
                  <br>
                  <center>Mujeres asistidas en lactancia</center>
                </td>
                <td rowspan="1" colspan="2">
                  <center>Niños internos beneficiados</center>
                </td>
                <td rowspan="2">
                  <br>
                  <center>Niños externos beneficiados</center>
                </td>
              </tr>

              <tr>
                <td><center>Homóloga</center></td>
                <td><center>Heteróloga</center></td>
              </tr>

              <tr>
                <td>
                  <input
                    id="mujeresAsistidas"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="mujeresAsistidas"
                    name="mujeresAsistidas"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="niniosInternos_Homo"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="niniosInternos_Homo"
                    name="niniosInternos_Homo"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="niniosInternos_Hete"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="niniosInternos_Hete"
                    name="niniosInternos_Hete"
                    disabled
                  >
                </td>
                <td>
                  <input
                    id="niniosExternos"
                    type="number"
                    class="form-control"
                    placeholder=""
                    autocomplete="off"
                    v-model="niniosExternos"
                    name="niniosExternos"
                    disabled
                  >
                </td>
              </tr>


            </tbody>
          </table>

        </div>
      </div>



      <!-- Tarjeta De Firmas -->
      <div class="card mb-3">
        <!-- Titulo de la tarjeta -->
        <div class="card-header"></div>

        <!-- Cuerpo -->
        <div class="card-body">

          <table class="table" style="width:100%" style="border:none;">
            <colgroup>
              <!--Establece el tamaño de cada Columna-->
                <col style="width: 45%">
                <col style="width: 10%">
                <col style="width: 45%">
            </colgroup>
            <!--Encabezados Tabla-->
            <thead class="thead-light">
            </thead>
            <tbody>
              <tr>
                <td style="border:none;"></td>
                <td style="border:none;"> </td>
                <td style="border:none;"></td>
              </tr>
              <tr>
                <td><center>Elaboró</center></td>
                <td> </td>
                <td><center>Autorizó</center></td>
              </tr>
              <tr>
                <td style="border:none;"><center>Responsable del Banco de Leche Humana</center></td>
                <td style="border:none;"> </td>
                <td style="border:none;"><center>Director del Hospital</center></td>
              </tr>
            </tbody>
          </table>

        </div>
      </div>







    </div><!-- FIN Contenedor Principal -->
  </form>
</div>



<script type="text/javascript">
  const app = new Vue({
    el: '#productividad_estatal',
    data: {
      ID_SESION: '<?php echo  auth()->user()->id; ?>',
      mes: '0',

      //Variables Tabla
      institucion: '',
      unidad_hospitalaria: '',
      nombre_responsable: '',
      total_prematuros: '',
      total_neonatos: '',
      total_lactantes: '',
      total_homologasInternas: '',
      total_heterologasInternas: '',
      total_heterologasExternas: '',
      sobranteHomologa: '',
      homologaRLH: '',
      heterologaRLH: '',
      embalaje: '',
      suciedad: '',
      color: '',
      olor: '',
      acidez: '',
      mayorCuatroRLH: '',
      CuatroOchoRLH: '',
      mlPasteurizada: '',
      institucionConvenio: '',
      nombreUnidad_Salud: '',
      mlLeche_recibida: '',
      institucionConvenio: '',
      nombreUnidad_Salud: '',
      institucionUH: '',
      mujeresAsistidas: '',
      niniosInternos_Homo: '',
      niniosInternos_Hete: '',
      niniosExternos: ''


    },
    methods: {

    },
    created: function(){
      console.log("ESTE ES EL ID DE SESIÓN: "+this.ID_SESION);
    }
  });
</script>

@endsection
