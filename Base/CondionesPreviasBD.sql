insert into TIPO_DONACION(DESCRIPCION) VALUES('Homologa');
insert into TIPO_DONACION(DESCRIPCION) VALUES('Heterologa');
insert into ROL_SISTEMA(DESCRIPCION) VALUES('Enfermería');
insert into ROL_SISTEMA(DESCRIPCION) VALUES('Laboratorio');
insert into ROL_SISTEMA(DESCRIPCION) VALUES('Administración');
insert into HOSPITALES(NOMBRE_HOSPITAL,UBICACION) VALUES('Hospital Materno Infantil Chalco Josefa Ortiz de Domínguez','Chalco');
insert into HOSPITALES(NOMBRE_HOSPITAL,UBICACION) VALUES('Hospital Materno Perinata Mónica Pretelini Sáenz','Toluca');
insert into HOSPITALES(NOMBRE_HOSPITAL,UBICACION) VALUES('Hospital Dr. José María Rodríguez.','Ecatepec');
insert into HOSPITALES(NOMBRE_HOSPITAL,UBICACION) VALUES('Hospital General de Atlacomulco.','Atlacomulco');
insert into HOSPITALES(NOMBRE_HOSPITAL,UBICACION) VALUES('Hospital General Miguel Hidalgo y Costilla.','Tenancingo');
insert into HOSPITALES(NOMBRE_HOSPITAL,UBICACION) VALUES('Hospital General Dr. Maximiliano Ruiz Castañeda.','Naucalpan');
insert into HOSPITALES(NOMBRE_HOSPITAL,UBICACION) VALUES('Hospital General de Axapusco.','Axapusco');
insert into HOSPITALES(NOMBRE_HOSPITAL,UBICACION) VALUES('Hospital General La Perla Nezahualcóyotl.','Nezahualcóyotl');
insert into CLASIFICACION_LECHE(DESCRIPCION) VALUES('Prematuro');
insert into CLASIFICACION_LECHE(DESCRIPCION) VALUES('Calostro');
insert into CLASIFICACION_LECHE(DESCRIPCION) VALUES('Transicion');
insert into CLASIFICACION_LECHE(DESCRIPCION) VALUES('Madura');


insert into DIAGNOSTICO(DESCRIPCION) VALUES('Galactosernia clásica');
insert into DIAGNOSTICO(DESCRIPCION) VALUES('Fenilcetonuria');
insert into DIAGNOSTICO(DESCRIPCION) VALUES('Hipoglucemia');
insert into DIAGNOSTICO(DESCRIPCION) VALUES('VIH');

insert into TIPO_FORMULA(DESCRIPCION) VALUES('Prematuro');
insert into TIPO_FORMULA(DESCRIPCION) VALUES('Inicio');
insert into TIPO_FORMULA(DESCRIPCION) VALUES('Continuación');
insert into TIPO_FORMULA(DESCRIPCION) VALUES('Especial');



/*laravel*/
php artisan db:seed



Hospital___1 DATOS DE LOS USUARIOS
1 Enfermería PASSWORD=> Materno834_En
2 Laboratorio PASSWORD=> Materno3483_Lab
Hospital___2 DATOS DE LOS USUARIOS
1 Enfermería PASSWORD=>Pretelini594-En  
2 Laboratorio PASSWORD=>Pretelini843-Lab
Hospital___3 DATOS DE LOS USUARIOS
1 Enfermería PASSWORD=>Dr348fEn
2 Laboratorio PASSWORD=>Dr282Lab
Hospital___4 DATOS DE LOS USUARIOS
1 Enfermería PASSWORD=>Atlacomulco2494>En
2 Laboratorio PASSWORD=>Atlacomulco564>Lab
Hospital___5 DATOS DE LOS USUARIOS
1 Enfermería PASSWORD=>Hidalgo0454-En
2 Laboratorio PASSWORD=>Hidalgo764-Lab
Hospital___6 DATOS DE LOS USUARIOS
1 Enfermería PASSWORD=>Maximiliano9484-En
2 Laboratorio PASSWORD=>Maximiliano9644-Lab
Hospital___7 DATOS DE LOS USUARIOS
1 Enfermería PASSWORD=>Axapusco8273-En
2 Laboratorio PASSWORD=>Axapusco03884-Lab
Hospital___8 DATOS DE LOS USUARIOS
1 Enfermería PASSWORD=>Perla4834-En
2 Laboratorio PASSWORD=>Perla8573-Lab







