create table MADRE(
     ID_MADRE INT AUTO_INCREMENT PRIMARY KEY,
     ID_SISTEMA VARCHAR(30) UNIQUE NOT NULL,
     NOMBRE VARCHAR(40) NOT NULL,
     APELLIDO_PATERNO VARCHAR(40) NOT NULL,
     APELLIDO_MATERNO VARCHAR(40) NOT NULL,
     FECHA_REGISTRO TIMESTAMP NOT NULL,
) ENGINE = INNODB;
create table PRUEBA(
     ID_PRUEBA INT AUTO_INCREMENT PRIMARY KEY,
     SIDA BOOLEAN NOT NULL,
     SIFILIS BOOLEAN NOT NULL,
     HEPATITIS BOOLEAN NOT NULL,
     DROGAS BOOLEAN NOT NULL
) ENGINE = INNODB;

create table CLASIFICACION_LECHE(
     ID_CLASIFICACION INT AUTO_INCREMENT PRIMARY KEY,
     DESCRIPCION VARCHAR(20) NOT NULL
) ENGINE = INNODB;
create table ROL_SISTEMA(
     ID_ROL_SISTEMA INT AUTO_INCREMENT PRIMARY KEY,
     DESCRIPCION VARCHAR(20) NOT NULL
) ENGINE = INNODB;
create table HOSPITALES(
     ID_HOSPITAL INT AUTO_INCREMENT PRIMARY KEY,
     NOMBRE_HOSPITAL VARCHAR(40) NOT NULL,
     UBICACION VARCHAR(20) NOT NULL
) ENGINE = INNODB;
create table REGISTRO_USUARIO(
     ID_REGISTRO_USUARIO INT AUTO_INCREMENT PRIMARY KEY,
     USUARIO VARCHAR(30) UNIQUE NOT NULL,
     PASSWORD VARCHAR(30) NOT NULL,
     ID_ROL_SISTEMA INT,
     INDEX IND1 (ID_ROL_SISTEMA),
     FOREIGN KEY (ID_ROL_SISTEMA)
        REFERENCES ROL_SISTEMA(ID_ROL_SISTEMA),
     ID_HOSPITAL INT,
     INDEX IND2 (ID_HOSPITAL),
     FOREIGN KEY (ID_HOSPITAL)
        REFERENCES HOSPITALES(ID_HOSPITAL)
) ENGINE = INNODB;
create table RELACION_MADRE_REGISTRO_USUARIO(
     ID_RELACION_MADRE_REGISTRO_USUARIO INT AUTO_INCREMENT PRIMARY KEY,
     ID_MADRE INT,
     INDEX IND3 (ID_MADRE),
     FOREIGN KEY (ID_MADRE)
        REFERENCES MADRE(ID_MADRE),
     ID_REGISTRO_USUARIO INT,
     INDEX IND4 (ID_REGISTRO_USUARIO),
     FOREIGN KEY (ID_REGISTRO_USUARIO)
        REFERENCES REGISTRO_USUARIO(ID_REGISTRO_USUARIO)
) ENGINE = INNODB;
CREATE TABLE TIPO_DONACION(
     ID_TIPO_DONACION INT AUTO_INCREMENT PRIMARY KEY,
     DESCRIPCION VARCHAR(20)
) ENGINE = INNODB;
create table RECEPCION(
     ID_RECEPCION INT AUTO_INCREMENT PRIMARY KEY,
     ID_FRASCO VARCHAR(20) NOT NULL,
     AREA_HOSPITALIZACION VARCHAR(30) NOT NULL,
     FECHA_EXTRACCION TIMESTAMP NOT NULL,
     HORA_EXTRACCION TIME NOT NULL,
     ID_TIPO_DONACION INT,
     INDEX IND5 (ID_TIPO_DONACION),
     FOREIGN KEY (ID_TIPO_DONACION)
        REFERENCES TIPO_DONACION(ID_TIPO_DONACION),
     CANTIDAD_RECIBIDA DECIMAL NOT NULL
) ENGINE = INNODB;

CREATE TABLE RELACION_MADRE_RECEPCION(
     ID_RELACION_MADRE_RECEPCION INT AUTO_INCREMENT PRIMARY KEY,
     ID_MADRE INT,
     INDEX IND6 (ID_MADRE),
     FOREIGN KEY (ID_MADRE)
        REFERENCES MADRE(ID_MADRE),
     ID_RECEPCION INT,
     INDEX IND7 (ID_RECEPCION),
     FOREIGN KEY (ID_RECEPCION)
        REFERENCES RECEPCION(ID_RECEPCION)
) ENGINE = INNODB;

CREATE TABLE RELACION_MADRE_PRUEBA(
     ID_RELACION_MADRE_PRUEBA INT AUTO_INCREMENT PRIMARY KEY,
     ID_MADRE INT,
     INDEX IND8 (ID_MADRE),
     FOREIGN KEY (ID_MADRE)
        REFERENCES MADRE(ID_MADRE),
     ID_PRUEBA INT,
     INDEX IND9 (ID_PRUEBA),
     FOREIGN KEY (ID_PRUEBA)
        REFERENCES PRUEBA(ID_PRUEBA)
) ENGINE = INNODB;

CREATE TABLE ANALISIS(
     ID_ANALISIS INT AUTO_INCREMENT PRIMARY KEY,
     ID_CLASIFICACION INT,
     INDEX IND10 (ID_CLASIFICACION),
     FOREIGN KEY (ID_CLASIFICACION)
        REFERENCES CLASIFICACION_LECHE(ID_CLASIFICACION),
     COLOR BOOLEAN NOT NULL,
     SUCIEDAD BOOLEAN NOT NULL,
     OLOR BOOLEAN NOT NULL,
     CANTIDAD DECIMAL NOT NULL,
     DESECHADA BOOLEAN NOT NULL,
     ID_RECEPCION  INT,
     INDEX IND11 (ID_RECEPCION),
     FOREIGN KEY (ID_RECEPCION)
        REFERENCES RECEPCION(ID_RECEPCION)
) ENGINE = INNODB;
CREATE TABLE DOSIFICACION(
     ID_DOSIFICACION INT AUTO_INCREMENT PRIMARY KEY,
     ID_SISTEMA VARCHAR(30),
     FECHA_DOSIFICACION DATE NOT NULL,
     HORA_DOSIFICACION TIME NOT NULL,
     SERVICIO VARCHAR(30) NOT NULL,
     CANTIDAD_DOSIFICACION_ML DECIMAL NOT NULL,
     NUMERO_TOMA BOOLEAN NOT NULL,
     ADMINISTRACION_ML DECIMAL NOT NULL
) ENGINE = INNODB;

