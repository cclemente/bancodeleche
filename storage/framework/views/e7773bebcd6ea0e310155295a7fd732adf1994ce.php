<?php $__env->startSection('content'); ?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<link href="<?php echo e(asset('css/style.css')); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(asset('css/login.css')); ?>" rel="stylesheet" type="text/css"/>

<div class="container">
  <!-- Componente Cabecera -->
  <?php echo $__env->make('cabecera', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<div class="container text-center mt-2 mb-2">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><?php echo e(__('Login')); ?></div>

                <div class="card-body">
                    <form class="form-signin" method="POST" action="<?php echo e(route('login')); ?>">
                        <?php echo csrf_field(); ?>

                        <img class="mb-4" src="<?php echo e(asset('img/user.png')); ?>" alt="avatar" width="72" height="72">
                        <h1 class="h3 mb-3 font-weight-normal">
                          Inicio de Sesi&oacute;n
                        </h1>
                                <div class="form-group text-left">
                                  <label>Seleccione Rol</label>
                                  <select id="email" name="email" class="form-control">
                                    <option value="" disabled selected>Seleccione</option>
                                    <option value="1">Enfermer&iacute;a</option>
                                    <option value="2">Laboratorio</option>
                                    <option value="3">Administraci&oacute;n</option>
                                  </select>
                                </div>

                                <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <div class="form-group text-left">
                          <label>Ingrese su contrase&ntilde;a</label>
                          <input id="password" type="password" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="password" required autocomplete="current-password">
                          <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                              <span class="invalid-feedback" role="alert">
                                  <strong><?php echo e($message); ?></strong>
                              </span>
                          <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                        </div>
                        <button class="btn btn-lg btn-block boton-entrar-login text-white btn-primary" type="submit">
                          Entrar
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
  <!-- Componente Footer -->
  <?php echo $__env->make('pie', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>

<!-- Iconos -->
<script src="<?php echo e(asset('js/libs/fontawesome.js')); ?>"></script>
<!-- JQuery 3.5.1 -->
<script src="<?php echo e(asset('js/libs/jquery-3.5.1.min.js')); ?>"></script>
<!-- JQuery Validate Plugin -->
<script src="<?php echo e(asset('js/libs/jquery.validate.min.js')); ?>"></script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/pvd/Documentos/ProyectoBancoLeche/bancodeleche2/resources/views/auth/login.blade.php ENDPATH**/ ?>