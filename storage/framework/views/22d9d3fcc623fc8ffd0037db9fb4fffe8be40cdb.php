<?php $__env->startSection('cuerpo'); ?>

<form id="form-registro-donadora">
  <!-- Contenedor Principal -->
  <div class="p-3 pl-2 pr-2">
    <!-- Tarjeta para los datos de la madre donadora -->
    <div class="card mb-3">
      <!-- Titulo de la tarjeta -->
      <div class="card-header">Registro de Donadora</div>

      <!-- Cuerpo -->
      <div class="card-body">
        <!-- Primer Renglon -->
        <div class="row">
          <!-- campo nombre de la madre -->
          <div class="col-sm-4 form-group">
            <label>Nombre Donadora</label>
            <div class="input-group">
              <input
                id="nombre"
                type="text"
                class="form-control"
                placeholder="Nombre"
                autocomplete="off"
                name="nombre"
              />
              <!--<datalist id="lista-madres">
                <option value="Ejemplo madre uno"></option>
                <option value="Ejemplo madre dos"></option>
                <option value="Ejemplo madre tres"></option>
                <option value="Ejemplo madre cuatro"></option>
              </datalist>
              <div class="input-group-append">
                <button class="btn btn-primary" type="button" title="Registrar Nueva">
                  <i class="fa fa-plus"></i>
                </button>
              </div>-->
            </div>
            <!-- Solo para este caso el error tiene una ubicacion personalizada -->
            <div id="error-nombre"></div>
          </div>

          <!-- Columna apellido paterno -->
          <div class="col-sm-4 form-group">
            <label>Apellido Paterno</label>
            <input
              id="apellidoPaterno"
              type="text"
              class="form-control"
              placeholder="Apellido Paterno"
              autocomplete="off"
              name="apellidoPaterno"
            />
          </div>

          <!-- Columna apellido materno -->
          <div class="col-sm-4 form-group">
            <label>Apellido Materno</label>
            <input
              id="apellidoMaterno"
              type="text"
              class="form-control"
              placeholder="Apellido Materno"
              autocomplete="off"
              name="apellidoMaterno"
            />
          </div>

          <!-- Seleccion del tipo de donacion
          <div class="col-sm-6 form-group">
            <label>Seleccione tipo de donaci&oacute;n</label>
            <select name="tipoDonacion" class="form-control">
              <option value="" disabled selected>Seleccione</option>
              <option value="1">Hom&oacute;loga</option>
              <option value="2">Heter&oacute;loga</option>
            </select>
          </div>-->
        </div>
      </div>
    </div>
    <!-- FIN Tarjeta para los datos de la madre donadora -->

    <!-- Tarjeta para los datos de resultados de analisis -->
    <div class="card">
      <!-- Titulo de la tarjeta -->
      <div class="card-header">Resutados de An&aacute;lisis</div>

      <!-- Cuerpo -->
      <div class="card-body">
        <!-- Primer Renglon -->
        <div class="row">
          <!-- Columna VIH -->
          <div class="col-sm-3 form-group">
            <label>Prueba de VIH</label>
            <select class="form-control" name="vih" id="vih">
              <option value="" disabled selected>Seleccione</option>
              <option value="1">Negativo</option>
              <option value="2">Positivo</option>
            </select>
          </div>

          <!-- Columna Sifilis -->
          <div class="col-sm-3 form-group">
            <label>Prueba de Sifilis</label>
            <select class="form-control" name="sifilis" id="sifilis">
              <option value="" disabled selected>Seleccione</option>
              <option value="1">Negativo</option>
              <option value="2">Positivo</option>
            </select>
          </div>

          <!-- Columna Hepatitis C -->
          <div class="col-sm-3 form-group">
            <label>Prueba de Hepatitis C</label>
            <select class="form-control" name="hepatitisC" id="hepatitisC">
              <option value="" disabled selected>Seleccione</option>
              <option value="1">Negativo</option>
              <option value="2">Positivo</option>
            </select>
          </div>

          <!-- Columna Drogas -->
          <div class="col-sm-3 form-group">
            <label>Prueba de consumo de drogas</label>
            <select class="form-control" name="drogas" id="drogas">
              <option value="" disabled selected>Seleccione</option>
              <option value="1">Negativo</option>
              <option value="2">Positivo</option>
            </select>
          </div>
        </div>
      </div>
    </div>
    <!-- FIN Tarjeta para los datos de resultados de analisis -->

    <div style="display: flex; justify-content: center;" class="mt-3">
      <button class="btn boton-entrar-login text-white btn-guardar" type="submit">
        <i class="fa fa-save"></i>
        Guardar Datos
      </button>
    </div>
  </div>
  <!-- FIN Contenedor Principal -->
</form>

<label id="label-id-sesion" name="<?php echo e(auth()->user()->id); ?>"></label>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/pvd/Documentos/ProyectoBancoLeche/bancodeleche2/resources/views/registro_donadora.blade.php ENDPATH**/ ?>