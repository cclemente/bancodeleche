<?php $__env->startSection('cuerpo'); ?>

<div class="mt-4 pl-2 pr-2 mb-4">
  <h1 style="color: #e4894e; font-size: 40pt;">Bienvenido</h1>

  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="4"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="5"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="6"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="7"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="http://salud.edomex.gob.mx/salud/imagenes/eventos/recomendaciones_covid.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
  <!--        <h3 style="color: black;">Sistema Banco de Leche Humana</h3>  -->
        </div>
      </div>
      <div class="carousel-item">
        <img src="http://salud.edomex.gob.mx/salud/imagenes/eventos/plan_regreso_seguro.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
  <!--         <h3 style="color: black;">Sistema Banco de Leche Humana</h3>    -->
        </div>
      </div>
      <div class="carousel-item">
        <img src="http://salud.edomex.gob.mx/isem/imagenes/eventos/registroservsocial.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h3 style="color: black;">Sistema Banco de Leche Humana</h3>    -->
        </div>
      </div>
      <div class="carousel-item">
        <img src="http://salud.edomex.gob.mx/isem/imagenes/eventos/dengue_zika_ch.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
    <!--        <h3 style="color: black;">Sistema Banco de Leche Humana</h3>    -->
        </div>
      </div>

      <div class="carousel-item">
        <img src="http://salud.edomex.gob.mx/isem/imagenes/eventos/sarampion.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
  <!--          <h3 style="color: black;">Sistema Banco de Leche Humana</h3>   -->
        </div>
      </div>
      <div class="carousel-item">
        <img src="http://salud.edomex.gob.mx/isem/imagenes/eventos/temporadacalor2020.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
    <!--        <h3 style="color: black;">Sistema Banco de Leche Humana</h3>    -->
        </div>
      </div>
      <div class="carousel-item">
        <img src="http://salud.edomex.gob.mx/isem/imagenes/eventos/aqui_estoy.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
    <!--        <h3 style="color: black;">Sistema Banco de Leche Humana</h3>    -->
        </div>
      </div>
      <div class="carousel-item">
        <img src="http://salud.edomex.gob.mx/isem/imagenes/eventos/alerta_genero.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
  <!--          <h3 style="color: black;">Sistema Banco de Leche Humana</h3>     -->
        </div>
      </div>

    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>


  <!-- Inico de la tarjeta -->
<div class="card mb-3">
    <!-- Cuerpo -->
  <div class="card-body">
<div id="block-block-9" class="block block-block">
   <div class="container">
       <hr>
        <div class="principales col-xs-12 col-sm-12 col-md-3 col-md-lg-3">
          <a href="ac_lm_acerca" target="_self">
          <img alt="Mamá con bebé en brazos" src="http://salud.edomex.gob.mx/salud/imagenes/celmbl.jpg">
   	      <h1 class="sub">Lactancia Materna y Bancos de Leche</h1>
    	   La Lactancia Materna es la forma  de aportar a los niños pequeños los nutrientes que necesitan.</a>
   	    </div>
        <div class="principales col-xs-12 col-sm-12 col-md-3 col-md-lg-3">
	    <a href="../isem/camaleon" target="_self">
        <img alt="" src="http://salud.edomex.gob.mx/salud/imagenes/camal.jpg" />
	    <h1 class="sub">CA.MA.LE.ON</h1>
  	    Puedes evitar un infarto cerebral...</a>
      </div>

  	  <div class="principales col-xs-12 col-sm-12 col-md-3 col-md-lg-3">
        <a href="../isem/diabetes" target="_self">
        <img alt="glucómetro" src="http://salud.edomex.gob.mx/salud/imagenes/diabetes_.jpg">
   	    <h1 class="sub">Diabetes</h1>
  	    Infórmate y cuida tu salud<br>#PrevenirEsSalud</a>
    </div>

     <div class="principales col-xs-12 col-sm-12 col-md-3 col-md-lg-3">
  	  <a href="../isem/tp_cancer_mama" target="_blank">
      <img alt="liston,cáncer mama" src="http://salud.edomex.gob.mx/salud/imagenes/cancer_mama.jpg">
   	  <h1 class="sub">Cáncer de Mama</h1>
      Es importante detectarlo antes de que el tumor haya alcanzado un tamaño grande o se extienda a otros órganos.</a></p>
     </div>
  </div>
</div>

</div>
</div>
  <!-- Fin de la tarjeta -->



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/pvd/Documentos/ProyectoBancoLeche/bancodeleche2/resources/views/inicio.blade.php ENDPATH**/ ?>