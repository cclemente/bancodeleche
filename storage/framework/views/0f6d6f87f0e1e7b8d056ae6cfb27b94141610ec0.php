<section class="contenedor-sidebar">
  <div class="sidebar">
    <!-- Enlace Inicio -->
    <a
      href="<?php echo e(url('inicio')); ?>"
      class="<?php echo e((request()->is('inicio'))
        ? 'active'
        : ''); ?>"
    >
      Inicio
    </a>

    <?php if(auth()->user()->id == 1): ?>
      <!-- Enlace Registro Donadora -->
      <a
        href="<?php echo e(url('registro_donadora')); ?>"
        class="<?php echo e((request()->is('registro_donadora'))
          ? 'active'
          : ''); ?>"
      >
        Registro Donadora
      </a>
    
      <!-- Enlace Recepcion Leche -->
      <a
        href="<?php echo e(url('recepcion_leche')); ?>"
        class="<?php echo e((request()->is('recepcion_leche'))
          ? 'active'
          : ''); ?>"
      >
        Recepci&oacute;n Leche
      </a>
    <?php endif; ?>

    <?php if(auth()->user()->id == 2): ?>
      <!-- Enlace Dosificacion Homologa -->
      <a
        href="<?php echo e(url('dosificacion_homologa')); ?>"
        class="<?php echo e((request()->is('dosificacion_homologa'))
          ? 'active'
          : ''); ?>"
      >
        Dosificaci&oacute;n Leche 
      </a>

      <!-- Enlace Analisis de Leche Heterologa -->
      <a
        href="<?php echo e(url('analisis_leche')); ?>"
        class="<?php echo e((request()->is('analisis_leche'))
          ? 'active'
          : ''); ?>"
      >
        An&aacute;lisis Leche
      </a>

      <!-- Enlace Pasteurizacion -->
      <a
        href="<?php echo e(url('pasteurizacion')); ?>"
        class="<?php echo e((request()->is('pasteurizacion'))
          ? 'active'
          : ''); ?>"
      >
        Pasteurizaci&oacute;n Leche
      </a>

      <!-- Boton para accionar el dropdown de Sucedaneo -->
      <a class="dropdown-btn-sucedaneo">Suced&aacute;neo
        <i class="fa fa-caret-down"></i>
      </a>
      <!-- Elementos contenidos en el dropdown menu -->
      <div class="dropdown-container">
        <!-- Enlace a reporte Consultar Totales -->
        <a
          href="<?php echo e(url('preparacion_sucedaneo')); ?>"
          class="<?php echo e((request()->is('preparacion_sucedaneo'))
            ? 'active'
            : ''); ?>"
        >
          Preparaci&oacute;n
        </a>
        <a
          href="<?php echo e(url('dosificacion_sucedaneo')); ?>"
          class="<?php echo e((request()->is('dosificacion_sucedaneo'))
            ? 'active'
            : ''); ?>"
        >
          Dosificaci&oacute;n</a>
      </div>
    <?php endif; ?>

    <?php if(auth()->user()->id == 3): ?>
      <!-- Boton para accionar el dropdown de reportes -->
      <a class="dropdown-btn">Reportes
        <i class="fa fa-caret-down"></i>
      </a>
      <!-- Elementos contenidos en el dropdown menu -->
      <div class="dropdown-container">
        <!-- Enlace a reporte Consultar Totales -->
        <a
          href="<?php echo e(url('reporte_totales')); ?>"
          class="<?php echo e((request()->is('reporte_totales'))
            ? 'active'
            : ''); ?>"
        >
          Consultar Totales
        </a>
        <a
          href="<?php echo e(url('reporte_datos_leche')); ?>"
          class="<?php echo e((request()->is('reporte_datos_leche'))
            ? 'active'
            : ''); ?>"
        >
          Datos Leche
        </a>
      </div>

      <!-- Enlace Productivdad Estatal -->
      <a
        href="<?php echo e(url('productividad_estatal')); ?>"
        class="<?php echo e((request()->is('productividad_estatal'))
          ? 'active'
          : ''); ?>"
      >
        Productivdad Estatal
      </a>
    <?php endif; ?>

    <!-- Enlace Cerrar Sesion -->
    <a  href="<?php echo e(route('logout')); ?>"  onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
      Salir
      <i class="fa fa-sign-out-alt"></i>
    </a>
    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
        <?php echo csrf_field(); ?>
    </form>
  </div>
</section>
<?php /**PATH /home/pvd/Documentos/ProyectoBancoLeche/bancodeleche2/resources/views/sidebar.blade.php ENDPATH**/ ?>