<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">

  <!-- Estilos de bootstrap 4 -->
  <link href="<?php echo e(asset('css/libs/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css"/>

  <link href="<?php echo e(asset('css/style.css')); ?>" rel="stylesheet" type="text/css"/>
  <link href="<?php echo e(asset('css/login.css')); ?>" rel="stylesheet" type="text/css"/>
  <link href="<?php echo e(asset('css/sidebar.css')); ?>" rel="stylesheet" type="text/css"/>
  <link href="<?php echo e(asset('css/registro_donadora.css')); ?>" rel="stylesheet" type="text/css"/>
    <!--Agregando Vue y Axios -->
  <script src="<?php echo e(asset('js/vue.js')); ?>"></script>
  <script src="<?php echo e(asset('js/axios.min.js')); ?>"></script>

  <title></title>
</head>

<body>
  <!-- Componente Barra Lateral Izquierda -->
  <?php echo $__env->make('sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  <div class="content">
    <!-- Componente Cabecera -->
    <?php echo $__env->make('cabecera', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <!-- Componente Cuerpo (Rutas) -->
    <div>
      <?php echo $__env->yieldContent('cuerpo'); ?>
    </div>

    <!-- Componente Footer -->
    <?php echo $__env->make('pie', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </div>

  <!-- Iconos -->
  <script src="<?php echo e(asset('js/libs/fontawesome.js')); ?>"></script>
  <!-- SweetAlert2 -->
  <script src="<?php echo e(asset('js/libs/sweetalert29.js')); ?>"></script>

  <!-- JQuery 3.5.1 -->
  <script src="<?php echo e(asset('js/libs/jquery-3.5.1.min.js')); ?>"></script>
  <!-- JQuery Validate Plugin -->
  <script src="<?php echo e(asset('js/libs/jquery.validate.min.js')); ?>"></script>
  <!-- popper -->
  <script src="<?php echo e(asset('js/libs/popper.min.js')); ?>"></script>
  <!-- Bootstrap js -->
  <script src="<?php echo e(asset('js/libs/bootstrap.min.js')); ?>"></script>
  <!-- Chart js -->
  <script src="<?php echo e(asset('js/libs/Chart.min.js')); ?>"></script>

  <!-- Archivos personalizados -->
  <script src="<?php echo e(asset('js/registro_donadora.js')); ?>"></script>
  <script src="<?php echo e(asset('js/registro_leche.js')); ?>"></script>
  <script src="<?php echo e(asset('js/analisis_heterologa.js')); ?>"></script>
  <script src="<?php echo e(asset('js/pasteurizacion.js')); ?>"></script>
  <script src="<?php echo e(asset('js/sidebar.js')); ?>"></script>
</body>

</html>
<?php /**PATH /home/pvd/Documentos/ProyectoBancoLeche/bancodeleche2/resources/views/layout.blade.php ENDPATH**/ ?>